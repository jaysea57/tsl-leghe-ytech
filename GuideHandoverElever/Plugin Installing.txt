cordova plugin add cordova-plugin-inappbrowser

cordova plugin add cordova-plugin-dialogs

cordova plugin add https://github.com/pwlin/cordova-plugin-file-opener2.git

cordova plugin add https://github.com/Paldom/SpinnerDialog.git

cordova plugin add cordova-plugin-file-transfer

cordova plugin add cordova-plugin-file

cordova plugin add cordova-plugin-admobpro

cordova plugin add https://github.com/danwilson/google-analytics-plugin.git

SOLO ANDROID
cordova -d plugin add ".\CordovaLocalPluginAndroid\phonegap-facebook-plugin" --variable APP_ID="606280542738883" --variable APP_NAME="Tuttosport League - gironi"

SOLO IOS
cordova -d plugin add ".\CordovaLocalPluginIos\phonegap-facebook-plugin" --variable APP_ID="606280542738883" --variable APP_NAME="Tuttosport League - gironi"