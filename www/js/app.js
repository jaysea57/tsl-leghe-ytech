// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('myApp', 
    [
        'ionic',
        'Services',
        'appCtrl',
        'homeCtrl',
        'calendarioCtrl',
        'dettaglioGiornataGironiCtrl',
        'partiteGironiCtrl',
        'classificheCtrl',
        'classificheGironiCtrl',
        'formazioneCtrl',
        'gestioneRosaCtrl',  
        'aggiungiGiocatoreCtrl',  
        'loginCtrl',
        //'datiLegaCtrl',
        'formLegaCtrl',
        'opzioniSquadraCtrl',
        'registrazioneCtrl',
        'requiredRegCtrl',
        'modificaProfiloCtrl',
        'checklist-model',
        'ngMessages',
        'ngCordova'
    ]
)
//.constant('$ionicLoadingConfig', {
//    template: '<ion-spinner></ion-spinner>'
//  })
.run(function($ionicPlatform, $http, $rootScope, $ionicLoading, $cordovaSpinnerDialog, $ionicHistory, $timeout, $cordovaGoogleAnalytics) {
    
    $ionicPlatform.ready(function() {

        if (window.cordova) {
            $cordovaGoogleAnalytics.startTrackerWithId('UA-2135082-4');
        }

        if(window.cordova){
            $rootScope.showLoading = function(text){
                if(text == undefined)
                    text = 'Caricamento';
                $cordovaSpinnerDialog.show('',text, true);
            };
            $rootScope.hideLoading = function(){
                $cordovaSpinnerDialog.hide();
            };
            $rootScope.showToast = function(text, duration){
                if(!duration){
                    duration = 2500;
                }                
                $ionicLoading.show({
                    template: text,
                    duration: duration,
                    noBackdrop: true
                });
            };
        } else {
            $rootScope.showLoading = function(text){
                $ionicLoading.show({
                    template: '<ion-spinner></ion-spinner>'
                });
            };
            $rootScope.hideLoading = function(){
                $ionicLoading.hide();
            };
            $rootScope.showToast = function(text){
                //nothing..
            };
        }

        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            //StatusBar.styleDefault();
            StatusBar.hide();
        }
        // jc push notification stuff
        if(window.cordova){
			console.log('before push, calling firebase');
			// jc nuove push con firebase
			window.FirebasePlugin.hasPermission(function(data) {
    			console.log("FirebasePlugin haspermission: " + data.isEnabled);
				//console.log(JSON.stringify(data));
				if (!data.isEnabled) {
					if (ionic.Platform.isIOS()) {
						window.FirebasePlugin.grantPermission();
					}
				}
			});
			window.FirebasePlugin.onTokenRefresh(onTokenRefresh, onTokenRefreshErr);
			window.FirebasePlugin.getToken(getToken, getTokenErr);
			window.FirebasePlugin.onNotificationOpen(onNotificationAPN, onNotificationErr);			
		}
		// jc plugin push notif firebase
		function onTokenRefresh(token) {
			console.log("got token refresh: " + token);
			getToken(token);
		}

		function getToken(token) {
			console.log("got token: " + token);
			if ($rootScope.deviceToken == token) {
				console.log("token push gia preso, non rifaccio chiamata");
			}
			else {
				tokenHandler(token);
			}
		}

		function onTokenRefreshErr(error) {
			console.log("got token refresh error: " + error);
		}

		function getTokenErr(error) {
			console.log("got token error: " + error);
		}
		//

		function onNotificationAPN(event) {
			console.log('------NOTIFICATION------');
			console.log(event);
			console.log(JSON.stringify(event));
			/*
			if (event.alert) {
				navigator.notification.alert(event.alert);
			}

			if (event.sound) {
				var snd = new Media(event.sound);
				snd.play();
			}

			if (event.badge) {
				pushNotification.setApplicationIconBadgeNumber(successHandler, errorHandler, event.badge);
			}
			*/
		}

		function onNotificationErr(error) {
			console.log('------NOTIFICATION ERROR------');
			console.log(error);
		}

		function tokenHandler(result) {
			$rootScope.deviceToken = result;
			console.log('tokenHandler device token:' + result);
			$rootScope.deviceId = device.uuid;
			console.log('device uuid:' + $rootScope.deviceId);
			var type = 'UNKNOWN';
			if (ionic.Platform.isAndroid()) {
				type = 'ANDROID';
			}
			else if (ionic.Platform.isIOS()) {
				type = 'APPLE';
			}
            $rootScope.deviceTarget = type;
		};

		//fine push
        
        if(ionic.Platform.isAndroid()){
            $rootScope.backCloseApp = false;
            $ionicPlatform.registerBackButtonAction(function(){
                if(!$ionicHistory.backView()){
                    if($rootScope.backCloseApp == true){
                        navigator.app.exitApp();
                    }
                    $rootScope.backCloseApp = true;
                    $ionicLoading.show({
                        template: "Premi ancora indietro per uscire",
                        duration: 800,
                        noBackdrop: true
                    });
                    $timeout(function(){
                        $rootScope.backCloseApp = false;
                    },3000);
                } else {
                    $ionicHistory.goBack();
                }
            }, 110);
        }

    });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

    $ionicConfigProvider.views.maxCache(0);
    $ionicConfigProvider.backButton.text('');
    $ionicConfigProvider.backButton.icon('');
    $ionicConfigProvider.backButton.previousTitleText(false);
    $ionicConfigProvider.views.swipeBackEnabled(false);
    if(ionic.Platform.isAndroid()){
        $ionicConfigProvider.scrolling.jsScrolling(true);
    }

    $stateProvider

    //.state('splash', {
    //    url: "/splash",
    //    templateUrl: "templates/splash.html",
    //    controller: 'splashCtrl'
    //})
    .state('login', {
        cache: true,
        url: "/login/:gioco",
        templateUrl: "templates/login.html",
        controller: 'loginCtrl'
    })
    .state('app', {
        url: "/app",
        abstract: true,
        templateUrl: "templates/menu.html",
        controller: 'AppCtrl'
    })
    .state('app.home', {
        url: "/home",
        views: {
        'menuContent': {
            templateUrl: "templates/home.html",
            controller: "homeCtrl"
            }
        }
    })
    .state('app.gestioneRosa', {
        url: "/gestioneRosa",
        //cache: true,
        views: {
        'menuContent': {
            templateUrl: "templates/gestioneRosa.html",
            controller: 'gestioneRosaCtrl'
            }
        }
    })
    .state('app.aggiungiGiocatore', {
        url: "/aggiungiGiocatore/:ruoloGiocatore",
        //cache: true,
        views: {
        'menuContent': {
            templateUrl: "templates/aggiungiGiocatore.html",
            controller: 'aggiungiGiocatoreCtrl'
            }
        }
    })
    .state('app.formazione', {
        url: "/formazione",
        //cache: true,
        views: {
        'menuContent': {
            templateUrl: "templates/formazione.html",
            controller: 'formazioneCtrl'
            }
        }
    })
    .state('app.calendario', {
        url: "/calendario",
        views: {
        'menuContent': {
            templateUrl: "templates/calendario.html",
            controller: 'calendarioCtrl'
            }
        }
    })
    .state('app.partiteGironi', {
        url: "/partiteGironi",
        views: {
        'menuContent': {
            templateUrl: "templates/partiteGironi.html",
            controller: 'partiteGironiCtrl'
            }
        }
    })
    .state('app.dettaglioGiornata', {
        url: "/calendario/:numeroGiornata/:dataGiornata/:idGiornata",
        views: {
        'menuContent': {
            templateUrl: "templates/dettaglioGiornata.html",
            controller: 'calendarioCtrl'
            }
        }
    })
    .state('app.dettaglioGiornataGironi', {
        url: "/partiteGironi/:idGiornata/:dataGiornata/:numeroGiornata",
        views: {
        'menuContent': {
            templateUrl: "templates/dettaglioGiornataGironi.html",
            controller: 'dettaglioGiornataGironiCtrl'
            }
        }
    })
    .state('app.dettaglioTabellino', {
        url: "/calendario/dettaglioTabellino",
        views: {
        'menuContent': {
            templateUrl: "templates/dettaglioTabellino.html",
            controller: 'calendarioCtrl'
            }
        }
    })
    .state('app.classifiche', {
        url: "/classifiche",
        views: {
        'menuContent': {
            templateUrl: "templates/classifiche.html",
            controller: 'classificheCtrl'
            }
        }
    })
    .state('app.classificheGironi', {
        url: "/classificheGironi",
        views: {
        'menuContent': {
            templateUrl: "templates/classificheGironi.html",
            controller: 'classificheGironiCtrl'
            }
        }
    })
    .state('app.opzioniSquadra', {
        url: "/opzioniSquadra",
        views: {
        'menuContent': {
            templateUrl: "templates/opzioniSquadra.html",
            controller: 'opzioniSquadraCtrl'
            }
        }
    })
    .state('app.scegliStemma', {
        url: "/opzioniSquadra/scegliStemma",
        views: {
        'menuContent': {
            templateUrl: "templates/scegliStemma.html",
            controller: 'opzioniSquadraCtrl'
            }
        }
    })
    .state('app.regolamento', {
        url: "/regolamento",
        views: {
        'menuContent': {
            templateUrl: "templates/regolamento.html",
            controller: 'regolamentoCtrl'
            }
        }
    })
    .state('app.modificaProfilo', {
        url: "/modificaProfilo",
        views: {
        'menuContent': {
            templateUrl: "templates/modificaProfilo.html",
            controller: 'modificaProfiloCtrl'
            }
        }
    })
    .state('app.datiLega', {
        url: "/datiLega",
        views: {
        'menuContent': {
            templateUrl: "templates/formLega.html",
            controller: 'formLegaCtrl'
            }
        }
    })
    .state('registrazione', {
        cache: true,
        url: "/registrazione/:gioco",
        templateUrl: "templates/registrazione.html",
        controller: 'registrazioneCtrl'
    })
    .state('requiredReg', {
        cache: true,
        url: "/requiredReg/:requiredMail",
        templateUrl: "templates/requiredReg.html",
        controller: 'requiredRegCtrl'
    });
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/login/2');
});

/*
app.directive('formatDate', function() {
    return {
        require: 'ngModel',
        link: function (scope, elem, attr, modelCtrl) {
            modelCtrl.$formatters.push(function (modelValue) {
                return (new Date(modelValue));
            });
        }
    };
});
*/