/// <reference path="../../../typings/tsd.d.ts" />
	
	//response model

	//Object model
interface Window {
	cordova: any;
}
interface PromptResult{
	input1: any;
	buttonIndex: number;
}
interface AlertPlugin{
	alert: (message: string, confirmCallback: () => void, title?: string, buttonName?: string) => void;
	confirm: (message: string, confirmCallback: (buttonIndex: number) => void, title?: string, buttonLabels?: string[]) => void;
	prompt: (message: string, confirmCallback: (results: PromptResult) => void, title?: string, buttonLabels?: string[], defaultText?: string) => void;
}
interface Navigator{
	notification: AlertPlugin;
	app: any;
}
interface IFBFacebookConnectPlugin{
	api: (graphPath: string, permissions: string[], successCallback: () => void, failureCallback: () => void) => void;
	getAccessToken: (successCallback: () => void, failureCallback: () => void) => void;
	getLoginStatus: (successCallback: () => void, failureCallback: () => void) => void;
	logEvent: (name: string, params: any[], valueToSum: any, successCallback: () => void, failureCallback: () => void) => void;
	logPurchase: (value: any, currency: any, successCallback: () => void, failureCallback: () => void) => void;
	login: (permissions: string[], successCallback: (data: IFBLoginSuccessResponse) => void, failureCallback: (data: IFBLoginErrorResponse) => void) => void;
	logout: (successCallback: () => void, failureCallback: () => void) => void;
	showDialog: (options: any, successCallback: () => void, failureCallback: () => void) => void;
	setApplicationId: (appId: string[], successCallback: (data: any) => void, failureCallback: (data: any) => void) => void;
}
interface IFBLoginErrorResponse{
	errorCode: string;
	errorMessage: string;
}
interface IAuthResponse{
	accessToken: string;
	expiresIn: string;
	session_key: boolean;
	sig: string;
	userID: string;
}
interface IFBLoginSuccessResponse{
	status: string;
	authResponse: IAuthResponse;
}

declare var facebookConnectPlugin: IFBFacebookConnectPlugin;
declare var ionic: any;
module OM{
	//Authentication
	export class AuthenticationResponse {
		success: Success;
		error: ErrorResponse;
	}
	export class Success {
		presidente_lega: Boolean;
		required_email: number;
		required_reg: number;
		user: User;
	}
	export class User
	{
		CAP:string;
		campionati: Array<number>;
		cognome:string;
		dt_nascita:string;
		mail:string;
		firstLogin:Boolean;
		fittizia:Boolean;
		flUnsubscribe:number;
		fl_articoli_privacy_editore :number;
		fl_mktg_editore:number;
		fl_nwl_editore: number;
		fl_privacy_editore: number;
		fl_servizio_editore: number;
		fl_servizio_gioco: number;
		idCampionato: number;
		idCliente: number;
		localita: string;
		nickname: string;
		nome: string;
		numeroSquadre: number;
		pagamento: Boolean;
		presidente: Boolean;
		provincia: string;
		required_reg: number;
		sesso: string;
		squadraDelCuore: string;
		squadre: Array<number>;
		tipoToken: string;
		accessToken: string;
		token: string;
		userFBId: string;
		via: string;
	};

	//Logout
	export class LogoutResponse {
		success: SuccessLogout;
		error: ErrorResponse;
	}
	export class SuccessLogout {
		id_campionato: number;
		id_cliente: number;
		presidente_lega: Boolean;
		url: string;
		webUrl: string;
	}

	//Error Response
	export class ErrorResponse
	{
			debug_message: string;
			id: string;
			url: string;
			user_message: string;
	};
	//Get Squadre
	export class GetSquadreResponse{
		links: Links;
		squadre: Squadre[];
		tipo_gioco: string;
		error: ErrorResponse;
	}
	export class Links{
		calendario: string;
		classifica: string;
		formazione: string;
		rosa: string;
		tabellino: string;
	}
	export class Squadre {
		classifica: Classifica;
		codice_squadra: number;
		eraseable: Boolean;
		idAvatar: number;
		id_campionato: number;
		id_gruppo: number;
		is_presidente_lega: Boolean;
		iscrittaPlayoff: Boolean;
		iscrizioni_playoff_aperte: Boolean;
		msg_iscrizione_playoff: string;
		nome: string;
		nome_campionato: string;
		playoff: Boolean;
		prossima_partita: ProssimaPartita;
		qualificataPlayoff: Boolean;
		ultima_partita: UltimaPartita;
		url_avatar: string;
		url_avatar_large: string;
		url_avatar_small: string;
	}
	export class Classifica {
		settimanale: Settimanale;
		torneo_andata: TorneoAndata;
		torneo_estivo: TorneoEstivo;
		torneo_ritorno: TorneoRitorno;
		msg_utente: string;
	}
	export class Settimanale {
		posizione: number;
		punti: number;
	}
	 export class TorneoAndata {
		msg_utente: string;
	}
	export class TorneoEstivo {
		posizione: number;
		punti: number;
	}
	export class TorneoRitorno {
		msg_utente: string;
	}
	export class ProssimaPartita {
		data: string;
		id: number;
		numero: string;
		squadre: SquadreProssimaPartita[];
	}
	export class SquadreProssimaPartita {
		gol: string;
		id: number;
		nome: string;
	}
	export class UltimaPartita {
		data: string;
		id: number;
		numero: string;
		squadre: SquadreUltimaPartita[];
	}
	export class SquadreUltimaPartita {
		gol: string;
		id: number;
		nome: string;
	}
	//Get Calendario
	export class GetCalendarioResponse {
		giornate: Giornate[];
		error: ErrorResponse;
		msg_utente: string;
	}
	export class Giornate {
		data: string;
		id_giornata: number;
		id_partita: number;
		numero: number;
		punti: string;
		squadre: any[];
	}
	//Get Dettaglio
	export class GetDettaglioGiornataResponse {
		partita: number;
		squadre: SquadreDettaglio[];
		error: ErrorResponse;
	}
	export class SquadreDettaglio {
		bonus_casa: number;
		id: number;
		m_v: number;
		nome: string;
		riserve: Riserve[];
		titolari: Titolari[];
		totale_bonus: number;
		voto: number;
		bonus_sost: number;
		malus_sost: number;
	}
	export class Titolari {
		cd_ruolo: number;
		codice: number;
		cognome: string;
		nome: string;
		stats: Stat2[];
	}
	export class Stat2 {
		voto: number;
		b_m: number;
		mv: number;
		a_e: string;
		gs: number;
		aut: number;
		rsb: number;
		rs: number;
		rp: number;
		gf: number;
		gol_45: number;
	}
	export class Riserve {
		cd_ruolo: number;
		codice: number;
		cognome: string;
		nome: string;
		stats: Stat[];
	}
	export class Stat {
		voto: number;
		b_m: number;
		mv: number;
		a_e: string;
		gs: number;
		aut: number;
		rsb: number;
		rs: number;
		rp: number;
		gf: number;
		gol_45: number;
	}
	//Get Classifica
	export class GetClassificaResponse {
		settimanale: SettimanaleClassifica[];
		torneo_andata: TorneoAndataClassifica[];
		torneo_estivo: TorneoEstivoClassifica[];
		torneo_ritorno: TorneoRitornoClassifica[];
		torneo_leghe: TorneoLegheClassifica[];
		msg_utente: string;
		error: ErrorResponse;
	}
	export class TorneoRitornoClassifica {
		msg_utente: string;
	}
	export class TorneoEstivoClassifica {
		codice_squadra: number;
		nome_squadra: string;
		posizione: number;
		punti: number;
	}
	export class TorneoAndataClassifica {
		msg_utente: string;
	}
	export class SettimanaleClassifica {
		codice_squadra: number;
		nome_squadra: string;
		posizione: number;
		punti: number;
	}
	export class TorneoLegheClassifica{
		amm: number;
		codice_squadra: number;
		esp: number;
		gf: number;
		gs: number;
		nome_squadra: string;
		posizione: number;
		punti: number;
		punti_totali: number;
		msg_utente: string;
	}
	//Add Squadra ModificaAvatar
	export class ModificaAvatarSquadraResponse {
		avatarList: AvatarList[];
		links: Links;
		squadre: SquadreAddSquadra[];
		error: ErrorResponse;
	}
	export class SquadreAddSquadra {
		codice_squadra: number;
		ds_squadra: string;
		eraseable: Boolean;
		idAvatar: number;
		id_campionato: number;
		id_gruppo: number;
		is_presidente_lega: Boolean;
		iscrittaPlayoff: Boolean;
		iscrizioni_playoff_aperte: Boolean;
		playoff: Boolean;
		qualificataPlayoff: Boolean;
		url_avatar: string;
		url_avatar_large: string;
		url_avatar_small: string;
	}
	export class AvatarList {
		bynaryImgLarge: string;
		ds_avatar: string;
		idAvatar: number;
	}
	//Add Squadra ModificaSquadra
	export class ModificaSquadraResponse {
		success: SuccessModificaSquadra;
		error: ErrorResponse;
	}
	export class SuccessModificaSquadra {
		msg_utente: string;
		presidente_lega: Boolean;
		squadra: SquadraModificaSquadra;
	}
	export class SquadraModificaSquadra {
		codice_squadra: number;
		ds_squadra: string;
		eraseable: Boolean;
		idAvatar: number;
		id_campionato: number;
		id_gruppo: number;
		is_presidente_lega: Boolean;
		iscrittaPlayoff: Boolean;
		iscrizioni_playoff_aperte: Boolean;
		playoff: Boolean;
		qualificataPlayoff: Boolean;
		url_avatar: string;
		url_avatar_large: string;
		url_avatar_small: string;
	}
	//Add Squadra NuovaSquadra
	export class NuovaSquadraResponse {
		links: Links;
		squadre: SquadreNuovaSquadra[];
		error: ErrorResponse;
	}
	export class SquadreNuovaSquadra {
		codice_squadra: number;
		ds_squadra: string;
		eraseable: Boolean;
		idAvatar: number;
		id_campionato: number;
		id_gruppo: number;
		is_presidente_lega: Boolean;
		iscrittaPlayoff: Boolean;
		iscrizioni_playoff_aperte: Boolean;
		playoff: Boolean;
		qualificataPlayoff: Boolean;
		url_avatar: string;
		url_avatar_large: string;
		url_avatar_small: string;
	}
	//Gestione Rosa
	export class GestioneRosaResponse {
		ROSA: ROSA[];
		GIOCATORI_DISPONIBILI: GIOCATORIDISPONIBILI[];
		regolam_accettato: Boolean;
		id_campionato: number;
		id_squadra: number;
		minInRosa: number;
		maxInRosa: number;
		is_avviato: Boolean;
		is_singoli: Boolean;
		is_cup: Boolean;
		is_gprix: Boolean;
		saldo: number;
		asta_conclusa: Boolean;
		giorni_scaduti_uno: Boolean;
		tipo_form: number;
		num_types: number[];
		tot_num_players: number;
		loggato: Boolean;
		num_asta: number;
		site_name: string;
		campDone: Boolean;
		bloccoRosa: Boolean;
		msg_blocco_rosa: string;
		error: ErrorResponse;
	}
	export class GIOCATORIDISPONIBILI {
		ID: number;
		RUOLO: number;
		VALORE_ATTUALE: number;
		NOME: string;
		COGNOME: string;
		SQUADRA: string;
		CONSIGLIATO: Boolean;
	}
	export class ROSA {
		ID: number;
		RUOLO: number;
		VALORE_ATTUALE: number;
		NOME: string;
		COGNOME: string;
		SQUADRA: string;
		COSTO_ACQUISTO: number;
		IN_FORMAZIONE: Boolean;
		CONSIGLIATO: Boolean;
	}

	//Modifica rosa Response
	export class SuccessModificaRosaResponse {
		success: SuccessGestioneRosa;
		error: ErrorResponse;
	}
	export class SuccessGestioneRosa {
		msg_utente: string;
		presidente_lega: Boolean;
	}
	export class ErrorModificaRosaResponse {
		error: ErrorGestioneRosa;
	}
	export class ErrorGestioneRosa {
		debug_message: string;
		id: string;
		user_message: string;
	}

	//Formazione data
	export class FormazioneDataResponse {
		ROSA: RosaFormazioneData[];
		FORMAZIONE: any[];
		next_giornata: number;
		next_partita: number;
		is_blocked: Boolean;
		camp_done: Boolean;
		turno_di_riposo: Boolean;
		TATTICHE: TatticheFormazioneData[];
		is_singoli: Boolean;
		is_gprix: Boolean;
		id_squadra: number;
		id_campionato: number;
		is_avviato: Boolean;
		site_name: string;
		is_cup: Boolean;
		loggato: Boolean;
		id_cliente: number;
		tipo_blocco: string;
		msg_blocco: string;
		error: ErrorResponse;
	}
	export class TatticheFormazioneData {
		TATTICA: number;
		DESCRIZIONE: string;
		DESIGN: DESIGN;
	}
	export class DESIGN {
		DIFENSORI: number;
		CENTROCAMPISTI: number;
		ATTACCANTI: number;
	}
	export class RosaFormazioneData {
		ID_GIOCATORE: number;
		CODICE_RUOLO: number;
		NOME: string;
		COGNOME: string;
		SQUADRA: string;
		IN_ROSA: number;
		inFormazione: boolean;
	}

	//Salva Formazione Response
	export class SalvaFormazioneResponse {
		success: SuccessSalvaFormazione;
		error: ErrorResponse;
	}
	export class SuccessSalvaFormazione {
		msg_utente: string;
		presidente_lega: Boolean;
	}
	
	//Cancella formazione
	export class CancellaFormazioneResponse {
		success: SuccessCancellaFormazione;
		error: ErrorResponse;
	}
	export class SuccessCancellaFormazione {
		msg_utente: string;
		presidente_lega: Boolean;
	}

	//registrazione
	export class RegistrazioneResponse {
		success: SuccessRegistrazione;
		error: ErrorResponse
	}
	export class SuccessRegistrazione {
		presidente_lega: number;
		url: string;
		user: UserRegistrazione;
	}
	export class UserRegistrazione {
		CAP: string;
		campionati: number[];
		cognome: string;
		dt_nascita: string;
		email: string;
		firstLogin: number;
		fittizia: number;
		flUnsubscribe: number;
		fl_articoli_privacy_editore: number;
		fl_mktg_editore: number;
		fl_nwl_editore: number;
		fl_privacy_editore: number;
		fl_regolamento: number;
		fl_servizio_editore: number;
		fl_servizio_gioco: number;
		idCampionato: number;
		idCliente: number;
		idGruppo: number;
		larba: string;
		localita: string;
		nickname: string;
		nome: string;
		numeroSquadre: number;
		pagamento: number;
		password: string;
		presidente: number;
		professione: string;
		profilo: string;
		provincia: string;
		required_reg: number;
		sesso: string;
		squadraDelCuore: string;
		squadre: string[];
		tel: string;
		username: string;
		via: string;
	}

	//getProfilo
	export class GetProfiloResponse {
		success: SuccessGetProfilo;
		error: ErrorResponse;
	}

	export class SuccessGetProfilo {
		presidente_lega: boolean;
		user: UserGetProfilo;
	}

	export class UserGetProfilo {
		CAP: string;
		campionati: any[];
		capSpedizione: string;
		cittaSpedizione: string;
		cognome: string;
		dt_nascita: string;
		email: string;
		emailAlternativa: string;
		firstLogin: boolean;
		fittizia: boolean;
		flUnsubscribe: number;
		idCliente: number;
		indirizzoSpedizione: string;
		localita: string;
		nome: string;
		pagamento: boolean;
		presidente: boolean;
		provincia: string;
		provinciaSpedizione: string;
		required_reg: number;
		sesso: string;
		squadre: any[];
		tel: string;
		via: string;
	}

	//modifica profilo
	export class ModificaProfiloResponse{
		success: SuccessModificaProfilo;
		error: ErrorResponse;
	}

	export class SuccessModificaProfilo {
		msg_utente: string;
	}

	//Get News
	export class GetNewsResponse{
		flAttivo: number;
		idNews: number;
		listaNews: News[];
	}

	export class News{
		dsNews: string;
		dsTitle: string;
		dtCreation: string;
		dtExpiry: string;
		flAttivo: number;
		idNews: number;
	}

	//TUUUUUTTA ROBA DELLE LEGHE
	//Get Dati Lega
	export class GetDatiLegaResponse {
        amici: Amici[];
        campionato: Campionato;
        giornate: GiornateLeghe[];
        msgUtente: string;
        minSquadreLega: number;
        maxSquadreLega: number;
        error: {user_message: string};
    }

	 export class Amici {
        cognome: string;
        email: string;
        hasBids: boolean;
        idCliente: number;
        idGruppo: number;
        idSquadra: number;
        nome: string;
        posizione: string;
        modified: boolean;
    }

    export class A {
        a_e: string;
    }

    export class BonusSost {
        BSG: number;
    }

    export class CampionatoConfig {
        avviato: boolean;
        dtPrimaPartita: string;
        dtScadAsta: string;
        id: number;
        idGgCamp: number;
        idGruppo: number;
        nomeCampionato: string;
        nuSquadre: number;
        saldoIniziale: number;
    }

    export class E {
        a_e: string;
    }

    export class Gola {
        GOLA: number;
    }

    export class Golc {
        GOLC: number;
    }

    export class Gold {
        GOLD: number;
    }

    export class Golp {
        GOLP: number;
    }

    export class Gsub {
        gs: number;
    }

    export class MalusSost {
        MSA: number;
    }

    export class Naut {
        aut: number;
    }

    export class Rigseg {
        rs: number;
    }

    export class Rpar {
        rp: number;
    }

    export class Rsba {
        rsb: number;
    }

    export class Zerogol {
        gol_45: number;
    }

    export class Campionato {
        a: A;
        bonus_sost: BonusSost;
        campionatoConfig: CampionatoConfig;
        e: E;
        gola: Gola;
        golc: Golc;
        gold: Gold;
        golp: Golp;
        gsub: Gsub;
        malus_sost: MalusSost;
        naut: Naut;
        rigseg: Rigseg;
        rpar: Rpar;
        rsba: Rsba;
        zerogol: Zerogol;
    }

    export class GiornateLeghe {
        dtPartite: string;
        idGGCamp: number;
    }

    //FINE ROBA DELLE LEGHE

	//Factory
	export class FantaCalcioFactory {
		creaLega: (idCliente: number) => ng.IPromise<any>;
		modificaAmicoLega: (mod_idcliente: number, email: string, index: string, id_presidente: number) => ng.IPromise<any>;
		invitaAmiciLega: (n_squadre: number, id_presidente: number, email: string[]) => ng.IPromise<any>;
		saveDatiLega: (nome_lega: string, modificaLega: string, id_gg_camp: number, n_squadre: number, dt_scad_asta1: string, is: number, 
				id_presidente: number, saldo_iniziale: number,b_gol_portiere: number, b_gol_difensore: number, b_gol_centrocampista: number, b_gol_attaccante: number, 
				b_rigore_segnato: number, b_rigore_parato: number, b_portiere: number, b_allenatore: number, m_gol_subito: number, m_autorete: number, 
				m_rigore_sbagliato: number, m_ammonizione: number, m_espulsione: number, m_allenatore: number) => ng.IPromise<any>;
		getDatiLega: (idGruppo: number, idSquadraPresidente: number) => ng.IPromise<OM.GetDatiLegaResponse>;
		authentication:(username: string, password: string, modoGioco: number, deviceToken?: string
				, deviceId?: string
				, deviceTarget?: string) => ng.IPromise<any>;
		facebookAuthentication: (username: string, password: string, modoGioco: number, token: string, deviceToken?: string
				, deviceId?: string
				, deviceTarget?: string) => ng.IPromise<any>;
		logout: (idCliente: number) => ng.IPromise<OM.LogoutResponse>;
		getSquadre: (idCliente: number, modoGioco: number) => ng.IPromise<OM.GetSquadreResponse>;
		getCalendario: (idSquadra: number) => ng.IPromise<OM.GetCalendarioResponse>;
		getDettaglioGiornata: (idSquadra: number, idPartita: number) => ng.IPromise<OM.GetDettaglioGiornataResponse>;
		getClassifica: (idSquadra: number) => ng.IPromise<OM.GetClassificaResponse>;
		modificaAvatarSquadra: (idCampionato: number, idSquadra: number) => ng.IPromise<OM.ModificaAvatarSquadraResponse>;
		modificaSquadra: (idCampionato: number, idSquadra: number, nomeSquadra: string, idAvatar: number) => ng.IPromise<OM.ModificaSquadraResponse>;
		eliminaSquadra: (idSquadra: number) => ng.IPromise<OM.ModificaSquadraResponse>;
		nuovaSquadra: (idCliente: number, modoGioco: number) => ng.IPromise<OM.NuovaSquadraResponse>;
		gestioneRosa: (idCampionato: number, idSquadra: number) => ng.IPromise<OM.GestioneRosaResponse>;
		modificaRosa: (idSquadra: number, sold: string, bought: string) => ng.IPromise<OM.SuccessModificaRosaResponse>;
		formazioneData: (idCampionato: number, idSquadra: number) => ng.IPromise<OM.FormazioneDataResponse>;
		salvaFormazione: (idCampionato: number, idSquadra: number, tattica: number, formazione: string) => ng.IPromise<OM.SalvaFormazioneResponse>;
		modificaFormazione: (idCampionato: number, idSquadra: number, tattica: number, remove: string, add: string) => ng.IPromise<OM.SalvaFormazioneResponse>;
		cancellaFormazione: (idCampionato: number, idSquadra: number, tattica: number) => ng.IPromise<OM.CancellaFormazioneResponse>;
		getNews: () => ng.IPromise<OM.GetNewsResponse>;
		registraUtente: (email: string, password: string, nome: string, cognome: string, username: string, dataNascita: string, 
				regolamento: number, fl_privacy: number, fl_mktg: number, fl_servizio_mcc: number, fl_servizio_cds: number, 
				fl_articoli_privacy: number, modoGioco: number) => ng.IPromise<OM.RegistrazioneResponse>;
		registraUtenteFB: (email: string, token: string, regolamento: number, fl_privacy: number, fl_mktg: number, fl_servizio_mcc: number, fl_servizio_cds: number, 
				fl_articoli_privacy: number, modoGioco: number) => ng.IPromise<OM.RegistrazioneResponse>;
		getProfilo: (idCliente: number) => ng.IPromise<OM.GetProfiloResponse>;
		modificaProfilo: (idCliente: number, NOME: string, COGNOME: string, DTNASCITA: string, SESSO: string, MAIL: string,
					INDIR: string, CITTA: string, PROV: string, CAP: string, UNSUB: number, MAIL_2: string, INDIRIZZO_SPED: string,
					CAP_SPED: string, CITTA_SPED: string, PROV_SPED: string) => ng.IPromise<OM.ModificaProfiloResponse>;
	}

	export class RosaService {
		setGiocatore: (giocatore: OM.GIOCATORIDISPONIBILI, offerta: number) => void;
		giocatoreRemovedFromRosa: (giocaore: OM.ROSA) => void;
		setGiocatoriDisponibili: (giocatoriDisponibili: OM.GIOCATORIDISPONIBILI) => void;
		getGiocatoriDisponibili: () => OM.GIOCATORIDISPONIBILI[];
		setGiocatoreSelezionato: (giocatoreSelezionato: OM.GIOCATORIDISPONIBILI) => void;
		getPortieri: (sortBy: string, crescente: boolean, squadra: string, costoMin: number, costoMax: number) => OM.GIOCATORIDISPONIBILI[];
		getDifensori: (sortBy: string, crescente: boolean, squadra: string, costoMin: number, costoMax: number) => OM.GIOCATORIDISPONIBILI[];
		getCentrocampisti: (sortBy: string, crescente: boolean, squadra: string, costoMin: number, costoMax: number) => OM.GIOCATORIDISPONIBILI[];
		getAllenatori: (sortBy: string, crescente: boolean, squadra: string, costoMin: number, costoMax: number) => OM.GIOCATORIDISPONIBILI[];
		setRosa: (portieri: OM.ROSA[], difensori: OM.ROSA[], centrocampisti: OM.ROSA[], attaccanti: OM.ROSA[], allenatori?: OM.ROSA) => void;
		getRosa: () => OM.ROSA[];
		getSquadre: () => string[];
		getAttaccanti: (sortBy: string, crescente: boolean, squadra: string, costoMin: number, costoMax: number) => OM.GIOCATORIDISPONIBILI[];
		getGiocatoreSelezionato: () => OM.GIOCATORIDISPONIBILI;
		setGiocatoriVenduti: (venduti: string) => void;
		getGiocatoriVenduti: () => string;
		setGiocatoriAcquistati: (acquistati: string) => void;
		getGiocatoriAcquistati: () => string;
		setLastSelected: (lastSelected: {ruolo: string; index: number}) => void;
		getLastSelected: () => {ruolo: string; index: number};
		setSaldo: (saldo: number) => void;
		getSaldo: () => number;
		setOfferte: (value: boolean) => void;
		getOfferte: () => boolean;
		setPrimaRosa: (value: boolean) => void;
		getPrimaRosa: () => boolean;
		setBloccoRosa: (value: boolean) => void;
		getBloccoRosa: () => boolean;
	}
}


