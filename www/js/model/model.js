/// <reference path="../../../typings/tsd.d.ts" />
var OM;
(function (OM) {
    //Authentication
    var AuthenticationResponse = (function () {
        function AuthenticationResponse() {
        }
        return AuthenticationResponse;
    }());
    OM.AuthenticationResponse = AuthenticationResponse;
    var Success = (function () {
        function Success() {
        }
        return Success;
    }());
    OM.Success = Success;
    var User = (function () {
        function User() {
        }
        return User;
    }());
    OM.User = User;
    ;
    //Logout
    var LogoutResponse = (function () {
        function LogoutResponse() {
        }
        return LogoutResponse;
    }());
    OM.LogoutResponse = LogoutResponse;
    var SuccessLogout = (function () {
        function SuccessLogout() {
        }
        return SuccessLogout;
    }());
    OM.SuccessLogout = SuccessLogout;
    //Error Response
    var ErrorResponse = (function () {
        function ErrorResponse() {
        }
        return ErrorResponse;
    }());
    OM.ErrorResponse = ErrorResponse;
    ;
    //Get Squadre
    var GetSquadreResponse = (function () {
        function GetSquadreResponse() {
        }
        return GetSquadreResponse;
    }());
    OM.GetSquadreResponse = GetSquadreResponse;
    var Links = (function () {
        function Links() {
        }
        return Links;
    }());
    OM.Links = Links;
    var Squadre = (function () {
        function Squadre() {
        }
        return Squadre;
    }());
    OM.Squadre = Squadre;
    var Classifica = (function () {
        function Classifica() {
        }
        return Classifica;
    }());
    OM.Classifica = Classifica;
    var Settimanale = (function () {
        function Settimanale() {
        }
        return Settimanale;
    }());
    OM.Settimanale = Settimanale;
    var TorneoAndata = (function () {
        function TorneoAndata() {
        }
        return TorneoAndata;
    }());
    OM.TorneoAndata = TorneoAndata;
    var TorneoEstivo = (function () {
        function TorneoEstivo() {
        }
        return TorneoEstivo;
    }());
    OM.TorneoEstivo = TorneoEstivo;
    var TorneoRitorno = (function () {
        function TorneoRitorno() {
        }
        return TorneoRitorno;
    }());
    OM.TorneoRitorno = TorneoRitorno;
    var ProssimaPartita = (function () {
        function ProssimaPartita() {
        }
        return ProssimaPartita;
    }());
    OM.ProssimaPartita = ProssimaPartita;
    var SquadreProssimaPartita = (function () {
        function SquadreProssimaPartita() {
        }
        return SquadreProssimaPartita;
    }());
    OM.SquadreProssimaPartita = SquadreProssimaPartita;
    var UltimaPartita = (function () {
        function UltimaPartita() {
        }
        return UltimaPartita;
    }());
    OM.UltimaPartita = UltimaPartita;
    var SquadreUltimaPartita = (function () {
        function SquadreUltimaPartita() {
        }
        return SquadreUltimaPartita;
    }());
    OM.SquadreUltimaPartita = SquadreUltimaPartita;
    //Get Calendario
    var GetCalendarioResponse = (function () {
        function GetCalendarioResponse() {
        }
        return GetCalendarioResponse;
    }());
    OM.GetCalendarioResponse = GetCalendarioResponse;
    var Giornate = (function () {
        function Giornate() {
        }
        return Giornate;
    }());
    OM.Giornate = Giornate;
    //Get Dettaglio
    var GetDettaglioGiornataResponse = (function () {
        function GetDettaglioGiornataResponse() {
        }
        return GetDettaglioGiornataResponse;
    }());
    OM.GetDettaglioGiornataResponse = GetDettaglioGiornataResponse;
    var SquadreDettaglio = (function () {
        function SquadreDettaglio() {
        }
        return SquadreDettaglio;
    }());
    OM.SquadreDettaglio = SquadreDettaglio;
    var Titolari = (function () {
        function Titolari() {
        }
        return Titolari;
    }());
    OM.Titolari = Titolari;
    var Stat2 = (function () {
        function Stat2() {
        }
        return Stat2;
    }());
    OM.Stat2 = Stat2;
    var Riserve = (function () {
        function Riserve() {
        }
        return Riserve;
    }());
    OM.Riserve = Riserve;
    var Stat = (function () {
        function Stat() {
        }
        return Stat;
    }());
    OM.Stat = Stat;
    //Get Classifica
    var GetClassificaResponse = (function () {
        function GetClassificaResponse() {
        }
        return GetClassificaResponse;
    }());
    OM.GetClassificaResponse = GetClassificaResponse;
    var TorneoRitornoClassifica = (function () {
        function TorneoRitornoClassifica() {
        }
        return TorneoRitornoClassifica;
    }());
    OM.TorneoRitornoClassifica = TorneoRitornoClassifica;
    var TorneoEstivoClassifica = (function () {
        function TorneoEstivoClassifica() {
        }
        return TorneoEstivoClassifica;
    }());
    OM.TorneoEstivoClassifica = TorneoEstivoClassifica;
    var TorneoAndataClassifica = (function () {
        function TorneoAndataClassifica() {
        }
        return TorneoAndataClassifica;
    }());
    OM.TorneoAndataClassifica = TorneoAndataClassifica;
    var SettimanaleClassifica = (function () {
        function SettimanaleClassifica() {
        }
        return SettimanaleClassifica;
    }());
    OM.SettimanaleClassifica = SettimanaleClassifica;
    var TorneoLegheClassifica = (function () {
        function TorneoLegheClassifica() {
        }
        return TorneoLegheClassifica;
    }());
    OM.TorneoLegheClassifica = TorneoLegheClassifica;
    //Add Squadra ModificaAvatar
    var ModificaAvatarSquadraResponse = (function () {
        function ModificaAvatarSquadraResponse() {
        }
        return ModificaAvatarSquadraResponse;
    }());
    OM.ModificaAvatarSquadraResponse = ModificaAvatarSquadraResponse;
    var SquadreAddSquadra = (function () {
        function SquadreAddSquadra() {
        }
        return SquadreAddSquadra;
    }());
    OM.SquadreAddSquadra = SquadreAddSquadra;
    var AvatarList = (function () {
        function AvatarList() {
        }
        return AvatarList;
    }());
    OM.AvatarList = AvatarList;
    //Add Squadra ModificaSquadra
    var ModificaSquadraResponse = (function () {
        function ModificaSquadraResponse() {
        }
        return ModificaSquadraResponse;
    }());
    OM.ModificaSquadraResponse = ModificaSquadraResponse;
    var SuccessModificaSquadra = (function () {
        function SuccessModificaSquadra() {
        }
        return SuccessModificaSquadra;
    }());
    OM.SuccessModificaSquadra = SuccessModificaSquadra;
    var SquadraModificaSquadra = (function () {
        function SquadraModificaSquadra() {
        }
        return SquadraModificaSquadra;
    }());
    OM.SquadraModificaSquadra = SquadraModificaSquadra;
    //Add Squadra NuovaSquadra
    var NuovaSquadraResponse = (function () {
        function NuovaSquadraResponse() {
        }
        return NuovaSquadraResponse;
    }());
    OM.NuovaSquadraResponse = NuovaSquadraResponse;
    var SquadreNuovaSquadra = (function () {
        function SquadreNuovaSquadra() {
        }
        return SquadreNuovaSquadra;
    }());
    OM.SquadreNuovaSquadra = SquadreNuovaSquadra;
    //Gestione Rosa
    var GestioneRosaResponse = (function () {
        function GestioneRosaResponse() {
        }
        return GestioneRosaResponse;
    }());
    OM.GestioneRosaResponse = GestioneRosaResponse;
    var GIOCATORIDISPONIBILI = (function () {
        function GIOCATORIDISPONIBILI() {
        }
        return GIOCATORIDISPONIBILI;
    }());
    OM.GIOCATORIDISPONIBILI = GIOCATORIDISPONIBILI;
    var ROSA = (function () {
        function ROSA() {
        }
        return ROSA;
    }());
    OM.ROSA = ROSA;
    //Modifica rosa Response
    var SuccessModificaRosaResponse = (function () {
        function SuccessModificaRosaResponse() {
        }
        return SuccessModificaRosaResponse;
    }());
    OM.SuccessModificaRosaResponse = SuccessModificaRosaResponse;
    var SuccessGestioneRosa = (function () {
        function SuccessGestioneRosa() {
        }
        return SuccessGestioneRosa;
    }());
    OM.SuccessGestioneRosa = SuccessGestioneRosa;
    var ErrorModificaRosaResponse = (function () {
        function ErrorModificaRosaResponse() {
        }
        return ErrorModificaRosaResponse;
    }());
    OM.ErrorModificaRosaResponse = ErrorModificaRosaResponse;
    var ErrorGestioneRosa = (function () {
        function ErrorGestioneRosa() {
        }
        return ErrorGestioneRosa;
    }());
    OM.ErrorGestioneRosa = ErrorGestioneRosa;
    //Formazione data
    var FormazioneDataResponse = (function () {
        function FormazioneDataResponse() {
        }
        return FormazioneDataResponse;
    }());
    OM.FormazioneDataResponse = FormazioneDataResponse;
    var TatticheFormazioneData = (function () {
        function TatticheFormazioneData() {
        }
        return TatticheFormazioneData;
    }());
    OM.TatticheFormazioneData = TatticheFormazioneData;
    var DESIGN = (function () {
        function DESIGN() {
        }
        return DESIGN;
    }());
    OM.DESIGN = DESIGN;
    var RosaFormazioneData = (function () {
        function RosaFormazioneData() {
        }
        return RosaFormazioneData;
    }());
    OM.RosaFormazioneData = RosaFormazioneData;
    //Salva Formazione Response
    var SalvaFormazioneResponse = (function () {
        function SalvaFormazioneResponse() {
        }
        return SalvaFormazioneResponse;
    }());
    OM.SalvaFormazioneResponse = SalvaFormazioneResponse;
    var SuccessSalvaFormazione = (function () {
        function SuccessSalvaFormazione() {
        }
        return SuccessSalvaFormazione;
    }());
    OM.SuccessSalvaFormazione = SuccessSalvaFormazione;
    //Cancella formazione
    var CancellaFormazioneResponse = (function () {
        function CancellaFormazioneResponse() {
        }
        return CancellaFormazioneResponse;
    }());
    OM.CancellaFormazioneResponse = CancellaFormazioneResponse;
    var SuccessCancellaFormazione = (function () {
        function SuccessCancellaFormazione() {
        }
        return SuccessCancellaFormazione;
    }());
    OM.SuccessCancellaFormazione = SuccessCancellaFormazione;
    //registrazione
    var RegistrazioneResponse = (function () {
        function RegistrazioneResponse() {
        }
        return RegistrazioneResponse;
    }());
    OM.RegistrazioneResponse = RegistrazioneResponse;
    var SuccessRegistrazione = (function () {
        function SuccessRegistrazione() {
        }
        return SuccessRegistrazione;
    }());
    OM.SuccessRegistrazione = SuccessRegistrazione;
    var UserRegistrazione = (function () {
        function UserRegistrazione() {
        }
        return UserRegistrazione;
    }());
    OM.UserRegistrazione = UserRegistrazione;
    //getProfilo
    var GetProfiloResponse = (function () {
        function GetProfiloResponse() {
        }
        return GetProfiloResponse;
    }());
    OM.GetProfiloResponse = GetProfiloResponse;
    var SuccessGetProfilo = (function () {
        function SuccessGetProfilo() {
        }
        return SuccessGetProfilo;
    }());
    OM.SuccessGetProfilo = SuccessGetProfilo;
    var UserGetProfilo = (function () {
        function UserGetProfilo() {
        }
        return UserGetProfilo;
    }());
    OM.UserGetProfilo = UserGetProfilo;
    //modifica profilo
    var ModificaProfiloResponse = (function () {
        function ModificaProfiloResponse() {
        }
        return ModificaProfiloResponse;
    }());
    OM.ModificaProfiloResponse = ModificaProfiloResponse;
    var SuccessModificaProfilo = (function () {
        function SuccessModificaProfilo() {
        }
        return SuccessModificaProfilo;
    }());
    OM.SuccessModificaProfilo = SuccessModificaProfilo;
    //Get News
    var GetNewsResponse = (function () {
        function GetNewsResponse() {
        }
        return GetNewsResponse;
    }());
    OM.GetNewsResponse = GetNewsResponse;
    var News = (function () {
        function News() {
        }
        return News;
    }());
    OM.News = News;
    //TUUUUUTTA ROBA DELLE LEGHE
    //Get Dati Lega
    var GetDatiLegaResponse = (function () {
        function GetDatiLegaResponse() {
        }
        return GetDatiLegaResponse;
    }());
    OM.GetDatiLegaResponse = GetDatiLegaResponse;
    var Amici = (function () {
        function Amici() {
        }
        return Amici;
    }());
    OM.Amici = Amici;
    var A = (function () {
        function A() {
        }
        return A;
    }());
    OM.A = A;
    var BonusSost = (function () {
        function BonusSost() {
        }
        return BonusSost;
    }());
    OM.BonusSost = BonusSost;
    var CampionatoConfig = (function () {
        function CampionatoConfig() {
        }
        return CampionatoConfig;
    }());
    OM.CampionatoConfig = CampionatoConfig;
    var E = (function () {
        function E() {
        }
        return E;
    }());
    OM.E = E;
    var Gola = (function () {
        function Gola() {
        }
        return Gola;
    }());
    OM.Gola = Gola;
    var Golc = (function () {
        function Golc() {
        }
        return Golc;
    }());
    OM.Golc = Golc;
    var Gold = (function () {
        function Gold() {
        }
        return Gold;
    }());
    OM.Gold = Gold;
    var Golp = (function () {
        function Golp() {
        }
        return Golp;
    }());
    OM.Golp = Golp;
    var Gsub = (function () {
        function Gsub() {
        }
        return Gsub;
    }());
    OM.Gsub = Gsub;
    var MalusSost = (function () {
        function MalusSost() {
        }
        return MalusSost;
    }());
    OM.MalusSost = MalusSost;
    var Naut = (function () {
        function Naut() {
        }
        return Naut;
    }());
    OM.Naut = Naut;
    var Rigseg = (function () {
        function Rigseg() {
        }
        return Rigseg;
    }());
    OM.Rigseg = Rigseg;
    var Rpar = (function () {
        function Rpar() {
        }
        return Rpar;
    }());
    OM.Rpar = Rpar;
    var Rsba = (function () {
        function Rsba() {
        }
        return Rsba;
    }());
    OM.Rsba = Rsba;
    var Zerogol = (function () {
        function Zerogol() {
        }
        return Zerogol;
    }());
    OM.Zerogol = Zerogol;
    var Campionato = (function () {
        function Campionato() {
        }
        return Campionato;
    }());
    OM.Campionato = Campionato;
    var GiornateLeghe = (function () {
        function GiornateLeghe() {
        }
        return GiornateLeghe;
    }());
    OM.GiornateLeghe = GiornateLeghe;
    //FINE ROBA DELLE LEGHE
    //Factory
    var FantaCalcioFactory = (function () {
        function FantaCalcioFactory() {
        }
        return FantaCalcioFactory;
    }());
    OM.FantaCalcioFactory = FantaCalcioFactory;
    var RosaService = (function () {
        function RosaService() {
        }
        return RosaService;
    }());
    OM.RosaService = RosaService;
})(OM || (OM = {}));
//# sourceMappingURL=model.js.map