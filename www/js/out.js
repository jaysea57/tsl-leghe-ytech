/// <reference path="../../../typings/tsd.d.ts" />
var OM;
(function (OM) {
    //Authentication
    var AuthenticationResponse = (function () {
        function AuthenticationResponse() {
        }
        return AuthenticationResponse;
    })();
    OM.AuthenticationResponse = AuthenticationResponse;
    var Success = (function () {
        function Success() {
        }
        return Success;
    })();
    OM.Success = Success;
    var User = (function () {
        function User() {
        }
        return User;
    })();
    OM.User = User;
    ;
    //Logout
    var LogoutResponse = (function () {
        function LogoutResponse() {
        }
        return LogoutResponse;
    })();
    OM.LogoutResponse = LogoutResponse;
    var SuccessLogout = (function () {
        function SuccessLogout() {
        }
        return SuccessLogout;
    })();
    OM.SuccessLogout = SuccessLogout;
    //Error Response
    var ErrorResponse = (function () {
        function ErrorResponse() {
        }
        return ErrorResponse;
    })();
    OM.ErrorResponse = ErrorResponse;
    ;
    //Get Squadre
    var GetSquadreResponse = (function () {
        function GetSquadreResponse() {
        }
        return GetSquadreResponse;
    })();
    OM.GetSquadreResponse = GetSquadreResponse;
    var Links = (function () {
        function Links() {
        }
        return Links;
    })();
    OM.Links = Links;
    var Squadre = (function () {
        function Squadre() {
        }
        return Squadre;
    })();
    OM.Squadre = Squadre;
    var Classifica = (function () {
        function Classifica() {
        }
        return Classifica;
    })();
    OM.Classifica = Classifica;
    var Settimanale = (function () {
        function Settimanale() {
        }
        return Settimanale;
    })();
    OM.Settimanale = Settimanale;
    var TorneoAndata = (function () {
        function TorneoAndata() {
        }
        return TorneoAndata;
    })();
    OM.TorneoAndata = TorneoAndata;
    var TorneoEstivo = (function () {
        function TorneoEstivo() {
        }
        return TorneoEstivo;
    })();
    OM.TorneoEstivo = TorneoEstivo;
    var TorneoRitorno = (function () {
        function TorneoRitorno() {
        }
        return TorneoRitorno;
    })();
    OM.TorneoRitorno = TorneoRitorno;
    var ProssimaPartita = (function () {
        function ProssimaPartita() {
        }
        return ProssimaPartita;
    })();
    OM.ProssimaPartita = ProssimaPartita;
    var SquadreProssimaPartita = (function () {
        function SquadreProssimaPartita() {
        }
        return SquadreProssimaPartita;
    })();
    OM.SquadreProssimaPartita = SquadreProssimaPartita;
    var UltimaPartita = (function () {
        function UltimaPartita() {
        }
        return UltimaPartita;
    })();
    OM.UltimaPartita = UltimaPartita;
    var SquadreUltimaPartita = (function () {
        function SquadreUltimaPartita() {
        }
        return SquadreUltimaPartita;
    })();
    OM.SquadreUltimaPartita = SquadreUltimaPartita;
    //Get Calendario
    var GetCalendarioResponse = (function () {
        function GetCalendarioResponse() {
        }
        return GetCalendarioResponse;
    })();
    OM.GetCalendarioResponse = GetCalendarioResponse;
    var Giornate = (function () {
        function Giornate() {
        }
        return Giornate;
    })();
    OM.Giornate = Giornate;
    //Get Dettaglio
    var GetDettaglioGiornataResponse = (function () {
        function GetDettaglioGiornataResponse() {
        }
        return GetDettaglioGiornataResponse;
    })();
    OM.GetDettaglioGiornataResponse = GetDettaglioGiornataResponse;
    var SquadreDettaglio = (function () {
        function SquadreDettaglio() {
        }
        return SquadreDettaglio;
    })();
    OM.SquadreDettaglio = SquadreDettaglio;
    var Titolari = (function () {
        function Titolari() {
        }
        return Titolari;
    })();
    OM.Titolari = Titolari;
    var Stat2 = (function () {
        function Stat2() {
        }
        return Stat2;
    })();
    OM.Stat2 = Stat2;
    var Riserve = (function () {
        function Riserve() {
        }
        return Riserve;
    })();
    OM.Riserve = Riserve;
    var Stat = (function () {
        function Stat() {
        }
        return Stat;
    })();
    OM.Stat = Stat;
    //Get Classifica
    var GetClassificaResponse = (function () {
        function GetClassificaResponse() {
        }
        return GetClassificaResponse;
    })();
    OM.GetClassificaResponse = GetClassificaResponse;
    var TorneoRitornoClassifica = (function () {
        function TorneoRitornoClassifica() {
        }
        return TorneoRitornoClassifica;
    })();
    OM.TorneoRitornoClassifica = TorneoRitornoClassifica;
    var TorneoEstivoClassifica = (function () {
        function TorneoEstivoClassifica() {
        }
        return TorneoEstivoClassifica;
    })();
    OM.TorneoEstivoClassifica = TorneoEstivoClassifica;
    var TorneoAndataClassifica = (function () {
        function TorneoAndataClassifica() {
        }
        return TorneoAndataClassifica;
    })();
    OM.TorneoAndataClassifica = TorneoAndataClassifica;
    var SettimanaleClassifica = (function () {
        function SettimanaleClassifica() {
        }
        return SettimanaleClassifica;
    })();
    OM.SettimanaleClassifica = SettimanaleClassifica;
    var TorneoLegheClassifica = (function () {
        function TorneoLegheClassifica() {
        }
        return TorneoLegheClassifica;
    })();
    OM.TorneoLegheClassifica = TorneoLegheClassifica;
    //Add Squadra ModificaAvatar
    var ModificaAvatarSquadraResponse = (function () {
        function ModificaAvatarSquadraResponse() {
        }
        return ModificaAvatarSquadraResponse;
    })();
    OM.ModificaAvatarSquadraResponse = ModificaAvatarSquadraResponse;
    var SquadreAddSquadra = (function () {
        function SquadreAddSquadra() {
        }
        return SquadreAddSquadra;
    })();
    OM.SquadreAddSquadra = SquadreAddSquadra;
    var AvatarList = (function () {
        function AvatarList() {
        }
        return AvatarList;
    })();
    OM.AvatarList = AvatarList;
    //Add Squadra ModificaSquadra
    var ModificaSquadraResponse = (function () {
        function ModificaSquadraResponse() {
        }
        return ModificaSquadraResponse;
    })();
    OM.ModificaSquadraResponse = ModificaSquadraResponse;
    var SuccessModificaSquadra = (function () {
        function SuccessModificaSquadra() {
        }
        return SuccessModificaSquadra;
    })();
    OM.SuccessModificaSquadra = SuccessModificaSquadra;
    var SquadraModificaSquadra = (function () {
        function SquadraModificaSquadra() {
        }
        return SquadraModificaSquadra;
    })();
    OM.SquadraModificaSquadra = SquadraModificaSquadra;
    //Add Squadra NuovaSquadra
    var NuovaSquadraResponse = (function () {
        function NuovaSquadraResponse() {
        }
        return NuovaSquadraResponse;
    })();
    OM.NuovaSquadraResponse = NuovaSquadraResponse;
    var SquadreNuovaSquadra = (function () {
        function SquadreNuovaSquadra() {
        }
        return SquadreNuovaSquadra;
    })();
    OM.SquadreNuovaSquadra = SquadreNuovaSquadra;
    //Gestione Rosa
    var GestioneRosaResponse = (function () {
        function GestioneRosaResponse() {
        }
        return GestioneRosaResponse;
    })();
    OM.GestioneRosaResponse = GestioneRosaResponse;
    var GIOCATORIDISPONIBILI = (function () {
        function GIOCATORIDISPONIBILI() {
        }
        return GIOCATORIDISPONIBILI;
    })();
    OM.GIOCATORIDISPONIBILI = GIOCATORIDISPONIBILI;
    var ROSA = (function () {
        function ROSA() {
        }
        return ROSA;
    })();
    OM.ROSA = ROSA;
    //Modifica rosa Response
    var SuccessModificaRosaResponse = (function () {
        function SuccessModificaRosaResponse() {
        }
        return SuccessModificaRosaResponse;
    })();
    OM.SuccessModificaRosaResponse = SuccessModificaRosaResponse;
    var SuccessGestioneRosa = (function () {
        function SuccessGestioneRosa() {
        }
        return SuccessGestioneRosa;
    })();
    OM.SuccessGestioneRosa = SuccessGestioneRosa;
    var ErrorModificaRosaResponse = (function () {
        function ErrorModificaRosaResponse() {
        }
        return ErrorModificaRosaResponse;
    })();
    OM.ErrorModificaRosaResponse = ErrorModificaRosaResponse;
    var ErrorGestioneRosa = (function () {
        function ErrorGestioneRosa() {
        }
        return ErrorGestioneRosa;
    })();
    OM.ErrorGestioneRosa = ErrorGestioneRosa;
    //Formazione data
    var FormazioneDataResponse = (function () {
        function FormazioneDataResponse() {
        }
        return FormazioneDataResponse;
    })();
    OM.FormazioneDataResponse = FormazioneDataResponse;
    var TatticheFormazioneData = (function () {
        function TatticheFormazioneData() {
        }
        return TatticheFormazioneData;
    })();
    OM.TatticheFormazioneData = TatticheFormazioneData;
    var DESIGN = (function () {
        function DESIGN() {
        }
        return DESIGN;
    })();
    OM.DESIGN = DESIGN;
    var RosaFormazioneData = (function () {
        function RosaFormazioneData() {
        }
        return RosaFormazioneData;
    })();
    OM.RosaFormazioneData = RosaFormazioneData;
    //Salva Formazione Response
    var SalvaFormazioneResponse = (function () {
        function SalvaFormazioneResponse() {
        }
        return SalvaFormazioneResponse;
    })();
    OM.SalvaFormazioneResponse = SalvaFormazioneResponse;
    var SuccessSalvaFormazione = (function () {
        function SuccessSalvaFormazione() {
        }
        return SuccessSalvaFormazione;
    })();
    OM.SuccessSalvaFormazione = SuccessSalvaFormazione;
    //Cancella formazione
    var CancellaFormazioneResponse = (function () {
        function CancellaFormazioneResponse() {
        }
        return CancellaFormazioneResponse;
    })();
    OM.CancellaFormazioneResponse = CancellaFormazioneResponse;
    var SuccessCancellaFormazione = (function () {
        function SuccessCancellaFormazione() {
        }
        return SuccessCancellaFormazione;
    })();
    OM.SuccessCancellaFormazione = SuccessCancellaFormazione;
    //registrazione
    var RegistrazioneResponse = (function () {
        function RegistrazioneResponse() {
        }
        return RegistrazioneResponse;
    })();
    OM.RegistrazioneResponse = RegistrazioneResponse;
    var SuccessRegistrazione = (function () {
        function SuccessRegistrazione() {
        }
        return SuccessRegistrazione;
    })();
    OM.SuccessRegistrazione = SuccessRegistrazione;
    var UserRegistrazione = (function () {
        function UserRegistrazione() {
        }
        return UserRegistrazione;
    })();
    OM.UserRegistrazione = UserRegistrazione;
    //getProfilo
    var GetProfiloResponse = (function () {
        function GetProfiloResponse() {
        }
        return GetProfiloResponse;
    })();
    OM.GetProfiloResponse = GetProfiloResponse;
    var SuccessGetProfilo = (function () {
        function SuccessGetProfilo() {
        }
        return SuccessGetProfilo;
    })();
    OM.SuccessGetProfilo = SuccessGetProfilo;
    var UserGetProfilo = (function () {
        function UserGetProfilo() {
        }
        return UserGetProfilo;
    })();
    OM.UserGetProfilo = UserGetProfilo;
    //modifica profilo
    var ModificaProfiloResponse = (function () {
        function ModificaProfiloResponse() {
        }
        return ModificaProfiloResponse;
    })();
    OM.ModificaProfiloResponse = ModificaProfiloResponse;
    var SuccessModificaProfilo = (function () {
        function SuccessModificaProfilo() {
        }
        return SuccessModificaProfilo;
    })();
    OM.SuccessModificaProfilo = SuccessModificaProfilo;
    //Get News
    var GetNewsResponse = (function () {
        function GetNewsResponse() {
        }
        return GetNewsResponse;
    })();
    OM.GetNewsResponse = GetNewsResponse;
    var News = (function () {
        function News() {
        }
        return News;
    })();
    OM.News = News;
    //TUUUUUTTA ROBA DELLE LEGHE
    //Get Dati Lega
    var GetDatiLegaResponse = (function () {
        function GetDatiLegaResponse() {
        }
        return GetDatiLegaResponse;
    })();
    OM.GetDatiLegaResponse = GetDatiLegaResponse;
    var Amici = (function () {
        function Amici() {
        }
        return Amici;
    })();
    OM.Amici = Amici;
    var A = (function () {
        function A() {
        }
        return A;
    })();
    OM.A = A;
    var BonusSost = (function () {
        function BonusSost() {
        }
        return BonusSost;
    })();
    OM.BonusSost = BonusSost;
    var CampionatoConfig = (function () {
        function CampionatoConfig() {
        }
        return CampionatoConfig;
    })();
    OM.CampionatoConfig = CampionatoConfig;
    var E = (function () {
        function E() {
        }
        return E;
    })();
    OM.E = E;
    var Gola = (function () {
        function Gola() {
        }
        return Gola;
    })();
    OM.Gola = Gola;
    var Golc = (function () {
        function Golc() {
        }
        return Golc;
    })();
    OM.Golc = Golc;
    var Gold = (function () {
        function Gold() {
        }
        return Gold;
    })();
    OM.Gold = Gold;
    var Golp = (function () {
        function Golp() {
        }
        return Golp;
    })();
    OM.Golp = Golp;
    var Gsub = (function () {
        function Gsub() {
        }
        return Gsub;
    })();
    OM.Gsub = Gsub;
    var MalusSost = (function () {
        function MalusSost() {
        }
        return MalusSost;
    })();
    OM.MalusSost = MalusSost;
    var Naut = (function () {
        function Naut() {
        }
        return Naut;
    })();
    OM.Naut = Naut;
    var Rigseg = (function () {
        function Rigseg() {
        }
        return Rigseg;
    })();
    OM.Rigseg = Rigseg;
    var Rpar = (function () {
        function Rpar() {
        }
        return Rpar;
    })();
    OM.Rpar = Rpar;
    var Rsba = (function () {
        function Rsba() {
        }
        return Rsba;
    })();
    OM.Rsba = Rsba;
    var Zerogol = (function () {
        function Zerogol() {
        }
        return Zerogol;
    })();
    OM.Zerogol = Zerogol;
    var Campionato = (function () {
        function Campionato() {
        }
        return Campionato;
    })();
    OM.Campionato = Campionato;
    var GiornateLeghe = (function () {
        function GiornateLeghe() {
        }
        return GiornateLeghe;
    })();
    OM.GiornateLeghe = GiornateLeghe;
    //FINE ROBA DELLE LEGHE
    //Factory
    var FantaCalcioFactory = (function () {
        function FantaCalcioFactory() {
        }
        return FantaCalcioFactory;
    })();
    OM.FantaCalcioFactory = FantaCalcioFactory;
    var RosaService = (function () {
        function RosaService() {
        }
        return RosaService;
    })();
    OM.RosaService = RosaService;
})(OM || (OM = {}));
/// <reference path="../../typings/tsd.d.ts" />
/// <reference path="model/model.ts" />
(function () {
    var app = angular.module('Services', []);
    var torneoClassicoUrl = window.location.host.indexOf('192') == 0 || window.location.host.indexOf('localhost') == 0
        ? 'http://localhost:9292/league.tsport.yland.it/'
        : 'http://league.tsport.yland.it/'; //ESTERNA
    //:'http://devts.tuttosport.com/';       						//RETE AZIENDALE Y TECH
    //MISTER CALCIO CUP CLASSICO
    //?'http://localhost:9292/gp.mr.yland.it/'
    //:'http://gp.mr.yland.it/'									//RETE ESTERNA
    //:'http://devgp.corrieredellosport.it/'                       	//RETE AZIENDALE Y TECH
    ;
    var torneoGironiUrl = window.location.host.indexOf('192') == 0 || window.location.host.indexOf('localhost') == 0
        ? 'http://localhost:9292/gironi.tsport.yland.it/'
        : 'http://gironi.tsport.yland.it/'; //ESTERNA
    //:'http://devtsgironi.tuttosport.com/';       				//RETE AZIENDALE Y TECH
    //MISTER CALCIO CUP GIRONI modo_gioco = 1
    //?'http://localhost:9292/www.mr.yland.it/'
    //:'http://www.mr.yland.it/';								//ESTERNA
    //:'http://devmcc.corrieredellosport.it/'                       	//RETE AZIENDALE Y TECH
    ;
    var torneoLegheUrl = window.location.host.indexOf('192') == 0 || window.location.host.indexOf('localhost') == 0
        ? 'http://localhost:9292/www.mr.yland.it/'
        : 'http://www.mr.yland.it/'; //ESTERNA
    //:'http://devtsgironi.tuttosport.com/';       				//RETE AZIENDALE Y TECH
    //MISTER CALCIO CUP LEGHE modo_gioco = 2  
    //?'http://localhost:9292/www.mr.yland.it/'
    //:'http://www.mr.yland.it/';								//ESTERNA
    //:'http://devmcc.corrieredellosport.it/'					//RETE AZIENDALE Y TECH
    ;
    var torneoPlayoffUrl = window.location.host.indexOf('192') == 0 || window.location.host.indexOf('localhost') == 0
        ? 'http://localhost:9292/www.mr.yland.it/'
        : 'http://www.mr.yland.it/'; //ESTERNA
    //:'http://devtsgironi.tuttosport.com/';       				//RETE AZIENDALE Y TECH
    //MISTER CALCIO CUP PLAYOFF modo_gioco = 4
    //?'http://localhost:9292/www.mr.yland.it/'
    //:'http://www.mr.yland.it/';								//ESTERNA
    //:'http://devmcc.corrieredellosport.it/'					//RETE AZIENDALE Y TECH
    ;
    app.factory('FantaCalcioServices', ['$q', '$http', '$rootScope', function ($q, $http, $rootScope) {
            return {
                creaLega: function (idCliente) {
                    var urlServizi = torneoLegheUrl;
                    var deferred = $q.defer();
                    $http.get(urlServizi + 'mobile/lega.json', { params: {
                            action: 'creaLega',
                            idc: idCliente,
                        }
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                modificaAmicoLega: function (mod_idcliente, email, index, id_presidente) {
                    var urlServizi = torneoLegheUrl;
                    var deferred = $q.defer();
                    $http.get(urlServizi + 'mobile/amico.json', { params: {
                            action: 'modificaAmico',
                            mod_idcliente: mod_idcliente,
                            email: email,
                            index: index,
                            is: id_presidente
                        }
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                invitaAmiciLega: function (n_squadre, id_presidente, email) {
                    var urlServizi = torneoLegheUrl;
                    var params = {
                        params: {
                            action: 'invitaAmico',
                            n_squadre: n_squadre,
                            is: id_presidente
                        }
                    };
                    email.forEach(function (item, i) {
                        params.params['email' + i] = item;
                    });
                    var deferred = $q.defer();
                    $http.get(urlServizi + 'mobile/amico.json', params).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                saveDatiLega: function (nome_lega, modificaLega, id_gg_camp, n_squadre, dt_scad_asta1, is, id_presidente, saldo_iniziale, b_gol_portiere, b_gol_difensore, b_gol_centrocampista, b_gol_attaccante, b_rigore_segnato, b_rigore_parato, b_portiere, b_allenatore, m_gol_subito, m_autorete, m_rigore_sbagliato, m_ammonizione, m_espulsione, m_allenatore) {
                    var urlServizi = torneoLegheUrl;
                    var deferred = $q.defer();
                    $http.get(urlServizi + 'mobile/lega.json', { params: {
                            action: 'saveLega',
                            nome_lega: nome_lega,
                            modificaLega: modificaLega,
                            id_gg_camp: id_gg_camp,
                            n_squadre: n_squadre,
                            dt_scad_asta1: dt_scad_asta1,
                            is: is,
                            id_cliente: id_presidente,
                            saldo_iniziale: saldo_iniziale,
                            b_gol_portiere: b_gol_portiere,
                            b_gol_difensore: b_gol_difensore,
                            b_gol_centrocampista: b_gol_centrocampista,
                            b_gol_attaccante: b_gol_attaccante,
                            b_rigore_segnato: b_rigore_segnato,
                            b_rigore_parato: b_rigore_parato,
                            b_portiere: b_portiere,
                            b_allenatore: b_allenatore,
                            m_gol_subito: m_gol_subito,
                            m_autorete: m_autorete,
                            m_rigore_sbagliato: m_rigore_sbagliato,
                            m_ammonizione: m_ammonizione,
                            m_espulsione: m_espulsione,
                            m_allenatore: m_allenatore
                        }
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                getDatiLega: function (idGruppo, idSquadraPresidente) {
                    var urlServizi = torneoLegheUrl;
                    var deferred = $q.defer();
                    $http.get(urlServizi + 'mobile/lega.json', { params: {
                            action: 'getDatiLega',
                            id_cliente: idGruppo,
                            is: idSquadraPresidente
                        } }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                authentication: function (username, password, modoGioco) {
                    var urlServizi;
                    //var modo = parseInt($rootScope.modoGioco);
                    switch (parseInt(modoGioco)) {
                        case 3:
                            urlServizi = torneoClassicoUrl;
                            break;
                        case 1:
                            urlServizi = torneoGironiUrl;
                            break;
                        case 2:
                            urlServizi = torneoLegheUrl;
                            break;
                        case 4:
                            urlServizi = torneoPlayoffUrl;
                            break;
                    }
                    var deferred = $q.defer();
                    $http.get(urlServizi + 'mobile/authentication.json', { params: {
                            os: 'ios',
                            appversion: '1.0',
                            tipologin: 'sso',
                            password: password,
                            action: 'login',
                            modo_gioco: modoGioco,
                            username: username
                        } }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                facebookAuthentication: function (username, password, modoGioco, token) {
                    var urlServizi;
                    //var modo = parseInt($rootScope.modoGioco);
                    switch (parseInt(modoGioco)) {
                        case 3:
                            urlServizi = torneoClassicoUrl;
                            break;
                        case 1:
                            urlServizi = torneoGironiUrl;
                            break;
                        case 2:
                            urlServizi = torneoLegheUrl;
                            break;
                        case 4:
                            urlServizi = torneoPlayoffUrl;
                            break;
                    }
                    var deferred = $q.defer();
                    $http.get(urlServizi + 'mobile/authentication.json', { params: {
                            os: 'ios',
                            appversion: '1.0',
                            tipologin: 'facebook',
                            password: password,
                            action: 'login',
                            token: token,
                            modo_gioco: modoGioco,
                            username: username
                        } }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                logout: function (idCliente, idCampionato) {
                    var urlServizi;
                    var modo = parseInt($rootScope.modoGioco);
                    switch (modo) {
                        case 3:
                            urlServizi = torneoClassicoUrl;
                            break;
                        case 1:
                            urlServizi = torneoGironiUrl;
                            break;
                        case 2:
                            urlServizi = torneoLegheUrl;
                            break;
                        case 4:
                            urlServizi = torneoPlayoffUrl;
                            break;
                    }
                    var deferred = $q.defer();
                    $http.get(urlServizi + 'mobile/login.json', {
                        params: {
                            action: 'logout',
                            ic: idCampionato,
                            modo_gioco: 3,
                            id_cliente: idCliente
                        }
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                getSquadre: function (idCliente, modoGioco) {
                    var urlServizi;
                    //var modo = parseInt($rootScope.modoGioco);
                    switch (parseInt(modoGioco)) {
                        case 3:
                            urlServizi = torneoClassicoUrl;
                            break;
                        case 1:
                            urlServizi = torneoGironiUrl;
                            break;
                        case 2:
                            urlServizi = torneoLegheUrl;
                            break;
                        case 4:
                            urlServizi = torneoPlayoffUrl;
                            break;
                    }
                    var deferred = $q.defer();
                    $http.get(urlServizi + 'mobile/home.json', {
                        params: {
                            ic: idCliente,
                            action: 'getSquadre',
                            modo_gioco: modoGioco
                        }
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                getCalendario: function (idSquadra) {
                    var urlServizi;
                    var modo = parseInt($rootScope.modoGioco);
                    switch (modo) {
                        case 3:
                            urlServizi = torneoClassicoUrl;
                            break;
                        case 1:
                            urlServizi = torneoGironiUrl;
                            break;
                        case 2:
                            urlServizi = torneoLegheUrl;
                            break;
                        case 4:
                            urlServizi = torneoPlayoffUrl;
                            break;
                    }
                    var deferred = $q.defer();
                    $http.get(urlServizi + 'mobile/Calendario.json', {
                        params: {
                            is: idSquadra,
                            action: 'getCalendario'
                        }
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                getDettaglioGiornata: function (idSquadra, idPartita) {
                    var urlServizi;
                    var modo = parseInt($rootScope.modoGioco);
                    switch (modo) {
                        case 3:
                            urlServizi = torneoClassicoUrl;
                            break;
                        case 1:
                            urlServizi = torneoGironiUrl;
                            break;
                        case 2:
                            urlServizi = torneoLegheUrl;
                            break;
                        case 4:
                            urlServizi = torneoPlayoffUrl;
                            break;
                    }
                    var deferred = $q.defer();
                    $http.get(urlServizi + 'mobile/tabellino.json', {
                        params: {
                            ip: idPartita,
                            is: idSquadra,
                            action: 'getDettaglio'
                        }
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                getClassifica: function (idSquadra) {
                    var urlServizi;
                    var modo = parseInt($rootScope.modoGioco);
                    switch (modo) {
                        case 3:
                            urlServizi = torneoClassicoUrl;
                            break;
                        case 1:
                            urlServizi = torneoGironiUrl;
                            break;
                        case 2:
                            urlServizi = torneoLegheUrl;
                            break;
                        case 4:
                            urlServizi = torneoPlayoffUrl;
                            break;
                    }
                    var deferred = $q.defer();
                    $http.get(urlServizi + 'mobile/classifica.json', {
                        params: {
                            is: idSquadra,
                            action: 'getClassifica'
                        }
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                modificaAvatarSquadra: function (idCampionato, idSquadra) {
                    var urlServizi;
                    var modo = parseInt($rootScope.modoGioco);
                    switch (modo) {
                        case 3:
                            urlServizi = torneoClassicoUrl;
                            break;
                        case 1:
                            urlServizi = torneoGironiUrl;
                            break;
                        case 2:
                            urlServizi = torneoLegheUrl;
                            break;
                        case 4:
                            urlServizi = torneoPlayoffUrl;
                            break;
                    }
                    var deferred = $q.defer(); //stemma.json?action=getStemmi  ????
                    $http.get(urlServizi + 'mobile/stemma.json', {
                        params: {
                            action: 'getStemmi',
                        }
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                modificaSquadra: function (idCampionato, idSquadra, nomeSquadra, idAvatar) {
                    var urlServizi;
                    var modo = parseInt($rootScope.modoGioco);
                    switch (modo) {
                        case 3:
                            urlServizi = torneoClassicoUrl;
                            break;
                        case 1:
                            urlServizi = torneoGironiUrl;
                            break;
                        case 2:
                            urlServizi = torneoLegheUrl;
                            break;
                        case 4:
                            urlServizi = torneoPlayoffUrl;
                            break;
                    }
                    var deferred = $q.defer();
                    $http.get(urlServizi + 'mobile/squadra.json', {
                        params: {
                            team: nomeSquadra,
                            idavatar: idAvatar,
                            action: 'modificaSquadra',
                            is: idSquadra
                        }
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                eliminaSquadra: function (idSquadra) {
                    var urlServizi;
                    var modo = parseInt($rootScope.modoGioco);
                    switch (modo) {
                        case 3:
                            urlServizi = torneoClassicoUrl;
                            break;
                        case 1:
                            urlServizi = torneoGironiUrl;
                            break;
                        case 2:
                            urlServizi = torneoLegheUrl;
                            break;
                        case 4:
                            urlServizi = torneoPlayoffUrl;
                            break;
                    }
                    var deferred = $q.defer();
                    $http.get(urlServizi + 'mobile/squadra.json', {
                        params: {
                            action: 'cancellaSquadra',
                            is: idSquadra
                        }
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                nuovaSquadra: function (idCliente, modoGioco) {
                    var urlServizi;
                    //var modo = parseInt($rootScope.modoGioco);
                    switch (parseInt(modoGioco)) {
                        case 3:
                            urlServizi = torneoClassicoUrl;
                            break;
                        case 1:
                            urlServizi = torneoGironiUrl;
                            break;
                        case 2:
                            urlServizi = torneoLegheUrl;
                            break;
                        case 4:
                            urlServizi = torneoPlayoffUrl;
                            break;
                    }
                    var deferred = $q.defer();
                    $http.get(urlServizi + 'mobile/squadra.json', {
                        params: {
                            action: 'addSquadra',
                            idc: idCliente,
                            operation: 'add',
                            modo_gioco: modoGioco,
                            avatarlist: false
                        }
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                gestioneRosa: function (idCampionato, idSquadra) {
                    var urlServizi;
                    var modo = parseInt($rootScope.modoGioco);
                    switch (modo) {
                        case 3:
                            urlServizi = torneoClassicoUrl;
                            break;
                        case 1:
                            urlServizi = torneoGironiUrl;
                            break;
                        case 2:
                            urlServizi = torneoLegheUrl;
                            break;
                        case 4:
                            urlServizi = torneoPlayoffUrl;
                            break;
                    }
                    var deferred = $q.defer();
                    $http.get(urlServizi + 'scripts/rosadata.jsp', {
                        params: {
                            is: idSquadra,
                            ic: idCampionato,
                            format: 'json'
                        }
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                modificaRosa: function (idSquadra, sold, bought) {
                    var urlServizi;
                    var modo = parseInt($rootScope.modoGioco);
                    switch (modo) {
                        case 3:
                            urlServizi = torneoClassicoUrl;
                            break;
                        case 1:
                            urlServizi = torneoGironiUrl;
                            break;
                        case 2:
                            urlServizi = torneoLegheUrl;
                            break;
                        case 4:
                            urlServizi = torneoPlayoffUrl;
                            break;
                    }
                    var date = new Date();
                    var rand = date.getTime();
                    var deferred = $q.defer();
                    var append = '';
                    if (modo == 3)
                        append = 'gprixteam/create_rosa_mobile.jsp';
                    else
                        append = 'squadra/create_rosa_mobile.jsp';
                    $http.get(urlServizi + append, {
                        params: {
                            rand: rand,
                            sold: sold,
                            bought: bought,
                            is: idSquadra,
                            format: 'json'
                        }
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                formazioneData: function (idCampionato, idSquadra) {
                    var urlServizi;
                    var modo = parseInt($rootScope.modoGioco);
                    switch (modo) {
                        case 3:
                            urlServizi = torneoClassicoUrl;
                            break;
                        case 1:
                            urlServizi = torneoGironiUrl;
                            break;
                        case 2:
                            urlServizi = torneoLegheUrl;
                            break;
                        case 4:
                            urlServizi = torneoPlayoffUrl;
                            break;
                    }
                    var deferred = $q.defer();
                    $http.get(urlServizi + 'scripts/formazionedata.jsp', {
                        params: {
                            is: idSquadra,
                            ic: idCampionato,
                            format: 'json'
                        }
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                salvaFormazione: function (idCampionato, idSquadra, tattica, formazione) {
                    var urlServizi;
                    var modo = parseInt($rootScope.modoGioco);
                    switch (modo) {
                        case 3:
                            urlServizi = torneoClassicoUrl;
                            break;
                        case 1:
                            urlServizi = torneoGironiUrl;
                            break;
                        case 2:
                            urlServizi = torneoLegheUrl;
                            break;
                        case 4:
                            urlServizi = torneoPlayoffUrl;
                            break;
                    }
                    var date = new Date();
                    var rand = date.getTime();
                    var deferred = $q.defer();
                    var urlChiamata = '';
                    if (modo == 3) {
                        urlChiamata = 'gprixteam/create_formazione_mobile.jsp';
                    }
                    else {
                        urlChiamata = 'squadra/create_formazione_mobile.jsp';
                    }
                    $http.get(urlServizi + urlChiamata, {
                        params: {
                            rand: rand,
                            tattica: tattica,
                            ic: idCampionato,
                            add: formazione,
                            is: idSquadra,
                            format: 'json'
                        }
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                modificaFormazione: function (idCampionato, idSquadra, tattica, remove, add) {
                    var urlServizi;
                    var modo = parseInt($rootScope.modoGioco);
                    switch (modo) {
                        case 3:
                            urlServizi = torneoClassicoUrl;
                            break;
                        case 1:
                            urlServizi = torneoGironiUrl;
                            break;
                        case 2:
                            urlServizi = torneoLegheUrl;
                            break;
                        case 4:
                            urlServizi = torneoPlayoffUrl;
                            break;
                    }
                    var date = new Date();
                    var rand = date.getTime();
                    var deferred = $q.defer();
                    var urlChiamata = '';
                    if (modo == 3) {
                        urlChiamata = 'gprixteam/create_formazione_mobile.jsp';
                    }
                    else {
                        urlChiamata = 'squadra/create_formazione_mobile.jsp';
                    }
                    $http.get(urlServizi + urlChiamata, {
                        params: {
                            ic: idCampionato,
                            is: idSquadra,
                            tattica: tattica,
                            remove: remove,
                            add: add,
                            rand: rand
                        }
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                cancellaFormazione: function (idCampionato, idSquadra, tattica) {
                    var urlServizi;
                    var modo = parseInt($rootScope.modoGioco);
                    switch (modo) {
                        case 3:
                            urlServizi = torneoClassicoUrl;
                            break;
                        case 1:
                            urlServizi = torneoGironiUrl;
                            break;
                        case 2:
                            urlServizi = torneoLegheUrl;
                            break;
                        case 4:
                            urlServizi = torneoPlayoffUrl;
                            break;
                    }
                    var date = new Date();
                    var rand = date.getTime();
                    var deferred = $q.defer();
                    var urlChiamata = '';
                    if (modo == 3) {
                        urlChiamata = 'gprixteam/create_formazione_mobile.jsp';
                    }
                    else {
                        urlChiamata = 'squadra/create_formazione_mobile.jsp';
                    }
                    $http.get(urlServizi + urlChiamata, {
                        params: {
                            rand: rand,
                            tattica: tattica,
                            ic: idCampionato,
                            clrfrmzn: 'yes',
                            is: idSquadra,
                            format: 'json'
                        }
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                getNews: function () {
                    var urlServizi;
                    urlServizi = torneoClassicoUrl;
                    var deferred = $q.defer();
                    $http.get(urlServizi + 'mobile/news.json', {
                        params: {
                            action: "getNews"
                        }
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                registraUtente: function (email, password, nome, cognome, username, dataNascita, regolamento, fl_privacy, fl_mktg, fl_servizio_mcc, fl_servizio_cds, fl_articoli_privacy, modoGioco) {
                    var urlServizi;
                    //var modo = parseInt($rootScope.modoGioco);
                    switch (parseInt(modoGioco)) {
                        case 3:
                            urlServizi = torneoClassicoUrl;
                            break;
                        case 1:
                            urlServizi = torneoGironiUrl;
                            break;
                        case 2:
                            urlServizi = torneoLegheUrl;
                            break;
                        case 4:
                            urlServizi = torneoPlayoffUrl;
                            break;
                    }
                    var deferred = $q.defer();
                    $http.get(urlServizi + 'mobile/register.json', {
                        params: {
                            action: 'addUser',
                            MAIL: email,
                            PASSWORD: password,
                            NOME: nome,
                            COGNOME: cognome,
                            USERNAME: username,
                            DTNASCITA: dataNascita,
                            Regolamento: regolamento,
                            fl_privacy: fl_privacy,
                            fl_mktg: fl_mktg,
                            fl_servizio_mcc: fl_servizio_mcc,
                            fl_servizio_cds: fl_servizio_cds,
                            fl_articoli_privacy: fl_articoli_privacy,
                            modo_gioco: modoGioco
                        }
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                registraUtenteFB: function (email, token, regolamento, fl_privacy, fl_mktg, fl_servizio_mcc, fl_servizio_cds, fl_articoli_privacy, modoGioco) {
                    var urlServizi;
                    //var modo = parseInt($rootScope.modoGioco);
                    switch (parseInt(modoGioco)) {
                        case 3:
                            urlServizi = torneoClassicoUrl;
                            break;
                        case 1:
                            urlServizi = torneoGironiUrl;
                            break;
                        case 2:
                            urlServizi = torneoLegheUrl;
                            break;
                        case 4:
                            urlServizi = torneoPlayoffUrl;
                            break;
                    }
                    var deferred = $q.defer();
                    $http.get(urlServizi + 'mobile/registerFB.json', {
                        params: {
                            email: email,
                            action: 'addUser',
                            Regolamento: regolamento,
                            fl_privacy: fl_privacy,
                            fl_mktg: fl_mktg,
                            fl_servizio_mcc: fl_servizio_mcc,
                            fl_servizio_cds: fl_servizio_cds,
                            fl_articoli_privacy: fl_articoli_privacy,
                            modo_gioco: modoGioco,
                            token: token
                        }
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                getProfilo: function (idCliente) {
                    var urlServizi;
                    var modo = parseInt($rootScope.modoGioco);
                    switch (modo) {
                        case 3:
                            urlServizi = torneoClassicoUrl;
                            break;
                        case 1:
                            urlServizi = torneoGironiUrl;
                            break;
                        case 2:
                            urlServizi = torneoLegheUrl;
                            break;
                        case 4:
                            urlServizi = torneoPlayoffUrl;
                            break;
                    }
                    var deferred = $q.defer();
                    $http.get(urlServizi + 'mobile/user.json', {
                        params: {
                            action: 'datiProfilo',
                            idc: idCliente
                        }
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                modificaProfilo: function (idCliente, NOME, COGNOME, DTNASCITA, SESSO, MAIL, INDIR, CITTA, PROV, CAP, UNSUB, MAIL_2, INDIRIZZO_SPED, CAP_SPED, CITTA_SPED, PROV_SPED) {
                    var urlServizi;
                    var modo = parseInt($rootScope.modoGioco);
                    switch (modo) {
                        case 3:
                            urlServizi = torneoClassicoUrl;
                            break;
                        case 1:
                            urlServizi = torneoGironiUrl;
                            break;
                        case 2:
                            urlServizi = torneoLegheUrl;
                            break;
                        case 4:
                            urlServizi = torneoPlayoffUrl;
                            break;
                    }
                    var deferred = $q.defer();
                    $http.get(urlServizi + 'mobile/user.json', {
                        params: {
                            action: 'updateUser',
                            idc: idCliente,
                            NOME: NOME,
                            COGNOME: COGNOME,
                            DTNASCITA: DTNASCITA,
                            SESSO: SESSO,
                            MAIL: MAIL,
                            INDIR: INDIR,
                            CITTA: CITTA,
                            PROV: PROV,
                            CAP: CAP,
                            UNSUB: UNSUB,
                            MAIL_2: MAIL_2,
                            INDIRIZZO_SPED: INDIRIZZO_SPED,
                            CAP_SPED: CAP_SPED,
                            CITTA_SPED: CITTA_SPED,
                            PROV_SPED: PROV_SPED
                        }
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                }
            };
        }]);
    app.service('RosaService', function ($rootScope) {
        var offerte = false;
        var primaRosa = false;
        var bloccoRosa = false;
        var portieriRosa = [];
        var difensoriRosa = [];
        var centrocampistiRosa = [];
        var attaccantiRosa = [];
        var allenatoriRosa = [];
        var giocatoriDisponibili = [];
        var giocatoreSelezionato = null;
        var giocatoriVenduti = '';
        var giocatoriAcquistati = '';
        var saldo = 0;
        var lastSelected = null;
        return {
            setBloccoRosa: function (value) {
                bloccoRosa = value;
            },
            getBloccoRosa: function () {
                return bloccoRosa;
            },
            setOfferte: function (value) {
                offerte = value;
            },
            getOfferte: function () {
                return offerte;
            },
            setPrimaRosa: function (value) {
                primaRosa = value;
            },
            getPrimaRosa: function () {
                return primaRosa;
            },
            setGiocatore: function (giocatore, offerta) {
                switch (lastSelected.ruolo) {
                    case 'P':
                        var giocatoreRosaCast = {};
                        angular.extend(giocatoreRosaCast, giocatore);
                        giocatoreRosaCast.COSTO_ACQUISTO = offerta;
                        giocatoreRosaCast.IN_FORMAZIONE = false;
                        portieriRosa[lastSelected.index] = giocatoreRosaCast;
                        break;
                    case 'D':
                        var giocatoreRosaCast = {};
                        angular.extend(giocatoreRosaCast, giocatore);
                        giocatoreRosaCast.COSTO_ACQUISTO = offerta;
                        giocatoreRosaCast.IN_FORMAZIONE = false;
                        difensoriRosa[lastSelected.index] = giocatoreRosaCast;
                        break;
                    case 'C':
                        var giocatoreRosaCast = {};
                        angular.extend(giocatoreRosaCast, giocatore);
                        giocatoreRosaCast.COSTO_ACQUISTO = offerta;
                        giocatoreRosaCast.IN_FORMAZIONE = false;
                        centrocampistiRosa[lastSelected.index] = giocatoreRosaCast;
                        break;
                    case 'A':
                        var giocatoreRosaCast = {};
                        angular.extend(giocatoreRosaCast, giocatore);
                        giocatoreRosaCast.COSTO_ACQUISTO = offerta;
                        giocatoreRosaCast.IN_FORMAZIONE = false;
                        attaccantiRosa[lastSelected.index] = giocatoreRosaCast;
                        break;
                    case 'AL':
                        var giocatoreRosaCast = {};
                        angular.extend(giocatoreRosaCast, giocatore);
                        giocatoreRosaCast.COSTO_ACQUISTO = offerta;
                        giocatoreRosaCast.IN_FORMAZIONE = false;
                        allenatoriRosa[lastSelected.index] = giocatoreRosaCast;
                        break;
                }
                giocatoriDisponibili.splice(giocatoriDisponibili.indexOf(giocatore), 1);
            },
            giocatoreRemovedFromRosa: function (giocatore) {
                var rosaGiocatoreCast = {};
                angular.extend(rosaGiocatoreCast, giocatore);
                //rosaGiocatoreCast.VALORE_ATTUALE = giocatore.COSTO_ACQUISTO;
                delete rosaGiocatoreCast.COSTO_ACQUISTO;
                delete rosaGiocatoreCast.IN_FORMAZIONE;
                giocatoriDisponibili.push(rosaGiocatoreCast);
            },
            getPortieri: function (sortBy, crescente, squadra, costoMin, costoMax) {
                var giocatori = [];
                if (giocatoriDisponibili.length > 0) {
                    giocatoriDisponibili.forEach(function (item) {
                        if (item.RUOLO == 0 && (squadra.length == 0 || squadra.indexOf(item.SQUADRA) > -1) &&
                            (item.VALORE_ATTUALE >= costoMin && item.VALORE_ATTUALE <= costoMax)) {
                            giocatori.push(item);
                        }
                    });
                }
                if (sortBy.toLowerCase() == "cognome") {
                    if (crescente) {
                        giocatori.sort(compareByCognomeCrescente);
                    }
                    else {
                        giocatori.sort(compareByCognomeDecrescente);
                    }
                }
                else if (sortBy.toLowerCase() == "costo") {
                    if (crescente) {
                        giocatori.sort(compareByCostoCrescente);
                    }
                    else {
                        giocatori.sort(compareByCostoDecrescente);
                    }
                }
                else {
                    return [];
                }
                return giocatori;
            },
            getDifensori: function (sortBy, crescente, squadra, costoMin, costoMax) {
                var giocatori = [];
                if (giocatoriDisponibili.length > 0) {
                    giocatoriDisponibili.forEach(function (item) {
                        if (item.RUOLO == 1 && (squadra.length == 0 || squadra.indexOf(item.SQUADRA) > -1) &&
                            (item.VALORE_ATTUALE >= costoMin && item.VALORE_ATTUALE <= costoMax)) {
                            giocatori.push(item);
                        }
                    });
                }
                if (sortBy.toLowerCase() == "cognome") {
                    if (crescente) {
                        giocatori.sort(compareByCognomeCrescente);
                    }
                    else {
                        giocatori.sort(compareByCognomeDecrescente);
                    }
                }
                else if (sortBy.toLowerCase() == "costo") {
                    if (crescente) {
                        giocatori.sort(compareByCostoCrescente);
                    }
                    else {
                        giocatori.sort(compareByCostoDecrescente);
                    }
                }
                else {
                    return [];
                }
                return giocatori;
            },
            getCentrocampisti: function (sortBy, crescente, squadra, costoMin, costoMax) {
                var giocatori = [];
                if (giocatoriDisponibili.length > 0) {
                    giocatoriDisponibili.forEach(function (item) {
                        if (item.RUOLO == 2 && (squadra.length == 0 || squadra.indexOf(item.SQUADRA) > -1) &&
                            (item.VALORE_ATTUALE >= costoMin && item.VALORE_ATTUALE <= costoMax)) {
                            giocatori.push(item);
                        }
                    });
                }
                if (sortBy.toLowerCase() == "cognome") {
                    if (crescente) {
                        giocatori.sort(compareByCognomeCrescente);
                    }
                    else {
                        giocatori.sort(compareByCognomeDecrescente);
                    }
                }
                else if (sortBy.toLowerCase() == "costo") {
                    if (crescente) {
                        giocatori.sort(compareByCostoCrescente);
                    }
                    else {
                        giocatori.sort(compareByCostoDecrescente);
                    }
                }
                else {
                    return [];
                }
                return giocatori;
            },
            getAttaccanti: function (sortBy, crescente, squadra, costoMin, costoMax) {
                var giocatori = [];
                if (giocatoriDisponibili.length > 0) {
                    giocatoriDisponibili.forEach(function (item) {
                        if (item.RUOLO == 3 && (squadra.length == 0 || squadra.indexOf(item.SQUADRA) > -1) &&
                            (item.VALORE_ATTUALE >= costoMin && item.VALORE_ATTUALE <= costoMax)) {
                            giocatori.push(item);
                        }
                    });
                }
                if (sortBy.toLowerCase() == "cognome") {
                    if (crescente) {
                        giocatori.sort(compareByCognomeCrescente);
                    }
                    else {
                        giocatori.sort(compareByCognomeDecrescente);
                    }
                }
                else if (sortBy.toLowerCase() == "costo") {
                    if (crescente) {
                        giocatori.sort(compareByCostoCrescente);
                    }
                    else {
                        giocatori.sort(compareByCostoDecrescente);
                    }
                }
                else {
                    return [];
                }
                return giocatori;
            },
            getAllenatori: function (sortBy, crescente, squadra, costoMin, costoMax) {
                var giocatori = [];
                if (giocatoriDisponibili.length > 0) {
                    giocatoriDisponibili.forEach(function (item) {
                        if (item.RUOLO == 4 && (squadra.length == 0 || squadra.indexOf(item.SQUADRA) > -1) &&
                            (item.VALORE_ATTUALE >= costoMin && item.VALORE_ATTUALE <= costoMax)) {
                            giocatori.push(item);
                        }
                    });
                }
                if (sortBy.toLowerCase() == "cognome") {
                    if (crescente) {
                        giocatori.sort(compareByCognomeCrescente);
                    }
                    else {
                        giocatori.sort(compareByCognomeDecrescente);
                    }
                }
                else if (sortBy.toLowerCase() == "costo") {
                    if (crescente) {
                        giocatori.sort(compareByCostoCrescente);
                    }
                    else {
                        giocatori.sort(compareByCostoDecrescente);
                    }
                }
                else {
                    return [];
                }
                return giocatori;
            },
            setRosa: function (portieri, difensori, centrocampisti, attaccanti, allenatori) {
                portieriRosa = portieri;
                difensoriRosa = difensori;
                centrocampistiRosa = centrocampisti;
                attaccantiRosa = attaccanti;
                if ($rootScope.misterCalcioCup)
                    allenatoriRosa = allenatori;
            },
            getRosa: function () {
                if (!$rootScope.mistercalciocup)
                    return portieriRosa.concat(difensoriRosa, centrocampistiRosa, attaccantiRosa, allenatoriRosa);
                else
                    ($rootScope.mistercalciocup);
                return portieriRosa.concat(difensoriRosa, centrocampistiRosa, attaccantiRosa, allenatoriRosa, allenatoriRosa);
            },
            getSquadre: function () {
                var elencoSquadre = [];
                giocatoriDisponibili.forEach(function (item) {
                    if (elencoSquadre.indexOf(item.SQUADRA) == -1) {
                        elencoSquadre.push(item.SQUADRA);
                    }
                });
                return elencoSquadre;
            },
            setGiocatoriDisponibili: function (_giocatoriDisponibili) {
                giocatoriDisponibili = _giocatoriDisponibili;
            },
            getGiocatoriDisponibili: function () {
                return giocatoriDisponibili;
            },
            setGiocatoreSelezionato: function (_giocatoreSelezionato) {
                giocatoreSelezionato = _giocatoreSelezionato;
            },
            getGiocatoreSelezionato: function () {
                return giocatoreSelezionato;
            },
            setGiocatoriVenduti: function (_giocatoriVenduti) {
                giocatoriVenduti = _giocatoriVenduti;
            },
            getGiocatoriVenduti: function () {
                return giocatoriVenduti;
            },
            setGiocatoriAcquistati: function (_giocatoriAcquistati) {
                giocatoriAcquistati = _giocatoriAcquistati;
            },
            getGiocatoriAcquistati: function () {
                return giocatoriAcquistati;
            },
            setLastSelected: function (_lastSelected) {
                lastSelected = _lastSelected;
            },
            getLastSelected: function () {
                return lastSelected;
            },
            setSaldo: function (_saldo) {
                saldo = _saldo;
            },
            getSaldo: function () {
                return saldo;
            }
        };
        //FUNZIONI PER IL SORTING
        function compareByCognomeCrescente(a, b) {
            if (a.COGNOME < b.COGNOME)
                return -1;
            if (a.COGNOME > b.COGNOME)
                return 1;
            return 0;
        }
        ;
        function compareByCostoCrescente(a, b) {
            if (a.VALORE_ATTUALE < b.VALORE_ATTUALE)
                return -1;
            if (a.VALORE_ATTUALE > b.VALORE_ATTUALE)
                return 1;
            return 0;
        }
        ;
        function compareByCognomeDecrescente(a, b) {
            if (a.COGNOME > b.COGNOME)
                return -1;
            if (a.COGNOME < b.COGNOME)
                return 1;
            return 0;
        }
        ;
        function compareByCostoDecrescente(a, b) {
            if (a.VALORE_ATTUALE > b.VALORE_ATTUALE)
                return -1;
            if (a.VALORE_ATTUALE < b.VALORE_ATTUALE)
                return 1;
            return 0;
        }
        ;
    });
})();
/// <reference path="../../../typings/tsd.d.ts" />
/// <reference path="../model/model.ts" />
(function () {
    var app = angular.module('appCtrl', []);
    app.controller('AppCtrl', function ($scope, $rootScope, $state, $ionicHistory, $cordovaFileTransfer, $timeout, FantaCalcioServices, $ionicLoading) {
        $rootScope.hideNews = { value: false };
        $rootScope.testoEdizioneDigitale = '';
        $scope.accordionMenu = false;
        $scope.openAccordion = function () {
            if ($scope.accordionMenu == true)
                $scope.accordionMenu = false;
            else
                $scope.accordionMenu = true;
        };
        $scope.selectSquadra = function (squadra, modo) {
            $rootScope.squadraSelected = squadra;
            $rootScope.modoGioco = modo;
            //$rootScope.user = $rootScope.userGironi;
            $scope.accordionMenu = false;
            $ionicHistory.nextViewOptions({
                disableAnimate: true,
                disableBack: true,
                historyRoot: true
            });
            $rootScope.showToast($rootScope.squadraSelected.nome);
            $state.go('app.home');
        };
        $scope.logout = function () {
            $rootScope.showLoading();
            if ($rootScope.misterCalcioCup) {
                window.localStorage.removeItem('misterCalcioCupLegheLogin');
            }
            else {
                window.localStorage.removeItem('tuttoSportLeagueLegheLogin');
            }
            FantaCalcioServices.logout($rootScope.modoGioco).then(function (data) {
                if (data.error) {
                    $rootScope.showToast(data.error.user_message);
                    $rootScope.user = null;
                    $rootScope.squadraSelected = null;
                    $rootScope.squadreTorneoClassico = [];
                    $rootScope.squadreTorneoGironi = [];
                    $scope.accordionMenu = false;
                    $ionicHistory.nextViewOptions({
                        disableAnimate: true,
                        disableBack: true,
                        historyRoot: true
                    });
                    $state.go('tipoGioco');
                }
                else {
                    $rootScope.user = null;
                    $rootScope.squadraSelected = null;
                    $rootScope.squadreTorneoClassico = [];
                    $rootScope.squadreTorneoGironi = [];
                    $scope.accordionMenu = false;
                    $ionicHistory.nextViewOptions({
                        disableAnimate: true,
                        disableBack: true,
                        historyRoot: true
                    });
                    $state.go('login', { gioco: 2 });
                }
            }, function (data) {
                $rootScope.hideLoading();
                $rootScope.showToast('Errore di connessione');
            });
        };
        $scope.goToModificaProfilo = function () {
            $rootScope.showLoading();
            //$rootScope.user = user;
            $scope.accordionMenu = false;
            $state.go('app.modificaProfilo');
        };
        $rootScope.autologin = function (counter) {
            if (counter != undefined) {
                if (counter == 0)
                    $rootScope.showLoading();
                counter++;
                if ($rootScope.misterCalcioCup)
                    var loginData = JSON.parse(window.localStorage.getItem('misterCalcioCupLegheLogin'));
                else
                    var loginData = JSON.parse(window.localStorage.getItem('tuttoSportLeagueLegheLogin'));
                FantaCalcioServices.authentication(loginData.email, loginData.password, loginData.modoGioco).then(function (data) {
                    if (data.success) {
                        console.log(data);
                        if ($rootScope.misterCalcioCup) {
                            window.localStorage.setItem('misterCalcioCupLegheLogin', JSON.stringify(loginData));
                        }
                        else {
                            window.localStorage.setItem('tuttoSportLeagueLegheLogin', JSON.stringify(loginData));
                        }
                        $rootScope.user = data.success.user;
                        $ionicHistory.nextViewOptions({
                            disableAnimate: true,
                            disableBack: true,
                            historyRoot: true
                        });
                        $state.go('app.home');
                    }
                    else if (data.error) {
                        console.log(data);
                        if (counter <= 3) {
                            $rootScope.autologin(counter);
                        }
                        else {
                            if ($rootScope.misterCalcioCup) {
                                window.localStorage.removeItem('misterCalcioCupLegheLogin');
                            }
                            else {
                                window.localStorage.removeItem('tuttoSportLeagueLegheLogin');
                            }
                            $rootScope.hideLoading();
                            $rootScope.showToast(data.error.user_message);
                        }
                    }
                    $rootScope.hideLoading();
                }, function (data) {
                    $rootScope.hideLoading();
                    $rootScope.showToast('Errore di connessione');
                });
            }
            else {
                $rootScope.autologin(0);
            }
        };
        $scope.goToState = function (state) {
            if ($state.current.name != state) {
                $rootScope.showLoading();
                $ionicHistory.nextViewOptions({
                    disableAnimate: true,
                    disableBack: true,
                    historyRoot: false
                });
                $state.go(state);
            }
            ;
        };
        $scope.openRegolamento = function () {
            $rootScope.showLoading();
            var urlDomain = window.location.host.indexOf('192') == 0 || window.location.host.indexOf('localhost') == 0
                ? 'http://localhost:9292/www.mr.yland.it/'
                : 'http://www.mr.yland.it/'; //ESTERNA
            //MISTER CALCIO CUP LEGHE modo_gioco = 2  
            //?'http://localhost:9292/www.mr.yland.it/'
            //:'http://www.mr.yland.it/';								//ESTERNA
            ;
            if (ionic.Platform.isIOS()) {
                $rootScope.hideLoading();
                window.open(urlDomain + 'doc/regolamento_concorso' + ($rootScope.modoGioco == 3 ? '_classico' : '_gironi') + '.pdf', "_blank", "closebuttoncaption=Indietro,location=no,enableviewportscale=yes");
            }
            else if (ionic.Platform.isAndroid()) {
                var filePath = encodeURI(window.cordova.file.externalRootDirectory + ('Download/regolamento_concorso_gironi.pdf') + ($rootScope.modoGioco == 3 ? '_classico' : '_gironi') + '.pdf');
                var trustHosts = true;
                var options = {};
                $cordovaFileTransfer.download(encodeURI(urlDomain + 'doc/regolamento_concorso') + ($rootScope.modoGioco == 3 ? '_classico' : '_gironi') + '.pdf', filePath, options, trustHosts).then(function (fileEntry) {
                    $rootScope.hideLoading();
                    console.log("Download success");
                    console.log(fileEntry);
                    window.cordova.plugins.fileOpener2.open(filePath, 'application/pdf');
                }, function (error) {
                    $rootScope.hideLoading();
                    navigator.notification.alert('Non è stato possibile recuperare l\'allegato, riprovare.', function () { }, '...');
                    console.log("Download error");
                    console.log(error);
                });
            }
        };
        $scope.openEdizioneDigitale = function () {
            if (window.cordova) {
                navigator.notification.alert($rootScope.testoEdizioneDigitale, undefined, 'Leghe Amici', 'OK');
            }
            else {
                alert($rootScope.testoEdizioneDigitale);
            }
        };
        $scope.creaLegaAmici = function () {
            $rootScope.showLoading();
            FantaCalcioServices.creaLega($rootScope.user.idCliente).then(function (data) {
                if (data.success) {
                    $rootScope.user.idCampionato = data.success.id_campionato;
                    $rootScope.user.idGruppo = data.success.id_gruppo;
                    $rootScope.user.presidente = true;
                    $rootScope.showToast(data.success.msg_message);
                    $state.go('app.datiLega');
                }
                else {
                    $rootScope.showToast(data.error.user_message);
                    $rootScope.hideLoading();
                }
            }, function (data) {
                $rootScope.hideLoading();
            });
        };
    });
})();
/// <reference path="../../../typings/tsd.d.ts" />
/// <reference path="../model/model.ts" />
(function () {
    var app = angular.module('calendarioCtrl', []);
    app.controller('calendarioCtrl', function ($scope, $rootScope, $stateParams, $state, $ionicLoading, $ionicPopover, FantaCalcioServices) {
        $scope.vm = {
            numeroGiornata: $stateParams.numeroGiornata,
            dataGiornata: $stateParams.dataGiornata,
            giornate: [],
            portieriRosa: [],
            portieriRosaPanchina: [],
            difensoriRosa: [],
            difensoriRosaPanchina: [],
            centrocampistiRosa: [],
            centrocampistiRosaPanchina: [],
            attaccantiRosa: [],
            attaccantiRosaPanchina: [],
            allenatoriRosa: [],
            allenatoriRosaPanchina: [],
            loadingOk: null,
            errorMessage: ''
        };
        $scope.initCalendario = function () {
            FantaCalcioServices.getCalendario($rootScope.squadraSelected.codice_squadra).then(function (data) {
                if (data.msg_utente) {
                    $rootScope.hideLoading();
                    $scope.vm.loadingOk = false;
                    $scope.vm.errorMessage = data.msg_utente;
                    //$rootScope.showToast(data.error.user_message);
                    if (data.error && data.error.id == 'not_logged')
                        $rootScope.autologin();
                }
                else {
                    //$rootScope.showToast(data.msg_utente);
                    console.log(data);
                    if (data.giornate) {
                        $scope.vm.loadingOk = true;
                        $scope.vm.giornate = data.giornate;
                    }
                    $rootScope.hideLoading();
                }
            }, function (data) {
                $rootScope.hideLoading();
                $rootScope.showToast('Errore nella connessione');
                console.log(data);
            });
        };
        $scope.goToDettaglioGiornata = function (giornata) {
            $rootScope.showLoading();
            if ($rootScope.modoGioco == 3) {
                $rootScope.giornataClassico = giornata;
                $state.go('app.dettaglioGiornata', {
                    idGiornata: giornata.id_partita,
                    numeroGiornata: giornata.numero,
                    dataGiornata: giornata.data
                });
            }
            else {
                $rootScope.giornataGironi = giornata;
                $state.go('app.partiteGironi');
            }
        };
        $scope.initDettaglioGiornata = function () {
            FantaCalcioServices.getDettaglioGiornata($rootScope.squadraSelected.codice_squadra, $rootScope.giornataClassico.id_partita)
                .then(function (data) {
                if (data.error) {
                    $rootScope.showToast(data.error.user_message);
                    if (data.error && data.error.id == 'not_logged')
                        $rootScope.autologin();
                }
                else {
                    console.log(data);
                    data.squadre[0].titolari.forEach(function (item) {
                        switch (item.cd_ruolo) {
                            case 0:
                                $scope.vm.portieriRosa.push(item);
                                break;
                            case 1:
                                $scope.vm.difensoriRosa.push(item);
                                break;
                            case 2:
                                $scope.vm.centrocampistiRosa.push(item);
                                break;
                            case 3:
                                $scope.vm.attaccantiRosa.push(item);
                                break;
                            case 4:
                                $scope.vm.allenatoriRosa.push(item);
                                break;
                        }
                        ;
                    });
                    data.squadre[0].riserve.forEach(function (item) {
                        switch (item.cd_ruolo) {
                            case 0:
                                $scope.vm.portieriRosaPanchina.push(item);
                                break;
                            case 1:
                                $scope.vm.difensoriRosaPanchina.push(item);
                                break;
                            case 2:
                                $scope.vm.centrocampistiRosaPanchina.push(item);
                                break;
                            case 3:
                                $scope.vm.attaccantiRosaPanchina.push(item);
                                break;
                            case 4:
                                $scope.vm.allenatoriRosaPanchina.push(item);
                                break;
                        }
                        ;
                    });
                }
                $rootScope.hideLoading();
            }, function (data) {
                $rootScope.hideLoading();
                $rootScope.showToast('Errore nella connessione');
            });
        };
        $scope.goToDettaglioTabellino = function (giocatore) {
            $rootScope.giocatore = giocatore;
            if (giocatore.cd_ruolo == 0)
                giocatore.icona = 'icon--portiere';
            else if (giocatore.cd_ruolo == 1)
                giocatore.icona = 'icon--difensore';
            else if (giocatore.cd_ruolo == 2)
                giocatore.icona = 'icon--centrocampista';
            else if (giocatore.cd_ruolo == 3)
                giocatore.icona = 'icon--attaccante';
            else if (giocatore.cd_ruolo == 4)
                giocatore.icona = 'icon--allenatore';
            $state.go('app.dettaglioTabellino');
        };
        $scope.firstLetter = function (text) {
            if (text)
                return text.charAt(0) + '.';
            else
                return '';
        };
        $ionicPopover.fromTemplateUrl('popoverPanchina.html', {
            scope: $scope,
        }).then(function (popover) {
            $scope.popoverPanchina = popover;
        });
    });
})();
/// <reference path="../../../typings/tsd.d.ts" />
/// <reference path="../model/model.ts" />
(function () {
    var app = angular.module('classificheCtrl', []);
    app.controller('classificheCtrl', function ($scope, $rootScope, $stateParams, $state, $ionicLoading, FantaCalcioServices) {
        $scope.vm = {
            squadreSettimanale: [],
            squadreTorneoAndata: [],
            squadreTorneoRitorno: [],
            squadreTorneoEstivo: [],
            errorMessage: '',
            loadingOk: null
        };
        $scope.initClassifiche = function () {
            FantaCalcioServices.getClassifica($rootScope.squadraSelected.codice_squadra).then(function (data) {
                if (data.error) {
                    $rootScope.showToast(data.error.user_message);
                    if (data.error && data.error.id == 'not_logged')
                        $rootScope.autologin();
                }
                else {
                    console.log(data);
                    if (data.settimanale) {
                        $scope.vm.squadreSettimanale = data.settimanale;
                        $scope.vm.squadreTorneoAndata = data.torneo_andata;
                        $scope.vm.squadreTorneoRitorno = data.torneo_ritorno;
                        $scope.vm.squadreTorneoEstivo = data.torneo_estivo;
                        $scope.vm.loadingOk = true;
                    }
                    else {
                        $scope.vm.errorMessage = data.msg_utente;
                        $scope.vm.loadingOk = false;
                    }
                }
                $rootScope.hideLoading();
            }, function (data) {
                $rootScope.hideLoading();
                $rootScope.showToast('Errore nella connessione');
            });
        };
    });
})();
/// <reference path="../../../typings/tsd.d.ts" />
/// <reference path="../model/model.ts" />
(function () {
    var app = angular.module('formazioneCtrl', []);
    app.controller('formazioneCtrl', function ($scope, $rootScope, $ionicPopover, $ionicLoading, FantaCalcioServices) {
        $scope.vm = {
            portieri: [],
            portieriPosition: { value: null },
            portieriPanchina: [],
            portieriPanchinaPosition: { value: null },
            difensori: [],
            difensoriPosition: { value: null },
            difensoriPanchina: [],
            difensoriPanchinaPosition: { value: null },
            centrocampisti: [],
            centrocampistiPosition: { value: null },
            centrocampistiPanchina: [],
            centrocampistiPanchinaPosition: { value: null },
            attaccanti: [],
            attaccantiPosition: { value: null },
            attaccantiPanchina: [],
            attaccantiPanchinaPosition: { value: null },
            allenatoriPosition: { value: null },
            allenatoriPanchina: [],
            allenatoriPanchinaPosition: { value: null },
            portieriRosa: [],
            difensoriRosa: [],
            centrocampistiRosa: [],
            attaccantiRosa: [],
            allenatoriRosa: [],
            tattiche: [],
            tatticaSelected: {
                DESCRIZIONE: "4-4-2",
                DESIGN: {
                    ATTACCANTI: 2,
                    CENTROCAMPISTI: 4,
                    DIFENSORI: 4,
                },
                TATTICA: 5
            },
            rosaShown: { ruolo: '', position: null },
            rosaPanchinaShown: '',
            ruoloGiocatoreSelected: '',
            formazione: '',
            tipoBlocco: 'nessuno'
        };
        if ($rootScope.misterCalcioCup) {
            $scope.vm.allenatori = [];
            $scope.vm.allenatoriPosition = { value: null };
            $scope.vm.allenatoriPanchina = [];
            $scope.vm.allenatoriPanchinaPosition = { value: null };
            $scope.vm.allenatoriRosa = [];
        }
        function readFormazione(formazioneData) {
            if (formazioneData) {
                if ($rootScope.misterCalcioCup) {
                    $scope.vm.allenatori = [];
                    formazioneData.forEach(function (item, index) {
                        var itemAppoggio = null;
                        if (index == formazioneData.length - 1) {
                            $scope.vm.formazione += item + ':' + index;
                        }
                        else {
                            $scope.vm.formazione += item + ':' + index + ';';
                        }
                        if (index == 0) {
                            itemAppoggio = getGiocatoreById($scope.vm.portieriRosa, item);
                            itemAppoggio.inFormazione = true;
                            $scope.vm.portieri[0] = itemAppoggio;
                        }
                        else if (index < 1 + $scope.vm.tatticaSelected.DESIGN.DIFENSORI) {
                            itemAppoggio = getGiocatoreById($scope.vm.difensoriRosa, item);
                            itemAppoggio.inFormazione = true;
                            $scope.vm.difensori.push(itemAppoggio);
                        }
                        else if (index < 1 + $scope.vm.tatticaSelected.DESIGN.DIFENSORI + $scope.vm.tatticaSelected.DESIGN.CENTROCAMPISTI) {
                            itemAppoggio = getGiocatoreById($scope.vm.centrocampistiRosa, item);
                            itemAppoggio.inFormazione = true;
                            $scope.vm.centrocampisti.push(itemAppoggio);
                        }
                        else if (index < 1 + $scope.vm.tatticaSelected.DESIGN.DIFENSORI + $scope.vm.tatticaSelected.DESIGN.CENTROCAMPISTI + $scope.vm.tatticaSelected.DESIGN.ATTACCANTI) {
                            itemAppoggio = getGiocatoreById($scope.vm.attaccantiRosa, item);
                            itemAppoggio.inFormazione = true;
                            $scope.vm.attaccanti.push(itemAppoggio);
                        }
                        else if (index <= 11) {
                            itemAppoggio = getGiocatoreById($scope.vm.portieriRosa, item);
                            itemAppoggio.inFormazione = true;
                            $scope.vm.portieriPanchina.push(itemAppoggio);
                        }
                        else if (index <= 13) {
                            itemAppoggio = getGiocatoreById($scope.vm.difensoriRosa, item);
                            itemAppoggio.inFormazione = true;
                            $scope.vm.difensoriPanchina.push(itemAppoggio);
                        }
                        else if (index <= 15) {
                            itemAppoggio = getGiocatoreById($scope.vm.centrocampistiRosa, item);
                            itemAppoggio.inFormazione = true;
                            $scope.vm.centrocampistiPanchina.push(itemAppoggio);
                        }
                        else if (index <= 17) {
                            itemAppoggio = getGiocatoreById($scope.vm.attaccantiRosa, item);
                            itemAppoggio.inFormazione = true;
                            $scope.vm.attaccantiPanchina.push(itemAppoggio);
                        }
                        else if (index <= 18) {
                            itemAppoggio = getGiocatoreById($scope.vm.allenatoriRosa, item);
                            itemAppoggio.inFormazione = true;
                            $scope.vm.allenatori.push(itemAppoggio);
                        }
                        else if (index <= 19) {
                            itemAppoggio = getGiocatoreById($scope.vm.allenatoriRosa, item);
                            itemAppoggio.inFormazione = true;
                            $scope.vm.allenatoriPanchina.push(itemAppoggio);
                        }
                    });
                }
                else {
                    formazioneData.forEach(function (item, index) {
                        var itemAppoggio = null;
                        if (index == formazioneData.length - 1) {
                            $scope.vm.formazione += item + ':' + index;
                        }
                        else {
                            $scope.vm.formazione += item + ':' + index + ';';
                        }
                        if (index == 0) {
                            itemAppoggio = getGiocatoreById($scope.vm.portieriRosa, item);
                            itemAppoggio.inFormazione = true;
                            $scope.vm.portieri[0] = itemAppoggio;
                        }
                        else if (index < 1 + $scope.vm.tatticaSelected.DESIGN.DIFENSORI) {
                            itemAppoggio = getGiocatoreById($scope.vm.difensoriRosa, item);
                            itemAppoggio.inFormazione = true;
                            $scope.vm.difensori.push(itemAppoggio);
                        }
                        else if (index < 1 + $scope.vm.tatticaSelected.DESIGN.DIFENSORI + $scope.vm.tatticaSelected.DESIGN.CENTROCAMPISTI) {
                            itemAppoggio = getGiocatoreById($scope.vm.centrocampistiRosa, item);
                            itemAppoggio.inFormazione = true;
                            $scope.vm.centrocampisti.push(itemAppoggio);
                        }
                        else if (index < 1 + $scope.vm.tatticaSelected.DESIGN.DIFENSORI + $scope.vm.tatticaSelected.DESIGN.CENTROCAMPISTI + $scope.vm.tatticaSelected.DESIGN.ATTACCANTI) {
                            itemAppoggio = getGiocatoreById($scope.vm.attaccantiRosa, item);
                            itemAppoggio.inFormazione = true;
                            $scope.vm.attaccanti.push(itemAppoggio);
                        }
                        else if (index <= 11) {
                            itemAppoggio = getGiocatoreById($scope.vm.portieriRosa, item);
                            itemAppoggio.inFormazione = true;
                            $scope.vm.portieriPanchina.push(itemAppoggio);
                        }
                        else if (index <= 13) {
                            itemAppoggio = getGiocatoreById($scope.vm.difensoriRosa, item);
                            itemAppoggio.inFormazione = true;
                            $scope.vm.difensoriPanchina.push(itemAppoggio);
                        }
                        else if (index <= 15) {
                            itemAppoggio = getGiocatoreById($scope.vm.centrocampistiRosa, item);
                            itemAppoggio.inFormazione = true;
                            $scope.vm.centrocampistiPanchina.push(itemAppoggio);
                        }
                        else if (index <= 17) {
                            itemAppoggio = getGiocatoreById($scope.vm.attaccantiRosa, item);
                            itemAppoggio.inFormazione = true;
                            $scope.vm.attaccantiPanchina.push(itemAppoggio);
                        }
                    });
                }
            }
            else if (formazioneData == undefined) {
                $scope.vm.portieriPanchina.length = 1;
                $scope.vm.difensoriPanchina.length = 2;
                $scope.vm.centrocampistiPanchina.length = 2;
                $scope.vm.attaccantiPanchina.length = 2;
                if ($rootScope.misterCalcioCup) {
                    $scope.vm.allenatoriPanchina.length = 1;
                }
            }
        }
        ;
        $scope.init = function () {
            console.log("user!");
            console.log($rootScope.user);
            FantaCalcioServices.formazioneData($rootScope.modoGioco, $rootScope.squadraSelected.codice_squadra).then(function (data) {
                if (data.error) {
                    console.log(data);
                    $rootScope.showToast(data.error.user_message);
                    if (data.error && data.error.id == 'not_logged')
                        $rootScope.autologin();
                }
                else {
                    console.log("Formazione response!");
                    console.log(data);
                    $scope.vm.tattiche = data.TATTICHE;
                    $scope.vm.tipoBlocco = data.tipo_blocco;
                    data.ROSA.forEach(function (item) {
                        switch (item.CODICE_RUOLO) {
                            case 0:
                                if (item.IN_ROSA == 1) {
                                    item.inFormazione = false;
                                    $scope.vm.portieriRosa.push(item);
                                }
                                break;
                            case 1:
                                if (item.IN_ROSA == 1) {
                                    item.inFormazione = false;
                                    $scope.vm.difensoriRosa.push(item);
                                }
                                break;
                            case 2:
                                if (item.IN_ROSA == 1) {
                                    item.inFormazione = false;
                                    $scope.vm.centrocampistiRosa.push(item);
                                }
                                break;
                            case 3:
                                if (item.IN_ROSA == 1) {
                                    item.inFormazione = false;
                                    $scope.vm.attaccantiRosa.push(item);
                                }
                                break;
                            case 4:
                                if ($rootScope.misterCalcioCup) {
                                    if (item.IN_ROSA == 1) {
                                        item.inFormazione = false;
                                        $scope.vm.allenatoriRosa.push(item);
                                    }
                                }
                                break;
                        }
                        ;
                    });
                    if ($scope.vm.tipoBlocco != 'totale') {
                        $scope.openRosa = _openRosa;
                        $scope.setPosition = _setPosition;
                        $scope.openRosaPanchina = _openRosaPanchina;
                        $scope.popTattiche = _popTattiche;
                    }
                    if (data.FORMAZIONE.length > 0) {
                        if (data.FORMAZIONE[0].TATTICA) {
                            data.TATTICHE.some(function (item) {
                                if (item.TATTICA == data.FORMAZIONE[0].TATTICA) {
                                    $scope.vm.tatticaSelected = item;
                                    return true;
                                }
                            });
                        }
                        else {
                            data.TATTICHE.some(function (item) {
                                if (item.TATTICA == data.FORMAZIONE[0][2]) {
                                    $scope.vm.tatticaSelected = item;
                                    return true;
                                }
                            });
                        }
                        if (data.FORMAZIONE[0].FORMAZIONE && data.FORMAZIONE[0].FORMAZIONE.length > 0) {
                            $scope.vm.portieri = [];
                            $scope.vm.difensori = [];
                            $scope.vm.centrocampisti = [];
                            $scope.vm.attaccanti = [];
                            readFormazione(data.FORMAZIONE[0].FORMAZIONE);
                        }
                        else {
                            //$scope.vm.portieriPanchina.length = 1;
                            //$scope.vm.difensoriPanchina.length = 2;
                            //$scope.vm.centrocampistiPanchina.length = 2;
                            //$scope.vm.attaccantiPanchina.length = 2;
                            //if($rootScope.misterCalcioCup)
                            //	$scope.vm.allenatoriPanchina.length = 1;
                            readFormazione(data.FORMAZIONE[0][4]);
                        }
                    }
                    else {
                        $scope.vm.portieriPanchina.length = 1;
                        $scope.vm.difensoriPanchina.length = 2;
                        $scope.vm.centrocampistiPanchina.length = 2;
                        $scope.vm.attaccantiPanchina.length = 2;
                        if ($rootScope.misterCalcioCup)
                            $scope.vm.allenatoriPanchina.length = 1;
                    }
                    $scope.selectTattica($scope.vm.tatticaSelected);
                    $rootScope.hideLoading('');
                    if (data.msg_blocco) {
                        $rootScope.showToast(data.msg_blocco);
                    }
                }
                $rootScope.hideLoading();
            }, function (data) {
                $rootScope.hideLoading();
                $rootScope.showToast('Errore nella connessione');
            });
        };
        $scope.selectTattica = function (_tattica) {
            $scope.vm.tatticaSelected = _tattica;
            $scope.vm.portieri.length = 1;
            $scope.vm.difensori.length = _tattica.DESIGN.DIFENSORI;
            $scope.vm.difensoriRosa.forEach(function (item, index) {
                var trovato = false;
                item.inFormazione = false;
                for (var i = 0; i < $scope.vm.difensori.length && trovato == false; i++) {
                    if ($scope.vm.difensori[i] && item.ID_GIOCATORE == $scope.vm.difensori[i].ID_GIOCATORE) {
                        item.inFormazione = true;
                        trovato = true;
                    }
                }
                for (var i = 0; i < $scope.vm.difensoriPanchina.length && trovato == false; i++) {
                    if ($scope.vm.difensoriPanchina[i] && item.ID_GIOCATORE == $scope.vm.difensoriPanchina[i].ID_GIOCATORE) {
                        item.inFormazione = true;
                        trovato = true;
                    }
                }
            });
            $scope.vm.centrocampisti.length = _tattica.DESIGN.CENTROCAMPISTI;
            $scope.vm.centrocampistiRosa.forEach(function (item, index) {
                var trovato = false;
                item.inFormazione = false;
                for (var i = 0; i < $scope.vm.centrocampisti.length && trovato == false; i++) {
                    if ($scope.vm.centrocampisti[i] && item.ID_GIOCATORE == $scope.vm.centrocampisti[i].ID_GIOCATORE) {
                        item.inFormazione = true;
                        trovato = true;
                    }
                }
                for (var i = 0; i < $scope.vm.centrocampistiPanchina.length && trovato == false; i++) {
                    if ($scope.vm.centrocampistiPanchina[i] && item.ID_GIOCATORE == $scope.vm.centrocampistiPanchina[i].ID_GIOCATORE) {
                        item.inFormazione = true;
                        trovato = true;
                    }
                }
            });
            $scope.vm.attaccanti.length = _tattica.DESIGN.ATTACCANTI;
            $scope.vm.attaccantiRosa.forEach(function (item, index) {
                var trovato = false;
                item.inFormazione = false;
                for (var i = 0; i < $scope.vm.attaccanti.length && trovato == false; i++) {
                    if ($scope.vm.attaccanti[i] && item.ID_GIOCATORE == $scope.vm.attaccanti[i].ID_GIOCATORE) {
                        item.inFormazione = true;
                        trovato = true;
                    }
                }
                for (var i = 0; i < $scope.vm.attaccantiPanchina.length && trovato == false; i++) {
                    if ($scope.vm.attaccantiPanchina[i] && item.ID_GIOCATORE == $scope.vm.attaccantiPanchina[i].ID_GIOCATORE) {
                        item.inFormazione = true;
                        trovato = true;
                    }
                }
            });
            if ($rootScope.misterCalcioCup) {
                $scope.vm.allenatori.length = 1;
            }
        };
        function _openRosa(ruolo, indice, e) {
            e.stopPropagation();
            if ($scope.vm.rosaShown.ruolo == ruolo && $scope.vm.rosaShown.position == indice) {
                $scope.vm.rosaShown.ruolo = '';
                $scope.vm.rosaShown.position = null;
            }
            else {
                $scope.vm.rosaShown.ruolo = ruolo;
                $scope.vm.rosaShown.position = indice;
            }
        }
        ;
        function _setPosition(ruoloPosition, indice, e) {
            e.stopPropagation();
            $scope.vm.portieriPosition.value = null;
            $scope.vm.difensoriPosition.value = null;
            $scope.vm.centrocampistiPosition.value = null;
            $scope.vm.attaccantiPosition.value = null;
            ruoloPosition.value = indice;
        }
        ;
        $scope.setGiocatore = function (arrayGiocatori, giocatore, indice, e) {
            e.stopPropagation();
            if (giocatore.inFormazione == false) {
                if (arrayGiocatori[indice.value] == null) {
                    giocatore.inFormazione = true;
                    arrayGiocatori[indice.value] = giocatore;
                }
                else {
                    giocatore.inFormazione = true;
                    switch (arrayGiocatori[indice.value].CODICE_RUOLO) {
                        case 0:
                            //cerco nella rosa il giocatore rimosso e tolgo inFormazione
                            $scope.vm.portieriRosa.forEach(function (item, index) {
                                if (arrayGiocatori[indice.value].ID_GIOCATORE == item.ID_GIOCATORE) {
                                    $scope.vm.portieriRosa[index].inFormazione = false;
                                }
                            });
                            break;
                        case 1:
                            //cerco nella rosa il giocatore rimosso e tolgo inFormazione
                            $scope.vm.difensoriRosa.forEach(function (item, index) {
                                if (arrayGiocatori[indice.value].ID_GIOCATORE == item.ID_GIOCATORE) {
                                    $scope.vm.difensoriRosa[index].inFormazione = false;
                                }
                            });
                            break;
                        case 2:
                            //cerco nella rosa il giocatore rimosso e tolgo inFormazione
                            $scope.vm.centrocampistiRosa.forEach(function (item, index) {
                                if (arrayGiocatori[indice.value].ID_GIOCATORE == item.ID_GIOCATORE) {
                                    $scope.vm.centrocampistiRosa[index].inFormazione = false;
                                }
                            });
                            break;
                        case 3:
                            //cerco nella rosa il giocatore rimosso e tolgo inFormazione
                            $scope.vm.attaccantiRosa.forEach(function (item, index) {
                                if (arrayGiocatori[indice.value].ID_GIOCATORE == item.ID_GIOCATORE) {
                                    $scope.vm.attaccantiRosa[index].inFormazione = false;
                                }
                            });
                            break;
                        case 4:
                            //cerco nella rosa il giocatore rimosso e tolgo inFormazione
                            $scope.vm.allenatoriRosa.forEach(function (item, index) {
                                if (arrayGiocatori[indice.value].ID_GIOCATORE == item.ID_GIOCATORE) {
                                    $scope.vm.allenatoriRosa[index].inFormazione = false;
                                }
                            });
                            break;
                    }
                    ;
                    arrayGiocatori[indice.value] = giocatore;
                }
            }
            else {
                if (giocatore != arrayGiocatori[indice.value]) {
                    switch (giocatore.CODICE_RUOLO) {
                        case 0:
                            //se la posizione selezionata non è vuota, tolgo inFormazione dal giocatore nella rosa
                            if (arrayGiocatori[indice.value] != null) {
                                $scope.vm.portieriRosa.forEach(function (item, index) {
                                    if (arrayGiocatori[indice.value].ID_GIOCATORE == item.ID_GIOCATORE) {
                                        $scope.vm.portieriRosa[index].inFormazione = false;
                                    }
                                });
                            }
                            //cerco il giocatore in campo e in panchina, quando lo trovo, nullizzo
                            $scope.vm.portieriPanchina.forEach(function (item, index) {
                                if (item && item.ID_GIOCATORE == giocatore.ID_GIOCATORE) {
                                    $scope.vm.portieriPanchina[index] = null;
                                }
                            });
                            $scope.vm.portieri.forEach(function (item, index) {
                                if (item && item.ID_GIOCATORE == giocatore.ID_GIOCATORE) {
                                    $scope.vm.portieri[index] = null;
                                }
                            });
                            break;
                        case 1:
                            //se la posizione selezionata non è vuota, tolgo inFormazione dal giocatore nella rosa
                            if (arrayGiocatori[indice.value] != null) {
                                $scope.vm.difensoriRosa.forEach(function (item, index) {
                                    if (arrayGiocatori[indice.value].ID_GIOCATORE == item.ID_GIOCATORE) {
                                        $scope.vm.difensoriRosa[index].inFormazione = false;
                                    }
                                });
                            }
                            //cerco il giocatore in campo e in panchina, quando lo trovo, nullizzo
                            $scope.vm.difensoriPanchina.forEach(function (item, index) {
                                if (item && item.ID_GIOCATORE == giocatore.ID_GIOCATORE) {
                                    $scope.vm.difensoriPanchina[index] = null;
                                }
                            });
                            $scope.vm.difensori.forEach(function (item, index) {
                                if (item && item.ID_GIOCATORE == giocatore.ID_GIOCATORE) {
                                    $scope.vm.difensori[index] = null;
                                }
                            });
                            break;
                        case 2:
                            //se la posizione selezionata non è vuota, tolgo inFormazione dal giocatore nella rosa
                            if (arrayGiocatori[indice.value] != null) {
                                $scope.vm.centrocampistiRosa.forEach(function (item, index) {
                                    if (arrayGiocatori[indice.value].ID_GIOCATORE == item.ID_GIOCATORE) {
                                        $scope.vm.centrocampistiRosa[index].inFormazione = false;
                                    }
                                });
                            }
                            //cerco il giocatore in campo e in panchina, quando lo trovo, nullizzo
                            $scope.vm.centrocampistiPanchina.forEach(function (item, index) {
                                if (item && item.ID_GIOCATORE == giocatore.ID_GIOCATORE) {
                                    $scope.vm.centrocampistiPanchina[index] = null;
                                }
                            });
                            $scope.vm.centrocampisti.forEach(function (item, index) {
                                if (item && item.ID_GIOCATORE == giocatore.ID_GIOCATORE) {
                                    $scope.vm.centrocampisti[index] = null;
                                }
                            });
                            break;
                        case 3:
                            //se la posizione selezionata non è vuota, tolgo inFormazione dal giocatore nella rosa
                            if (arrayGiocatori[indice.value] != null) {
                                $scope.vm.attaccantiRosa.forEach(function (item, index) {
                                    if (arrayGiocatori[indice.value].ID_GIOCATORE == item.ID_GIOCATORE) {
                                        $scope.vm.attaccantiRosa[index].inFormazione = false;
                                    }
                                });
                            }
                            //cerco il giocatore in campo e in panchina, quando lo trovo, nullizzo
                            $scope.vm.attaccantiPanchina.forEach(function (item, index) {
                                if (item && item.ID_GIOCATORE == giocatore.ID_GIOCATORE) {
                                    $scope.vm.attaccantiPanchina[index] = null;
                                }
                            });
                            $scope.vm.attaccanti.forEach(function (item, index) {
                                if (item && item.ID_GIOCATORE == giocatore.ID_GIOCATORE) {
                                    $scope.vm.attaccanti[index] = null;
                                }
                            });
                            break;
                        case 4:
                            //se la posizione selezionata non è vuota, tolgo inFormazione dal giocatore nella rosa
                            if (arrayGiocatori[indice.value] != null) {
                                $scope.vm.allenatoriRosa.forEach(function (item, index) {
                                    if (arrayGiocatori[indice.value].ID_GIOCATORE == item.ID_GIOCATORE) {
                                        $scope.vm.allenatoriRosa[index].inFormazione = false;
                                    }
                                });
                            }
                            //cerco il giocatore in campo e in panchina, quando lo trovo, nullizzo
                            $scope.vm.allenatoriPanchina.forEach(function (item, index) {
                                if (item && item.ID_GIOCATORE == giocatore.ID_GIOCATORE) {
                                    $scope.vm.allenatoriPanchina[index] = null;
                                }
                            });
                            $scope.vm.allenatori.forEach(function (item, index) {
                                if (item && item.ID_GIOCATORE == giocatore.ID_GIOCATORE) {
                                    $scope.vm.allenatori[index] = null;
                                }
                            });
                            break;
                    }
                    ;
                }
                arrayGiocatori[indice.value] = giocatore;
            }
            if (indice.value == arrayGiocatori.length - 1)
                indice.value = 0;
            else
                indice.value++;
        };
        function _openRosaPanchina(ruolo, ruoloPosition, indice, e) {
            $scope.vm.portieriPanchinaPosition.value = null;
            $scope.vm.difensoriPanchinaPosition.value = null;
            $scope.vm.centrocampistiPanchinaPosition.value = null;
            $scope.vm.attaccantiPanchinaPosition.value = null;
            ruoloPosition.value = indice;
            $scope.vm.rosaPanchinaShown = ruolo;
            $scope.popoverPanchinaRosa.show(e);
        }
        ;
        $scope.firstLetter = function (text) {
            if (text && text.trim())
                return text.charAt(0) + '.';
            else
                return '';
        };
        $scope.popPanchina = function (e) {
            e.stopPropagation();
            $scope.vm.rosaShown.ruolo = '';
            $scope.vm.rosaShown.position = null;
            $scope.popoverPanchina.show(e);
        };
        function _popTattiche(e) {
            e.stopPropagation();
            $scope.vm.rosaShown.ruolo = '';
            $scope.vm.rosaShown.position = null;
            $scope.popoverTattiche.show(e);
        }
        ;
        $scope.ruoloGiocatoreSelected = function () {
            if ($scope.vm.rosaPanchinaShown == 'P') {
                return $scope.vm.portieriRosa.length;
            }
            if ($scope.vm.rosaPanchinaShown == 'D') {
                return $scope.vm.difensoriRosa.length;
            }
            if ($scope.vm.rosaPanchinaShown == 'C') {
                return $scope.vm.centrocampistiRosa.length;
            }
            if ($scope.vm.rosaPanchinaShown == 'A') {
                return $scope.vm.attaccantiRosa.length;
            }
            if ($scope.vm.rosaPanchinaShown == 'AL') {
                return $scope.vm.allenatoriRosa.length;
            }
        };
        $scope.saveFormazione = function () {
            $rootScope.showLoading();
            var formazioneValida = true;
            var primoBuco = -1;
            var formazione = [].concat($scope.vm.portieri, $scope.vm.difensori, $scope.vm.centrocampisti, $scope.vm.attaccanti, $scope.vm.portieriPanchina, $scope.vm.difensoriPanchina, $scope.vm.centrocampistiPanchina, $scope.vm.attaccantiPanchina);
            if ($rootScope.misterCalcioCup) {
                formazione = formazione.concat($scope.vm.allenatori, $scope.vm.allenatoriPanchina);
                if ($scope.vm.allenatori[0] == null) {
                    formazioneValida = false;
                    primoBuco = 1;
                }
                else if ($scope.vm.allenatoriPanchina[0] == null) {
                    formazioneValida = false;
                    primoBuco = 11;
                }
            }
            var add = '';
            ;
            for (var i = 0; i < formazione.length && formazioneValida == true; i++) {
                if (formazione[i] != null) {
                    add += formazione[i].ID_GIOCATORE + ':' + i + ';';
                }
                else {
                    formazioneValida = false;
                    primoBuco = i;
                }
            }
            if (formazioneValida == false) {
                if (primoBuco <= 10) {
                    $rootScope.hideLoading();
                    $rootScope.showToast('Formazione incompleta. Schierare tutti i giocatori in campo');
                }
                else {
                    $rootScope.hideLoading();
                    $rootScope.showToast('Formazione incompleta. Schierare tutti i giocatori in panchina');
                }
            }
            else {
                if ($scope.vm.formazione == '' || $scope.vm.formazione == add) {
                    FantaCalcioServices.salvaFormazione($rootScope.modoGioco, $rootScope.squadraSelected.codice_squadra, $scope.vm.tatticaSelected.TATTICA, add.substr(0, add.length - 1))
                        .then(function (data) {
                        if (data.success) {
                            $scope.vm.formazione = add;
                            $rootScope.showToast(data.success.msg_utente);
                        }
                        else {
                            $rootScope.showToast(data.error.user_message);
                            if (data.error && data.error.id == 'not_logged')
                                $rootScope.autologin();
                        }
                        $rootScope.hideLoading();
                    }, function (data) {
                        $rootScope.hideLoading();
                        $rootScope.showToast('Errore nella connessione');
                    });
                }
                else {
                    var remove = '';
                    var addSplit = add.split(';');
                    add = '';
                    var formazioneSplit = $scope.vm.formazione.split(';');
                    for (var i = 0; i < addSplit.length - 1; i++) {
                        if (addSplit[i] != formazioneSplit[i]) {
                            var giocatoreRimosso = formazioneSplit[i].split(':');
                            var giocatoreModificato = addSplit[i].split(':');
                            remove += giocatoreRimosso[0] + ';';
                            add += giocatoreModificato[0] + ':' + i + ';';
                        }
                    }
                    remove = remove.substring(0, remove.length - 1);
                    add = add.substring(0, add.length - 1);
                    console.log("Remove e add prima della chiamata");
                    console.log(remove);
                    console.log(add);
                    FantaCalcioServices.modificaFormazione($rootScope.modoGioco, $rootScope.squadraSelected.codice_squadra, $scope.vm.tatticaSelected.TATTICA, remove, add).then(function (data) {
                        if (data.success) {
                            $scope.vm.formazione = addSplit.join(';');
                            $rootScope.showToast(data.success.msg_utente);
                        }
                        else {
                            $rootScope.showToast(data.error.user_message);
                        }
                        $rootScope.hideLoading();
                    }, function (data) {
                        $rootScope.hideLoading();
                        $rootScope.showToast('Errore nella connessione');
                    });
                }
            }
            //$rootScope.hideLoading();
        };
        $scope.deleteFormazione = function () {
            $rootScope.showLoading();
            FantaCalcioServices.cancellaFormazione($rootScope.modoGioco, $rootScope.squadraSelected.codice_squadra, $scope.vm.tatticaSelected.TATTICA)
                .then(function (data) {
                if (data.success) {
                    $scope.vm.portieri.forEach(function (item, index) {
                        $scope.vm.portieri[index] = null;
                    });
                    $scope.vm.portieriPanchina.forEach(function (item, index) {
                        $scope.vm.portieriPanchina[index] = null;
                    });
                    $scope.vm.portieriRosa.forEach(function (item) {
                        item.inFormazione = false;
                    });
                    $scope.vm.difensori.forEach(function (item, index) {
                        $scope.vm.difensori[index] = null;
                    });
                    $scope.vm.difensoriPanchina.forEach(function (item, index) {
                        $scope.vm.difensoriPanchina[index] = null;
                    });
                    $scope.vm.difensoriRosa.forEach(function (item) {
                        item.inFormazione = false;
                    });
                    $scope.vm.centrocampisti.forEach(function (item, index) {
                        $scope.vm.centrocampisti[index] = null;
                    });
                    $scope.vm.centrocampistiPanchina.forEach(function (item, index) {
                        $scope.vm.centrocampistiPanchina[index] = null;
                    });
                    $scope.vm.centrocampistiRosa.forEach(function (item) {
                        item.inFormazione = false;
                    });
                    $scope.vm.attaccanti.forEach(function (item, index) {
                        $scope.vm.attaccanti[index] = null;
                    });
                    $scope.vm.attaccantiPanchina.forEach(function (item, index) {
                        $scope.vm.attaccantiPanchina[index] = null;
                    });
                    $scope.vm.attaccantiRosa.forEach(function (item) {
                        item.inFormazione = false;
                    });
                    if ($rootScope.misterCalcioCup) {
                        $scope.vm.allenatori.forEach(function (item, index) {
                            $scope.vm.allenatori[index] = null;
                        });
                        $scope.vm.allenatoriPanchina.forEach(function (item, index) {
                            $scope.vm.allenatoriPanchina[index] = null;
                        });
                        $scope.vm.allenatoriRosa.forEach(function (item) {
                            item.inFormazione = false;
                        });
                    }
                    $scope.vm.rosaShown = { ruolo: '', position: null };
                    $scope.vm.formazione = '';
                    $rootScope.showToast(data.success.msg_utente);
                }
                else {
                    $rootScope.showToast(data.error.user_message);
                }
                $rootScope.hideLoading();
            }, function (data) {
                $rootScope.hideLoading();
                $rootScope.showToast('Errore nella connessione');
            });
        };
        $ionicPopover.fromTemplateUrl('popoverTattiche.html', {
            scope: $scope,
        }).then(function (popover) {
            $scope.popoverTattiche = popover;
        });
        $ionicPopover.fromTemplateUrl('popoverPanchina.html', {
            scope: $scope,
        }).then(function (popover) {
            $scope.popoverPanchina = popover;
        });
        $ionicPopover.fromTemplateUrl('popoverPanchinaRosa.html', {
            scope: $scope,
        }).then(function (popover) {
            $scope.popoverPanchinaRosa = popover;
        });
        $scope.$on('$destroy', function () {
            $scope.popoverTattiche.remove();
            $scope.popoverPanchina.remove();
            $scope.popoverPanchinaRosa.remove();
        });
    });
    function getGiocatoreById(rosa, idGiocatore) {
        var ris = null;
        rosa.some(function (item) {
            if (item.ID_GIOCATORE == idGiocatore) {
                ris = item;
                return true;
            }
        });
        if (ris == null) {
            ris = {
                CODICE_RUOLO: -1,
                COGNOME: '',
                ID_GIOCATORE: -1,
                IN_ROSA: 0,
                NOME: '',
                SQUADRA: '',
                inFormazione: false
            };
        }
        return ris;
    }
    ;
})();
/// <reference path="../../../typings/tsd.d.ts" />
/// <reference path="../model/model.ts" />
(function () {
    var app = angular.module('gestioneRosaCtrl', []);
    app.controller('gestioneRosaCtrl', function ($scope, $rootScope, $state, $ionicLoading, $stateParams, $ionicHistory, $ionicModal, $timeout, $location, $ionicScrollDelegate, FantaCalcioServices, RosaService) {
        $scope.vm = {
            portieriRosa: [],
            totalePortieri: 0,
            difensoriRosa: [],
            totaleDifensori: 0,
            centrocampistiRosa: [],
            totaleCentrocampisti: 0,
            attaccantiRosa: [],
            totaleAttaccanti: 0,
            allenatoriRosa: [],
            totaleAllenatori: 0,
            rosaShown: '',
            indiceShown: -1,
            saldo: 0,
            venduti: '',
            acquistati: '',
            giocatoriDisponibili: [],
            squadreFiltro: [],
            //variabili usate solo per la visualizzazione
            //bloccoRosa: false,
            totaleRosa: 0,
            portieriLength: 0,
            difensoriLength: 0,
            centrocampistiLength: 0,
            attaccantiLength: 0,
            allenatoriLength: 0,
            totaleCosto: 0
        };
        $scope.filter = {
            ordinaPerNome: true,
            ordinaCrescente: true,
            filtroSquadra: [],
            minCosto: 1,
            maxCosto: 46,
        };
        function _vendiGiocatore(giocatore) {
            var prezzoVendita;
            if (RosaService.getOfferte() == true)
                prezzoVendita = giocatore.COSTO_ACQUISTO;
            else
                prezzoVendita = giocatore.VALORE_ATTUALE;
            var confirmText = "Sei sicuro di voler vendere " + giocatore.COGNOME + " al prezzo di " + prezzoVendita + " crediti?";
            if (window.cordova) {
                navigator.notification.confirm(confirmText, function (buttonPressed) {
                    if (buttonPressed == 1) {
                        venditaConfermata(giocatore, prezzoVendita);
                        $scope.$apply();
                    }
                }, 'Vendi giocatore', ['Conferma', 'Annulla']);
            }
            else {
                if (confirm(confirmText)) {
                    venditaConfermata(giocatore, prezzoVendita);
                }
            }
        }
        ;
        function venditaConfermata(giocatore, prezzoVendita) {
            switch (giocatore.RUOLO) {
                case 0:
                    $scope.vm.portieriRosa[$scope.vm.portieriRosa.indexOf(giocatore)] = { RUOLO: 0 };
                    if ($scope.vm.venduti.indexOf(giocatore.ID) == -1)
                        $scope.vm.venduti += giocatore.ID + ';';
                    if ($scope.vm.acquistati.indexOf(giocatore.ID) > -1) {
                        var stringaAcquistati = giocatore.ID;
                        if (RosaService.getOfferte()) {
                            stringaAcquistati += ':' + prezzoVendita;
                        }
                        var splitted = $scope.vm.acquistati.split(";");
                        splitted.splice(splitted.indexOf(String(stringaAcquistati)), 1);
                        $scope.vm.acquistati = splitted.join(";");
                        RosaService.setGiocatoriAcquistati($scope.vm.acquistati);
                    }
                    $scope.vm.saldo += prezzoVendita;
                    RosaService.setGiocatoriVenduti($scope.vm.venduti);
                    RosaService.giocatoreRemovedFromRosa(giocatore);
                    break;
                case 1:
                    $scope.vm.difensoriRosa[$scope.vm.difensoriRosa.indexOf(giocatore)] = { RUOLO: 1 };
                    if ($scope.vm.venduti.indexOf(giocatore.ID) == -1)
                        $scope.vm.venduti += giocatore.ID + ';';
                    if ($scope.vm.acquistati.indexOf(giocatore.ID) > -1) {
                        var stringaAcquistati = giocatore.ID;
                        if (RosaService.getOfferte()) {
                            stringaAcquistati += ':' + prezzoVendita;
                        }
                        var splitted = $scope.vm.acquistati.split(";");
                        splitted.splice(splitted.indexOf(String(stringaAcquistati)), 1);
                        $scope.vm.acquistati = splitted.join(";");
                        RosaService.setGiocatoriAcquistati($scope.vm.acquistati);
                    }
                    $scope.vm.saldo += prezzoVendita;
                    RosaService.setGiocatoriVenduti($scope.vm.venduti);
                    RosaService.giocatoreRemovedFromRosa(giocatore);
                    break;
                case 2:
                    $scope.vm.centrocampistiRosa[$scope.vm.centrocampistiRosa.indexOf(giocatore)] = { RUOLO: 2 };
                    if ($scope.vm.venduti.indexOf(giocatore.ID) == -1)
                        $scope.vm.venduti += giocatore.ID + ';';
                    if ($scope.vm.acquistati.indexOf(giocatore.ID) > -1) {
                        var stringaAcquistati = giocatore.ID;
                        if (RosaService.getOfferte()) {
                            stringaAcquistati += ':' + prezzoVendita;
                        }
                        var splitted = $scope.vm.acquistati.split(";");
                        splitted.splice(splitted.indexOf(String(stringaAcquistati)), 1);
                        $scope.vm.acquistati = splitted.join(";");
                        RosaService.setGiocatoriAcquistati($scope.vm.acquistati);
                    }
                    $scope.vm.saldo += prezzoVendita;
                    RosaService.setGiocatoriVenduti($scope.vm.venduti);
                    RosaService.giocatoreRemovedFromRosa(giocatore);
                    break;
                case 3:
                    $scope.vm.attaccantiRosa[$scope.vm.attaccantiRosa.indexOf(giocatore)] = { RUOLO: 3 };
                    if ($scope.vm.venduti.indexOf(giocatore.ID) == -1)
                        $scope.vm.venduti += giocatore.ID + ';';
                    if ($scope.vm.acquistati.indexOf(giocatore.ID) > -1) {
                        var stringaAcquistati = giocatore.ID;
                        if (RosaService.getOfferte()) {
                            stringaAcquistati += ':' + prezzoVendita;
                        }
                        var splitted = $scope.vm.acquistati.split(";");
                        splitted.splice(splitted.indexOf(String(stringaAcquistati)), 1);
                        $scope.vm.acquistati = splitted.join(";");
                        RosaService.setGiocatoriAcquistati($scope.vm.acquistati);
                    }
                    $scope.vm.saldo += prezzoVendita;
                    RosaService.setGiocatoriVenduti($scope.vm.venduti);
                    RosaService.giocatoreRemovedFromRosa(giocatore);
                    break;
                case 4:
                    $scope.vm.allenatoriRosa[$scope.vm.allenatoriRosa.indexOf(giocatore)] = { RUOLO: 4 };
                    if ($scope.vm.venduti.indexOf(giocatore.ID) == -1)
                        $scope.vm.venduti += giocatore.ID + ';';
                    if ($scope.vm.acquistati.indexOf(giocatore.ID) > -1) {
                        var stringaAcquistati = giocatore.ID;
                        if (RosaService.getOfferte()) {
                            stringaAcquistati += ':' + prezzoVendita;
                        }
                        var splitted = $scope.vm.acquistati.split(";");
                        splitted.splice(splitted.indexOf(String(stringaAcquistati)), 1);
                        $scope.vm.acquistati = splitted.join(";");
                        RosaService.setGiocatoriAcquistati($scope.vm.acquistati);
                    }
                    $scope.vm.saldo += prezzoVendita;
                    RosaService.setGiocatoriVenduti($scope.vm.venduti);
                    RosaService.giocatoreRemovedFromRosa(giocatore);
                    break;
            }
            ;
            $scope.totaleRosa();
            $scope.vm.portieriLength = _lengthEffettiva($scope.vm.portieriRosa);
            $scope.vm.difensoriLength = _lengthEffettiva($scope.vm.difensoriRosa);
            $scope.vm.centrocampistiLength = _lengthEffettiva($scope.vm.centrocampistiRosa);
            $scope.vm.attaccantiLength = _lengthEffettiva($scope.vm.attaccantiRosa);
            $scope.vm.allenatoriLength = _lengthEffettiva($scope.vm.allenatoriRosa);
            $scope.vm.totaleCosto = $scope.totaleCosto();
        }
        ;
        $scope.totaleRosa = function () {
            if ($rootScope.misterCalcioCup)
                $scope.vm.totaleRosa = _lengthEffettiva($scope.vm.portieriRosa) + _lengthEffettiva($scope.vm.difensoriRosa) +
                    _lengthEffettiva($scope.vm.centrocampistiRosa) + _lengthEffettiva($scope.vm.attaccantiRosa)
                    + _lengthEffettiva($scope.vm.allenatoriRosa);
            else
                $scope.vm.totaleRosa = _lengthEffettiva($scope.vm.portieriRosa) + _lengthEffettiva($scope.vm.difensoriRosa) +
                    _lengthEffettiva($scope.vm.centrocampistiRosa) + _lengthEffettiva($scope.vm.attaccantiRosa);
        };
        $scope.totaleCosto = function () {
            var totale = 0;
            $scope.vm.portieriRosa.forEach(function (item) {
                if (item.COGNOME)
                    totale += item.COSTO_ACQUISTO;
            });
            $scope.vm.difensoriRosa.forEach(function (item) {
                if (item.COGNOME)
                    totale += item.COSTO_ACQUISTO;
            });
            $scope.vm.centrocampistiRosa.forEach(function (item) {
                if (item.COGNOME)
                    totale += item.COSTO_ACQUISTO;
            });
            $scope.vm.attaccantiRosa.forEach(function (item) {
                if (item.COGNOME)
                    totale += item.COSTO_ACQUISTO;
            });
            if ($rootScope.misterCalcioCup) {
                $scope.vm.allenatoriRosa.forEach(function (item) {
                    if (item.COGNOME)
                        totale += item.COSTO_ACQUISTO;
                });
            }
            return totale;
        };
        function _aggiungiGiocatore(ruolo, index) {
            $rootScope.showLoading();
            $timeout(function () {
                RosaService.setLastSelected({ ruolo: $scope.vm.rosaShown, index: index });
                if ($rootScope.misterCalcioCup)
                    RosaService.setRosa($scope.vm.portieriRosa, $scope.vm.difensoriRosa, $scope.vm.centrocampistiRosa, $scope.vm.attaccantiRosa, $scope.vm.allenatoriRosa);
                else
                    RosaService.setRosa($scope.vm.portieriRosa, $scope.vm.difensoriRosa, $scope.vm.centrocampistiRosa, $scope.vm.attaccantiRosa);
                RosaService.setGiocatoriVenduti($scope.vm.venduti);
                RosaService.setSaldo($scope.vm.saldo);
                $state.go("app.aggiungiGiocatore", { ruoloGiocatore: ruolo });
            }, 10);
        }
        ;
        function _lengthEffettiva(array) {
            if (array) {
                var ris = 0;
                array.forEach(function (item) {
                    if (item.COGNOME)
                        ris++;
                });
                return ris;
            }
        }
        ;
        $scope.confermaRosa = function () {
            $rootScope.showLoading();
            var saldoOk = false;
            var numGiocatoriOk = false;
            if (RosaService.getOfferte()) {
                if ($scope.vm.saldo == 0)
                    saldoOk = true;
            }
            else {
                saldoOk = true;
            }
            if ($rootScope.misterCalcioCup) {
                if ($scope.vm.totaleRosa == 27)
                    numGiocatoriOk = true;
            }
            else {
                if ($scope.vm.totaleRosa == 25)
                    numGiocatoriOk = true;
            }
            if (saldoOk && numGiocatoriOk) {
                if (RosaService.getPrimaRosa()) {
                    $scope.vm.venduti = '';
                }
                FantaCalcioServices.modificaRosa($rootScope.squadraSelected.codice_squadra, $scope.vm.venduti.substr(0, $scope.vm.venduti.length - 1), $scope.vm.acquistati.substr(0, $scope.vm.acquistati.length - 1)).then(function (data) {
                    if (data.error) {
                        $rootScope.showToast(data.error.user_message);
                    }
                    else {
                        $scope.vm.venduti = '';
                        $scope.vm.acquistati = '';
                        RosaService.setGiocatoriVenduti('');
                        RosaService.setGiocatoriAcquistati('');
                        RosaService.setPrimaRosa(false);
                        $rootScope.showToast(data.success.msg_utente, 5000);
                    }
                    $rootScope.hideLoading();
                }, function (data) {
                    $rootScope.hideLoading();
                    $rootScope.showToast('Errore nella connessione');
                });
            }
            else {
                $rootScope.hideLoading();
                $timeout(function () {
                    if (numGiocatoriOk == false) {
                        if ($rootScope.misterCalcioCup)
                            if (window.cordova)
                                navigator.notification.alert('Inserire tutti i 27 giocatori', function () { }, 'Rosa');
                            else
                                alert('Inserire tutti i 27 giocatori');
                        else if (window.cordova)
                            navigator.notification.alert('Inserire tutti i 25 giocatori', function () { }, 'Rosa');
                        else
                            alert('Inserire tutti i 25 giocatori');
                    }
                    else if (saldoOk == false) {
                        if (window.cordova)
                            navigator.notification.alert('Devi esaurire il saldo prima di confermare la rosa!', function () { }, 'Rosa');
                        else
                            alert('Devi esaurire il saldo prima di confermare la rosa!');
                    }
                }, 5);
            }
        };
        $scope.openRosa = function (ruolo) {
            if ($scope.vm.rosaShown == ruolo) {
                $scope.vm.rosaShown = '';
            }
            else {
                $scope.vm.rosaShown = ruolo;
            }
            $timeout(function () {
                //$location.hash(ruolo);
                //console.log($location.hash());
                //$ionicScrollDelegate.$getByHandle('contentScroll').anchorScroll(true);
                $ionicScrollDelegate.$getByHandle('contentScroll').resize();
            }, 200);
        };
        $scope.firstLetter = function (text) {
            if (text)
                return text.charAt(0) + '.';
            else
                return '';
        };
        function rosaInit() {
            if ($rootScope.rosaInCorso == false) {
                FantaCalcioServices.gestioneRosa($rootScope.modoGioco, $rootScope.squadraSelected.codice_squadra)
                    .then(function (data) {
                    if (data.error) {
                        $rootScope.showToast(data.error.user_message, 5000);
                    }
                    else {
                        console.log(data);
                        RosaService.setGiocatoriAcquistati('');
                        RosaService.setGiocatoriVenduti('');
                        if ($rootScope.modoGioco != 3) {
                            if (data.asta_conclusa == true)
                                RosaService.setOfferte(false);
                            else
                                RosaService.setOfferte(true);
                        }
                        else {
                            RosaService.setOfferte(false);
                        }
                        $scope.vm.giocatoriDisponibili = data.GIOCATORI_DISPONIBILI;
                        RosaService.setGiocatoriDisponibili($scope.vm.giocatoriDisponibili);
                        $scope.vm.saldo = data.saldo;
                        if (data.ROSA.length > 0) {
                            RosaService.setPrimaRosa(false);
                            data.ROSA.forEach(function (item) {
                                switch (item.RUOLO) {
                                    case 0:
                                        $scope.vm.portieriRosa.push(item);
                                        break;
                                    case 1:
                                        $scope.vm.difensoriRosa.push(item);
                                        break;
                                    case 2:
                                        $scope.vm.centrocampistiRosa.push(item);
                                        break;
                                    case 3:
                                        $scope.vm.attaccantiRosa.push(item);
                                        break;
                                    case 4:
                                        $scope.vm.allenatoriRosa.push(item);
                                        break;
                                }
                                ;
                            });
                            for (var i = $scope.vm.portieriRosa.length; i < 3; ++i) {
                                $scope.vm.portieriRosa.push({ RUOLO: 0 });
                            }
                            for (var i = $scope.vm.difensoriRosa.length; i < 8; i++) {
                                $scope.vm.difensoriRosa.push({ RUOLO: 1 });
                            }
                            for (var i = $scope.vm.centrocampistiRosa.length; i < 8; i++) {
                                $scope.vm.centrocampistiRosa.push({ RUOLO: 2 });
                            }
                            for (var i = $scope.vm.attaccantiRosa.length; i < 6; i++) {
                                $scope.vm.attaccantiRosa.push({ RUOLO: 3 });
                            }
                            if ($rootScope.misterCalcioCup) {
                                for (var i = $scope.vm.allenatoriRosa.length; i < 2; i++) {
                                    $scope.vm.allenatoriRosa.push({ RUOLO: 4 });
                                }
                            }
                        }
                        else {
                            RosaService.setPrimaRosa(true);
                            for (var i = 0; i < 3; i++) {
                                $scope.vm.portieriRosa.push({ RUOLO: 0 });
                            }
                            for (var i = 0; i < 8; i++) {
                                $scope.vm.difensoriRosa.push({ RUOLO: 1 });
                            }
                            for (var i = 0; i < 8; i++) {
                                $scope.vm.centrocampistiRosa.push({ RUOLO: 2 });
                            }
                            for (var i = 0; i < 6; i++) {
                                $scope.vm.attaccantiRosa.push({ RUOLO: 3 });
                            }
                            if ($rootScope.misterCalcioCup) {
                                for (var i = 0; i < 2; i++) {
                                    $scope.vm.allenatoriRosa.push({ RUOLO: 4 });
                                }
                            }
                        }
                        $rootScope.showToast(data.msg_blocco_rosa, 5000);
                        if (data.error && data.error.id == 'not_logged') {
                            $rootScope.autologin();
                        }
                    }
                    if (data.bloccoRosa || data.campDone) {
                        RosaService.setBloccoRosa(true);
                        $scope.vm.bloccoRosa = true;
                    }
                    else {
                        RosaService.setBloccoRosa(false);
                        $scope.vm.bloccoRosa = false;
                        $scope.aggiungiGiocatore = _aggiungiGiocatore;
                        $scope.vendiGiocatore = _vendiGiocatore;
                    }
                    $scope.totaleRosa();
                    $scope.vm.portieriLength = _lengthEffettiva($scope.vm.portieriRosa);
                    $scope.vm.difensoriLength = _lengthEffettiva($scope.vm.difensoriRosa);
                    $scope.vm.centrocampistiLength = _lengthEffettiva($scope.vm.centrocampistiRosa);
                    $scope.vm.attaccantiLength = _lengthEffettiva($scope.vm.attaccantiRosa);
                    $scope.vm.allenatoriLength = _lengthEffettiva($scope.vm.allenatoriRosa);
                    $scope.vm.totaleCosto = $scope.totaleCosto();
                    $rootScope.hideLoading();
                }, function (data) {
                    $rootScope.hideLoading();
                    $rootScope.showToast('Errore nella connessione');
                });
            }
            else {
                RosaService.getRosa().forEach(function (item) {
                    switch (item.RUOLO) {
                        case 0:
                            $scope.vm.portieriRosa.push(item);
                            break;
                        case 1:
                            $scope.vm.difensoriRosa.push(item);
                            break;
                        case 2:
                            $scope.vm.centrocampistiRosa.push(item);
                            break;
                        case 3:
                            $scope.vm.attaccantiRosa.push(item);
                            break;
                        case 4:
                            $scope.vm.allenatoriRosa.push(item);
                            break;
                    }
                    ;
                });
                $scope.vm.venduti = RosaService.getGiocatoriVenduti();
                $scope.vm.acquistati = RosaService.getGiocatoriAcquistati();
                $scope.vm.rosaShown = RosaService.getLastSelected().ruolo;
                $scope.vm.indiceShown = RosaService.getLastSelected().index;
                $scope.vm.giocatoriDisponibili = RosaService.getGiocatoriDisponibili();
                $scope.vm.saldo = RosaService.getSaldo();
                if (RosaService.getBloccoRosa()) {
                    $scope.vm.bloccoRosa = true;
                }
                else {
                    $scope.vm.bloccoRosa = false;
                    $scope.aggiungiGiocatore = _aggiungiGiocatore;
                    $scope.vendiGiocatore = _vendiGiocatore;
                }
                $scope.totaleRosa();
                $scope.vm.portieriLength = _lengthEffettiva($scope.vm.portieriRosa);
                $scope.vm.difensoriLength = _lengthEffettiva($scope.vm.difensoriRosa);
                $scope.vm.centrocampistiLength = _lengthEffettiva($scope.vm.centrocampistiRosa);
                $scope.vm.attaccantiLength = _lengthEffettiva($scope.vm.attaccantiRosa);
                $scope.vm.allenatoriLength = _lengthEffettiva($scope.vm.allenatoriRosa);
                $scope.vm.totaleCosto = $scope.totaleCosto();
                $rootScope.hideLoading();
            }
        }
        ;
        $scope.$on('$ionicView.beforeEnter', rosaInit());
        $scope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
            if (toState.name == 'app.aggiungiGiocatore') {
                $rootScope.rosaInCorso = true;
            }
            else {
                $rootScope.rosaInCorso = false;
            }
        });
    });
    function isInteger(x) {
        return x % 1 === 0;
    }
})();
/// <reference path="../../../typings/tsd.d.ts" />
/// <reference path="../model/model.ts" />
(function () {
    var app = angular.module('loginCtrl', []);
    app.controller('loginCtrl', function ($scope, $rootScope, $state, $ionicLoading, $stateParams, $ionicHistory, FantaCalcioServices) {
        $scope.vm = {
            email: '',
            password: '',
            modoGioco: 2
        };
        $scope.counterLogin = 0;
        $scope.loginInit = function () {
            $rootScope.misterCalcioCup = false;
            if ($rootScope.misterCalcioCup) {
                var loginData = JSON.parse(window.localStorage.getItem('misterCalcioCupLegheLogin'));
            }
            else {
                var loginData = JSON.parse(window.localStorage.getItem('tuttoSportLeagueLegheLogin'));
            }
            if (loginData && loginData.email) {
                $scope.vm.email = loginData.email;
                $scope.vm.password = loginData.password;
            }
        };
        $scope.login = function () {
            if ($scope.counterLogin == 0)
                $rootScope.showLoading("Login in corso");
            $scope.counterLogin++;
            FantaCalcioServices.authentication($scope.vm.email, $scope.vm.password, $scope.vm.modoGioco).then(function (data) {
                if (data.success) {
                    console.log(data);
                    $rootScope.modoGioco = $scope.vm.modoGioco;
                    if ($rootScope.misterCalcioCup) {
                        window.localStorage.setItem('misterCalcioCupLegheLogin', JSON.stringify($scope.vm));
                    }
                    else {
                        window.localStorage.setItem('tuttoSportLeagueLegheLogin', JSON.stringify($scope.vm));
                    }
                    $rootScope.user = data.success.user;
                    $ionicHistory.nextViewOptions({
                        disableAnimate: true,
                        disableBack: true,
                        historyRoot: true
                    });
                    $state.go('app.home');
                }
                else if (data.error) {
                    console.log(data);
                    if ($scope.counterLogin <= 3) {
                        $scope.login();
                    }
                    else {
                        $rootScope.hideLoading();
                        $rootScope.showToast(data.error.user_message, 5000);
                    }
                }
            }, function (data) {
                $rootScope.hideLoading();
                $rootScope.showToast('Errore nella connessione');
            });
        };
        $scope.facebookLogin = function () {
            if (!ionic.Platform.isIOS()) {
                $rootScope.showLoading();
            }
            facebookConnectPlugin.setApplicationId(['606280542738883'], function (resp) {
                console.log(resp);
                facebookConnectPlugin.login(['email'], function (FBSuccessResponse) {
                    console.log("facebook login success data: ");
                    console.log(FBSuccessResponse);
                    FantaCalcioServices.facebookAuthentication($scope.vm.email, $scope.vm.password, $scope.vm.modoGioco, FBSuccessResponse.authResponse.accessToken).then(function (data) {
                        if (data.success) {
                            console.log(data);
                            $rootScope.modoGioco = $scope.vm.modoGioco;
                            if ($rootScope.misterCalcioCup) {
                                window.localStorage.setItem('misterCalcioCupLegheLogin', JSON.stringify($scope.vm));
                            }
                            else {
                                window.localStorage.setItem('tuttoSportLeagueLegheLogin', JSON.stringify($scope.vm));
                            }
                            $rootScope.user = data.success.user;
                            $rootScope.accessToken = data.success.user.accessToken;
                            if (data.success.required_reg == 1) {
                                if (data.success.required_email == 1)
                                    $state.go('requiredReg', { requiredMail: 1 });
                                else
                                    $state.go('requiredReg', { requiredMail: 0 });
                            }
                            else {
                                $ionicHistory.nextViewOptions({
                                    disableAnimate: true,
                                    disableBack: true,
                                    historyRoot: true
                                });
                                $state.go('app.home');
                            }
                        }
                        else if (data.error) {
                            console.log(data);
                            $rootScope.hideLoading();
                            $rootScope.showToast(data.error.user_message, 5000);
                        }
                        $rootScope.hideLoading();
                    }, function (data) {
                        $rootScope.hideLoading();
                        $rootScope.showToast('Errore nella connessione');
                    });
                }, function (FBErrorResponse) {
                    $rootScope.hideLoading();
                    $rootScope.showToast("Errore nella connessione con facebook");
                    console.log("facebook login error data: ");
                    console.log(FBErrorResponse);
                });
            }, function (errorResp) {
                console.log(errorResp);
            });
        };
        $scope.goRegistrazione = function () {
            $rootScope.showLoading();
            $state.go('registrazione', { gioco: $scope.vm.modoGioco });
        };
        if ($rootScope.hideLoading) {
            $rootScope.hideLoading();
        }
    });
})();
/// <reference path="../../../typings/tsd.d.ts" />
/// <reference path="../model/model.ts" />
(function () {
    var app = angular.module('opzioniSquadraCtrl', []);
    app.controller('opzioniSquadraCtrl', function ($scope, $rootScope, $state, $ionicHistory, $ionicLoading, FantaCalcioServices) {
        $scope.vm = {
            nomeSquadra: '',
            urlAvatar: '',
            stemmi: []
        };
        function opzioniSquadraInit() {
            if (!$ionicHistory.forwardView()) {
                $scope.vm.nomeSquadra = $rootScope.squadraSelected.nome;
                $rootScope.avatarTempImg = null;
            }
            else if ($rootScope.nomeSquadraTemp) {
                $scope.vm.nomeSquadra = $rootScope.nomeSquadraTemp;
            }
            else {
                $scope.vm.nomeSquadra = $rootScope.squadraSelected.nome;
                $rootScope.avatarTempImg = null;
            }
            if ($rootScope.avatarTempImg && $rootScope.avatarTempImg.nm_image) {
                $scope.vm.urlAvatar = 'http://' + $rootScope.avatarTempImg.nm_image;
            }
            else {
                $scope.vm.urlAvatar = 'http://' + $rootScope.squadraSelected.url_avatar_large;
            }
            console.log($rootScope.squadraSelected);
            $rootScope.hideLoading();
        }
        ;
        $scope.salvaModifiche = function () {
            $rootScope.showLoading();
            var avatarId;
            if ($rootScope.avatarTempImg)
                avatarId = $rootScope.avatarTempImg.idAvatar;
            else
                avatarId = $rootScope.squadraSelected.idAvatar;
            FantaCalcioServices.modificaSquadra($rootScope.modoGioco, $rootScope.squadraSelected.codice_squadra, $scope.vm.nomeSquadra, avatarId).then(function (data) {
                if (data.error) {
                    $rootScope.showToast(data.error.user_message, 'long');
                    $rootScope.hideLoading();
                }
                else {
                    $rootScope.showToast(data.success.msg_utente);
                    FantaCalcioServices.getSquadre($rootScope.user.idCliente, $rootScope.modoGioco).then(function (dataSquadre) {
                        dataSquadre.squadre.forEach(function (item) {
                            if (data.success.squadra.codice_squadra == item.codice_squadra)
                                $rootScope.squadraSelected = item;
                        });
                        if ($rootScope.modoGioco == 3) {
                            $rootScope.squadreTorneoClassico.forEach(function (item, index) {
                                if (item.codice_squadra == $rootScope.squadraSelected.codice_squadra) {
                                    $rootScope.squadreTorneoClassico[index] = $rootScope.squadraSelected;
                                    if ($rootScope.misterCalcioCup)
                                        window.localStorage.setItem('misterCalcioCupSquadreTorneoClassico' + $rootScope.user.idCliente, JSON.stringify($rootScope.squadreTorneoClassico));
                                    else
                                        window.localStorage.setItem('tuttoSportLeagueSquadreTorneoClassico' + $rootScope.user.idCliente, JSON.stringify($rootScope.squadreTorneoClassico));
                                }
                            });
                        }
                        else {
                            $rootScope.squadreTorneoGironi.forEach(function (item, index) {
                                if (item.codice_squadra == $rootScope.squadraSelected.codice_squadra) {
                                    $rootScope.squadreTorneoGironi[index] = $rootScope.squadraSelected;
                                    if ($rootScope.misterCalcioCup)
                                        window.localStorage.setItem('misterCalcioCupSquadreTorneoGironi' + $rootScope.user.idCliente, JSON.stringify($rootScope.squadreTorneoGironi));
                                    else
                                        window.localStorage.setItem('tuttoSportLeagueSquadreTorneoGironi' + $rootScope.user.idCliente, JSON.stringify($rootScope.squadreTorneoClassico));
                                }
                            });
                        }
                        //$rootScope.squadraSelected.nome = data.success.squadra.ds_squadra;
                        //$rootScope.squadraSelected.idAvatar = data.success.squadra.idAvatar;
                        if ($rootScope.misterCalcioCup)
                            window.localStorage.setItem('misterCalcioCupSquadraSelected' + $rootScope.user.idCliente, JSON.stringify($rootScope.squadraSelected));
                        else
                            window.localStorage.setItem('tuttoSportLeagueSquadraSelected' + $rootScope.user.idCliente, JSON.stringify($rootScope.squadraSelected));
                        console.log(data);
                        $rootScope.hideLoading();
                    }, function (dataSquadre) {
                        $rootScope.hideLoading();
                        $rootScope.showToast('Errore nella connessione');
                    });
                }
            }, function (data) {
                $rootScope.hideLoading();
                $rootScope.showToast('Errore nella connessione');
            });
        };
        $scope.modificaStemma = function () {
            $rootScope.showLoading();
            $rootScope.nomeSquadraTemp = $scope.vm.nomeSquadra;
            $state.go('app.scegliStemma');
        };
        $scope.scegliStemmaInit = function () {
            FantaCalcioServices.modificaAvatarSquadra($rootScope.modoGioco, $rootScope.squadraSelected.codice_squadra)
                .then(function (data) {
                if (data.error) {
                    $rootScope.showToast(data.error.user_message);
                }
                else {
                    $scope.vm.stemmi = data.avatarList;
                    console.log(data);
                }
                $rootScope.hideLoading();
            }, function (data) {
                $rootScope.hideLoading();
                $rootScope.showToast('Errore nella connessione');
            });
        };
        $scope.selezionaStemma = function (avatarImg) {
            $rootScope.avatarTempImg = avatarImg;
            //$rootScope.avatarTempImg = "data:image/png;base64," + avatarImg.bynaryImgLarge;
            $ionicHistory.goBack();
        };
        $scope.eliminaSquadra = function () {
            if (window.cordova) {
                navigator.notification.confirm("Confermi l'eliminazione della squadra?", _eliminaSquadra, 'Elimina squadra', ['Conferma', 'Annulla']);
            }
            else {
                if (confirm("Confermi l'eliminazione della squadra?")) {
                    _eliminaSquadra(1);
                }
            }
        };
        function _eliminaSquadra(buttonIndex) {
            if (buttonIndex == 1) {
                $rootScope.showLoading();
                FantaCalcioServices.eliminaSquadra($rootScope.squadraSelected.codice_squadra).then(function (data) {
                    if (data.error) {
                        $rootScope.showToast(data.error.user_message);
                    }
                    else {
                        $rootScope.showToast(data.success.msg_utente);
                        $ionicHistory.nextViewOptions({
                            disableAnimate: true,
                            disableBack: true,
                            historyRoot: true
                        });
                        $rootScope.squadraSelected = null;
                        $state.go('app.home');
                    }
                    $rootScope.hideLoading();
                }, function (data) {
                    $rootScope.hideLoading();
                    $rootScope.showToast('Errore nella connessione');
                });
            }
        }
        ;
        $scope.$on('$ionicView.beforeEnter', opzioniSquadraInit());
    });
})();
/// <reference path="../../../typings/tsd.d.ts" />
/// <reference path="../model/model.ts" />
(function () {
    var app = angular.module('regolamentoCtrl', []);
    app.controller('regolamentoCtrl', function ($scope, $rootScope) {
        $scope.regolamentoInit = function () {
            $rootScope.hideLoading();
        };
    });
})();
/// <reference path="../../../typings/tsd.d.ts" />
/// <reference path="../model/model.ts" />
(function () {
    var app = angular.module('homeCtrl', []);
    app.controller('homeCtrl', function ($scope, $rootScope, $state, $ionicLoading, $ionicHistory, $ionicModal, FantaCalcioServices) {
        $rootScope.rosaInCorso = false;
        $scope.init = function () {
            $ionicHistory.clearHistory;
            FantaCalcioServices.getSquadre($rootScope.user.idCliente, $rootScope.modoGioco).then(function (data) {
                if (data.error) {
                    $rootScope.showToast(data.error.user_message);
                    if (data.error && data.error.id == 'not_logged')
                        $rootScope.autologin();
                }
                else {
                    console.log(data);
                    $rootScope.testoEdizioneDigitale = $rootScope.user.msgCodicePromo;
                    $rootScope.squadreTorneoGironi = data.squadre;
                    if ($rootScope.misterCalcioCup) {
                        window.localStorage.setItem('misterCalcioCupSquadreTorneoGironi' + $rootScope.user.idCliente, JSON.stringify(data.squadre));
                    }
                    else {
                        window.localStorage.setItem('tuttoSportLeagueSquadreTorneoGironi' + $rootScope.user.idCliente, JSON.stringify(data.squadre));
                    }
                }
                if (!$rootScope.squadraSelected) {
                    $rootScope.squadraSelected = data.squadre[0];
                }
                if ($rootScope.misterCalcioCup) {
                    window.localStorage.setItem('misterCalcioCupSquadraSelected' + $rootScope.user.idCliente, JSON.stringify($rootScope.squadraSelected));
                }
                else {
                    window.localStorage.setItem('tuttoSportLeagueSquadraSelected' + $rootScope.user.idCliente, JSON.stringify($rootScope.squadraSelected));
                }
                if ($rootScope.hideNews.value == false) {
                    $ionicModal.fromTemplateUrl('newsModal.html', {
                        scope: $scope,
                        animation: 'slide-in-up'
                    }).then(function (modal) {
                        $scope.modal = modal;
                        $scope.news = [];
                        FantaCalcioServices.getNews().then(function (data) {
                            if (data.listaNews && data.listaNews.length > 0) {
                                var newsRead = [];
                                if ($rootScope.misterCalcioCup) {
                                    newsRead = JSON.parse(window.localStorage.getItem('misterCalcioCupNewsRead' + $rootScope.user.idCliente));
                                }
                                else {
                                    newsRead = JSON.parse(window.localStorage.getItem('tuttoSportLeagueNewsRead' + $rootScope.user.idCliente));
                                }
                                $rootScope.hideLoading();
                                if (newsRead) {
                                    newsRead.forEach(function (itemRead) {
                                        data.listaNews.forEach(function (itemLista, iLista) {
                                            if (itemLista.idNews == itemRead.idNews) {
                                                data.listaNews.splice(iLista, 1);
                                            }
                                        });
                                    });
                                }
                                $scope.news = data.listaNews;
                                if ($scope.news.length > 0) {
                                    $scope.modal.show();
                                }
                            }
                        });
                        $scope.closeNewsModal = function () {
                            if ($rootScope.hideNews.value == true) {
                                if ($rootScope.misterCalcioCup) {
                                    window.localStorage.setItem('misterCalcioCupNewsRead' + $rootScope.user.idCliente, JSON.stringify($scope.news));
                                }
                                else {
                                    window.localStorage.setItem('tuttoSportLeagueNewsRead' + $rootScope.user.idCliente, JSON.stringify($scope.news));
                                }
                            }
                            $scope.modal.hide();
                        };
                    });
                }
                $rootScope.hideLoading();
            }, function (data) {
                $rootScope.hideLoading();
                $rootScope.showToast('Errore nella connessione');
            });
        };
        $scope.goToFormazione = function () {
            $rootScope.showLoading();
            $state.go('app.formazione');
        };
        $scope.goToDettaglioGiornata = function () {
            $rootScope.showLoading();
            if ($rootScope.modoGioco == 3) {
                $rootScope.giornataClassico = { id_partita: $rootScope.squadraSelected.ultima_partita.id };
                $state.go('app.dettaglioGiornata', {
                    idGiornata: $rootScope.squadraSelected.ultima_partita.id,
                    dataGiornata: $rootScope.squadraSelected.ultima_partita.data,
                    numeroGiornata: $rootScope.squadraSelected.ultima_partita.numero
                });
            }
            else {
                $rootScope.partitaDettaglioGiornata = $rootScope.squadraSelected.ultima_partita;
                $state.go('app.dettaglioGiornataGironi', { idGiornata: $rootScope.squadraSelected.ultima_partita.id,
                    dataGiornata: $rootScope.squadraSelected.ultima_partita.data, numeroGiornata: $rootScope.squadraSelected.ultima_partita.numero });
            }
        };
        $scope.goToGestioneRosa = function () {
            $rootScope.showLoading();
            $state.go('app.gestioneRosa');
        };
        $scope.goToClassificaGironi = function () {
            $rootScope.showLoading();
            $state.go('app.classificheGironi');
        };
    });
})();
//# sourceMappingURL=out.js.map