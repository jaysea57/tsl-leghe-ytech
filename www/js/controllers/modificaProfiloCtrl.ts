/// <reference path="../../../typings/tsd.d.ts" />
/// <reference path="../model/model.ts" />
(function () {
	var app = angular.module('modificaProfiloCtrl', []);
	app.controller('modificaProfiloCtrl', function ($scope, $rootScope, $state, $ionicLoading, $ionicModal,
		FantaCalcioServices: OM.FantaCalcioFactory) {
		$scope.vm = {
			nome: '',
			cognome: '',
			dataNascita: '',
			sesso: '',
			email: '',
			indirizzo: '',
			citta: '',
			provincia: '',
			cap: '',
			notificaRisultati: '0',
			emailAlternativa: '',
			indirizzoSpedizione: '',
			cittaSpedizione: '',
			provinciaSpedizione: '',
			capSpedizione: ''
		};

		$scope.modificaProfilo = function(){
			$rootScope.showLoading();
			var day = $scope.vm.dataNascita.getDate();
			var month = $scope.vm.dataNascita.getMonth()+1;
			if (month < 10) { month = '0' + month; }
			var year = $scope.vm.dataNascita.getFullYear();
			var dataNascita = day + '/' + month + '/' + year;
			FantaCalcioServices.modificaProfilo($rootScope.user.idCliente, $scope.vm.nome, $scope.vm.cognome, dataNascita, $scope.vm.sesso, $scope.vm.email,
					$scope.vm.indirizzo, $scope.vm.citta, $scope.vm.provincia, $scope.vm.cap, $scope.vm.notificaRisultati, $scope.vm.emailAlternativa, 
					$scope.vm.indirizzoSpedizione, $scope.vm.capSpedizione, $scope.vm.cittaSpedizione, $scope.vm.provincia).then(function(data){
				if(data.success){
					console.log(data);
					$rootScope.showToast(data.success.msg_utente);
				}else if(data.error){
					console.log(data);
					$rootScope.showToast(data.error.user_message);
				}
				$rootScope.hideLoading();
			},function(data){
				$rootScope.hideLoading();
				$rootScope.showToast('Errore nella connessione');
			});
		};

		$scope.modificaProfiloInit = function(){
			FantaCalcioServices.getProfilo($rootScope.user.idCliente).then(function(data){
				console.log(data);
				if(data.error){
					$rootScope.showToast(data.error.user_message);
                    if(data.error && data.error.id == 'not_logged')
                        $rootScope.autologin();
				} else {					
					$scope.vm.nome = data.success.user.nome;
					$scope.vm.cognome = data.success.user.cognome;
					if(data.success.user.dt_nascita){
						var giraData = data.success.user.dt_nascita.split('/');
						$scope.vm.dataNascita = new Date(giraData[1]+'/'+giraData[0]+'/'+giraData[2]);
					}
					if(data.success.user.sesso)
						$scope.vm.sesso = data.success.user.sesso.toLowerCase();
					$scope.vm.email = data.success.user.email;
					$scope.vm.indirizzo = data.success.user.via;
					$scope.vm.citta = data.success.user.localita;
					$scope.vm.provincia = data.success.user.provincia;
					$scope.vm.cap = data.success.user.CAP;
					$scope.vm.notificaRisultati = data.success.user.flUnsubscribe;
					$scope.vm.emailAlternativa = data.success.user.emailAlternativa;
					$scope.vm.indirizzoSpedizione = data.success.user.indirizzoSpedizione;
					$scope.vm.cittaSpedizione = data.success.user.cittaSpedizione;
					$scope.vm.provinciaSpedizione = data.success.user.provinciaSpedizione;
					$scope.vm.capSpedizione = data.success.user.capSpedizione;
				}
				$rootScope.hideLoading();
			},function(data){
				$rootScope.hideLoading();
				$rootScope.showToast('Errore nella connessione');
			});
		};

	});

})();
