/// <reference path="../../../typings/tsd.d.ts" />
/// <reference path="../model/model.ts" />
(function () {
    var app = angular.module('regolamentoCtrl', []);
    app.controller('regolamentoCtrl', function ($scope, $rootScope) {
    	$scope.regolamentoInit = function(){
    		$rootScope.hideLoading();
    	};
    });
})();
