/// <reference path="../../../typings/tsd.d.ts" />
/// <reference path="../model/model.ts" />
(function () {
    var app = angular.module('classificheGironiCtrl', []);
    app.controller('classificheGironiCtrl', function ($scope, $rootScope, $stateParams, $state, $ionicLoading, $cordovaGoogleAnalytics,
    	FantaCalcioServices: OM.FantaCalcioFactory) {

        if (window.cordova) $cordovaGoogleAnalytics.trackView('Classifica');

    	$scope.vm = {
    		squadre: [],
            loadingOk: null,
            errorMessage: null
    	};

    	$scope.initClassifica = function(){
    		FantaCalcioServices.getClassifica($rootScope.squadraSelected.codice_squadra).then(function(data){
    			if(data.error){
                    $rootScope.showToast(data.error.user_message);
                    if(data.error && data.error.id == 'not_logged')
                        $rootScope.autologin();
                } else {
                    console.log(data);
                    if(data.msg_utente){
                        $scope.vm.loadingOk = false;
                        $scope.vm.errorMessage = data.msg_utente;
                    } else if(data.torneo_leghe[0].msg_utente){
                        $scope.vm.loadingOk = false;
                        $scope.vm.errorMessage = data.torneo_leghe[0].msg_utente;
                    } else {
                        $scope.vm.squadre = data.torneo_leghe;
                        $scope.vm.loadingOk = true;                        
                    }
                }
                $rootScope.hideLoading();
			},function(data){
                $rootScope.hideLoading();
                $rootScope.showToast('Errore nella connessione');
			})
    	};
    });
})();
