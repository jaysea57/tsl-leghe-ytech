/// <reference path="../../../typings/tsd.d.ts" />
/// <reference path="../model/model.ts" />
(function () {
    var app = angular.module('classificheCtrl', []);
    app.controller('classificheCtrl', function ($scope, $rootScope, $stateParams, $state, $ionicLoading, $cordovaGoogleAnalytics,
    	FantaCalcioServices: OM.FantaCalcioFactory) {

        if (window.cordova) $cordovaGoogleAnalytics.trackView('Classifica');

    	$scope.vm = {
    		squadreSettimanale: [],
    		squadreTorneoAndata: [],
    		squadreTorneoRitorno: [],
    		squadreTorneoEstivo: [],
            errorMessage: '',
            loadingOk: null
    	};

    	$scope.initClassifiche = function(){
    		FantaCalcioServices.getClassifica($rootScope.squadraSelected.codice_squadra).then(function(data){
    			if(data.error){
                    $rootScope.showToast(data.error.user_message);
                    if(data.error && data.error.id == 'not_logged')
                        $rootScope.autologin();
                }else{
                    console.log(data);
                    if(data.settimanale){
                        $scope.vm.squadreSettimanale = data.settimanale;
                        $scope.vm.squadreTorneoAndata = data.torneo_andata;
                        $scope.vm.squadreTorneoRitorno = data.torneo_ritorno;
                        $scope.vm.squadreTorneoEstivo = data.torneo_estivo;
                        $scope.vm.loadingOk = true;
                    } else {
                        $scope.vm.errorMessage = data.msg_utente;
                        $scope.vm.loadingOk = false;
                    }
                }
                $rootScope.hideLoading();
			},function(data){
                $rootScope.hideLoading();
                $rootScope.showToast('Errore nella connessione');
			})
    	};
    });
})();
