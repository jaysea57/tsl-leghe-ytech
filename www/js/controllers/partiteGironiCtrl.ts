/// <reference path="../../../typings/tsd.d.ts" />
/// <reference path="../model/model.ts" />
(function () {
    var app = angular.module('partiteGironiCtrl', []);
    app.controller('partiteGironiCtrl', function ($scope, $rootScope, $stateParams, $state, $ionicLoading, $cordovaGoogleAnalytics,
    	$ionicPopover, FantaCalcioServices: OM.FantaCalcioFactory) {
    	$scope.vm = {
        };

        $scope.partiteGironiInit = function(){
            console.log($rootScope.giornataGironi);
            $rootScope.giornataGironi.partite.forEach(function(item){
                if(item.squadre[0].gol > item.squadre[1].gol){
                    item.squadre[0].winner = true;
                } else if(item.squadre[0].gol < item.squadre[1].gol){
                    item.squadre[1].winner = true;                
                }
            });
            $rootScope.hideLoading();
        };

        $scope.goToDettaglioTabellino = function(indexPartita: number){
            $rootScope.showLoading();
            $rootScope.partitaDettaglioGiornata = $rootScope.giornataGironi.partite[indexPartita];
            if (window.cordova) $cordovaGoogleAnalytics.trackView('Tabellino');
            $state.go('app.dettaglioGiornataGironi', {idGiornata: $rootScope.giornataGironi.id_giornata, dataGiornata: $rootScope.giornataGironi.data, 
                numeroGiornata: $rootScope.giornataGironi.numero});
        };

    });
})();
