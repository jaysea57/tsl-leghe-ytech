/// <reference path="../../../typings/tsd.d.ts" />
/// <reference path="../model/model.ts" />
/*
interface IDatiLegaScope extends ng.IScope {
	modificaLega: string;
	vm: {
		readonly: boolean;
		bloccaSaldo: boolean;
		visibleCard: number;
		amici: OM.Amici[];
		nomeCampionato: string;
		numeroSquadre: number;
		bloccaNumeroSquadre: boolean;
		saldoIniziale: number;
		dataPrimaGiornata: any;
		dataScadenzaAsta: any;
		numeriSquadreDisponibili: number[],
		giornateDisponibili: OM.GiornateLeghe[];
		dataPrimaGiornataSelected: OM.GiornateLeghe;
		bonus: {
			golPortiere: number;
			golDifensore: number;
			golCentrocampista: number;
			golAttaccante: number;
			rigoreSegnato: number;
			rigoreParato: number;
			zeroGol: number;
			cambioVincente: number;
		},
		malus: {
			golSubito: number;
			autoRete: number;
			rigoreSbagliato: number;
			ammonizione: number;
			espulsione: number;
			cambioPerdente: number;
		}
	};
	setVisibleCard: (cardNumber: number) => void;
	datiLegaInit: () => void;
	changeSquadre: () => void;
	confermaLega: () => void;
}
*/
(function(){
	var app = angular.module('datiLegaCtrl', []);

	app.controller('datiLegaCtrl', function($scope, $rootScope,	FantaCalcioServices: OM.FantaCalcioFactory, 
		$timeout, $ionicScrollDelegate) {
		$scope.modificaLega = 'N';
		$scope.vm = {
			readonly: null,
			bloccaSaldo: false,
			visibleCard: 1,
			amici: [],
			nomeCampionato: null,
			numeroSquadre: null,
			bloccaNumeroSquadre: null,
			saldoIniziale: null,
			dataPrimaGiornata: null,
			dataScadenzaAsta: null,
			numeriSquadreDisponibili: [],
			giornateDisponibili: [],
			dataPrimaGiornataSelected: null,
			bonus: {
				cambioVincente: null,
				golAttaccante: null,
				golCentrocampista: null,
				golDifensore: null,
				golPortiere: null,
				rigoreParato: null,
				rigoreSegnato: null,
				zeroGol: null,
				portiere: null,
				allenatore: null
			},
			malus: {
				ammonizione: null,
				autoRete: null,
				cambioPerdente: null,
				espulsione: null,
				golSubito: null,
				rigoreSbagliato: null
			}
		};		

		$scope.setVisibleCard = function(cardNumber){
			$scope.vm.visibleCard = cardNumber;
			$timeout(function(){
				$ionicScrollDelegate.$getByHandle('legaScroll').resize();
			}, 40);
		};
		$scope.changeSquadre = function(){
			$scope.vm.amici = [];
			for (var i = 0; i < $scope.vm.numeroSquadre - 1; ++i) {
				$scope.vm.amici.push({
					cognome: null,
					email: null,
					hasBids: false,
					idCliente: null,
					idGruppo: null,
					idSquadra: null,
					nome: null,
					posizione: null
				});
			}
		};

		$scope.datiLegaInit = function(){
			if($rootScope.squadraSelected.is_presidente_lega == true){
				$scope.vm.readonly = false;
			} else {
				$scope.vm.readonly = true;
			}
			FantaCalcioServices.getDatiLega($rootScope.squadraSelected.id_gruppo, $rootScope.squadraSelected.codice_squadra).then(function(data){
				console.log(data);
				
				if(data.error && data.error.user_message){
					$rootScope.showToast(data.error.user_message, 5000);
					$scope.vm.readonly = true;
				} else {
					if(data.campionato.campionatoConfig.nomeCampionato != '-'){
						$scope.modificaLega = 'Y';
					}

					$scope.vm.numeroSquadre = data.amici.length + 1;
					$scope.vm.amici = data.amici;
					$scope.vm.amici.some(function(item){
						if(item.hasBids == true){
							$scope.vm.bloccaNumeroSquadre = true;
							$scope.vm.bloccaSaldo = true;
							return true;
						}
					});

					for (var i = data.minSquadreLega; i <= data.maxSquadreLega; i++) {
						$scope.vm.numeriSquadreDisponibili.push(i);
					}
					
					$scope.vm.giornateDisponibili = data.giornate;

					if(data.amici.length > 0){
						$scope.vm.bloccaNumeroSquadre = true;
					}
					$scope.vm.giornateDisponibili.some(function(item){
						if(item.dtPartite == data.campionato.campionatoConfig.dtPrimaPartita){
							$scope.vm.dataPrimaGiornataSelected = item;
							return true;
						}
					});

					$scope.vm.nomeCampionato = data.campionato.campionatoConfig.nomeCampionato;
					$scope.vm.saldoIniziale = data.campionato.campionatoConfig.saldoIniziale;
					if(data.campionato.campionatoConfig.dtScadAsta){
						var giraData = data.campionato.campionatoConfig.dtScadAsta.split('/');
						$scope.vm.dataScadenzaAsta = new Date(giraData[1]+'/'+giraData[0]+'/'+giraData[2]);
					} else {
						$scope.vm.dataScadenzaAsta = new Date();
					}
					$scope.vm.bonus.golPortiere = data.campionato.golp.GOLP;
					$scope.vm.bonus.golDifensore = data.campionato.gold.GOLD;
					$scope.vm.bonus.golCentrocampista = data.campionato.golc.GOLC;
					$scope.vm.bonus.golAttaccante = data.campionato.gola.GOLA;
					$scope.vm.bonus.rigoreSegnato = data.campionato.rigseg.rs;
					$scope.vm.bonus.rigoreParato = data.campionato.rpar.rp;
					$scope.vm.bonus.zeroGol = data.campionato.zerogol.gol_45;
					$scope.vm.bonus.cambioVincente = data.campionato.bonus_sost.BSG;
					$scope.vm.malus.golSubito = data.campionato.gsub.gs;
					$scope.vm.malus.autoRete = data.campionato.naut.aut;
					$scope.vm.malus.rigoreSbagliato = data.campionato.rsba.rsb;
					$scope.vm.malus.ammonizione = parseFloat(data.campionato.a.a_e);
					$scope.vm.malus.espulsione = parseFloat(data.campionato.e.a_e);
					$scope.vm.malus.cambioPerdente = data.campionato.malus_sost.MSA;
					
					if(data.msgUtente){
						$rootScope.showToast(data.msgUtente, 5000);
					}
				}
				$rootScope.hideLoading();
			}, function(data){
				$rootScope.showToast(data.error.user_message, 5000);
				$rootScope.hideLoading();
			});
		};

		$scope.confermaLega = function(){
			$rootScope.showLoading();
			if(!$scope.vm.dataScadenzaAsta){
				$rootScope.hideLoading();
				navigator.notification.alert('Inserisci la data di scadeza asta', function(){}, 'Lega amici');
				return;
			}
			var dataScadenzaAsta = '';
			var day = $scope.vm.dataScadenzaAsta.getDate();
			var month = $scope.vm.dataScadenzaAsta.getMonth()+1;
			if (month < 10) { month = '0' + month; }
			var year = $scope.vm.dataScadenzaAsta.getFullYear();
			dataScadenzaAsta = day + '/' + month + '/' + year;
			if($scope.vm.nomeCampionato.trim() == '-' || $scope.vm.nomeCampionato.trim() == '' ){
				$rootScope.hideLoading();
				navigator.notification.alert('Inserisci un nome per il campionato', function(){}, 'Lega amici');
				return;
			}
			if(isNaN($scope.vm.numeroSquadre)){
				$rootScope.hideLoading();
				navigator.notification.alert('Seleziona il numero di squadre', function(){}, 'Lega amici');
				return;
			}
			if(!$scope.vm.dataPrimaGiornataSelected.idGGCamp){
				$rootScope.hideLoading();
				navigator.notification.alert('Seleziona il numero di squadre', function(){}, 'Lega amici');
				return;	
			}

			var mailVuote = false;
			if($scope.vm.amici.length == 0){
				mailVuote = true;
			} else {
				$scope.vm.amici.some(function(item){
					if(!item.email){
						mailVuote = true;
						return true;
					}
				});
			}
			if(mailVuote == true){
				navigator.notification.alert('Gli amici non verranno invitati finchè non inserisci tutte le mail', function(){
					FantaCalcioServices.saveDatiLega($scope.vm.nomeCampionato, $scope.modificaLega, $scope.vm.dataPrimaGiornataSelected.idGGCamp, $scope.vm.numeroSquadre, dataScadenzaAsta,
						$rootScope.squadraSelected.codice_squadra, $rootScope.user.idCliente, $scope.vm.saldoIniziale, $scope.vm.bonus.golPortiere, $scope.vm.bonus.golDifensore,
						$scope.vm.bonus.golCentrocampista, $scope.vm.bonus.golAttaccante, $scope.vm.bonus.rigoreSegnato, $scope.vm.bonus.rigoreParato, $scope.vm.bonus.zeroGol,
						$scope.vm.bonus.cambioVincente, $scope.vm.malus.golSubito, $scope.vm.malus.autoRete, $scope.vm.malus.rigoreSbagliato, $scope.vm.malus.ammonizione,
						$scope.vm.malus.espulsione, $scope.vm.malus.cambioPerdente).then(function(data){
							console.log(data);
							if(data.success){
								$scope.modificaLega = 'Y';
								$rootScope.showToast(data.success.msg_utente);
								$rootScope.hideLoading();
							} else {
								$rootScope.showToast(data.error.user_message);
								$rootScope.hideLoading();
							}
						}, function(data){
							$rootScope.showToast("Errore nella connessione");
							console.log(data);
							$rootScope.hideLoading();
						});
					}, 'Lega amici');
				$rootScope.hideLoading();
				return;
			} else {
				FantaCalcioServices.saveDatiLega($scope.vm.nomeCampionato, $scope.modificaLega, $scope.vm.dataPrimaGiornataSelected.idGGCamp, $scope.vm.numeroSquadre, dataScadenzaAsta,
				$rootScope.squadraSelected.codice_squadra, $rootScope.user.idCliente, $scope.vm.saldoIniziale, $scope.vm.bonus.golPortiere, $scope.vm.bonus.golDifensore,
				$scope.vm.bonus.golCentrocampista, $scope.vm.bonus.golAttaccante, $scope.vm.bonus.rigoreSegnato, $scope.vm.bonus.rigoreParato, $scope.vm.bonus.zeroGol,
				$scope.vm.bonus.cambioVincente, $scope.vm.malus.golSubito, $scope.vm.malus.autoRete, $scope.vm.malus.rigoreSbagliato, $scope.vm.malus.ammonizione,
				$scope.vm.malus.espulsione, $scope.vm.malus.cambioPerdente).then(function(data){
					console.log(data);
					if(data.success){
						$scope.modificaLega = 'Y';
						$rootScope.showToast(data.success.msg_utente, 2500);
						if(mailVuote == false){
							var amici = [];
							$scope.vm.amici.forEach(function(item){
								amici.push(item.email);
							});
							FantaCalcioServices.invitaAmiciLega($scope.vm.numeroSquadre, $rootScope.squadraSelected.codice_squadra, amici).then(function(dataAmici){
								console.log(dataAmici);
								$rootScope.hideLoading();
								if(dataAmici.success){
									$rootScope.showToast(dataAmici.success.msg_utente, 5000);
									$scope.vm.bloccaNumeroSquadre = true;
								} else {
									$rootScope.showToast(dataAmici.error.user_message);
								}
							},function(dataAmici){
								$rootScope.showToast("Errore nella connessione");
								console.log(data);
								$rootScope.hideLoading();
							})
						} else {
							$rootScope.hideLoading();
						}
					} else {
						$rootScope.showToast(data.error.user_message);
						$rootScope.hideLoading();
					}
				}, function(data){
					$rootScope.showToast("Errore nella connessione");
					console.log(data);
					$rootScope.hideLoading();
				});
			}
		};

	});

/*
	app.directive('limitDecimal', function(){
		return {
			require: 'ngModel',
			scope: {
				upLimit: '=upLimit',
				downLimit: '=downLimit'
			},
			link: function(scope: any, element, attrs, modelCtrl) {
			modelCtrl.$parsers.push(function (inputValue) {
					if (inputValue == undefined || inputValue == null || inputValue == 0) 
						return '';

					var transformedInput: string = isNaN(modelCtrl.$viewValue) == false ? modelCtrl.$viewValue.toString() : modelCtrl.$viewValue ;
					var pointPosition: number = transformedInput.indexOf('.');
					if(pointPosition > 0){
						if(pointPosition < transformedInput.length - 1){
							var splitted = transformedInput.split('.');
							if(splitted[1].length > 1){
								splitted[1] = splitted[1].charAt(0);
								transformedInput = splitted.join('.');
							}
						}
					}


					if(transformedInput > scope.upLimit)
						transformedInput = scope.upLimit;
					else if (transformedInput < scope.downLimit)
						transformedInput = scope.downLimit;

					if (transformedInput.toString() != modelCtrl.$viewValue) {
						modelCtrl.$setViewValue(transformedInput);
						modelCtrl.$render();
					}         
					return transformedInput;
				});
			}
		};
	});

	//regex: /[0-9]|[\.|,-]/g,
	app.directive('onlyInteger', function(){
		return {
			require: 'ngModel',
			link: function(scope, element, attrs, modelCtrl) {
			 	
				modelCtrl.$parsers.push(function (inputValue) {
					if(inputValue){
						inputValue = inputValue.toString();
		                var transformedInput = inputValue ? inputValue.replace(/[^0-9]/g,'') : null;
	                	transformedInput = Math.abs(transformedInput);
		                if (transformedInput != inputValue) {
		                    modelCtrl.$setViewValue(transformedInput.toString().trim());
		                    modelCtrl.$render();
		                }

		                return transformedInput;
					}
	            })
				
			}
		}
	});
*/

  })();