/// <reference path="../../../typings/tsd.d.ts" />
/// <reference path="../model/model.ts" />

(function () {
	var app = angular.module('registrazioneCtrl', []);
	app.controller('registrazioneCtrl', function ($scope, $rootScope, $state, $ionicLoading, $ionicModal, $ionicScrollDelegate, $cordovaFileTransfer,
		$stateParams, $cordovaGoogleAnalytics, FantaCalcioServices: OM.FantaCalcioFactory) {

		/*var torneoClassicoUrl = window.location.host.indexOf('192') == 0 || window.location.host.indexOf('localhost') == 0
            //TUTTO SPORT LEAGUE CLASSICO
                ?'http://localhost:9292/league.tsport.yland.it/'
                //:'http://league.tsport.yland.it/';                      //ESTERNA
                :'http://league.tuttosport.com/';                       //PRODUZIONE
                //:'http://devts.tuttosport.com/';                              //RETE AZIENDALE Y TECH
            //MISTER CALCIO CUP CLASSICO
                //?'http://localhost:9292/gp.mr.yland.it/'
                //:'http://gp.mr.yland.it/'                                 //RETE ESTERNA
                //:'http://devgp.corrieredellosport.it/'                        //RETE AZIENDALE Y TECH
                ;

        var torneoGironiUrl = window.location.host.indexOf('192') == 0 || window.location.host.indexOf('localhost') == 0
            //TUTTO SPORT LEAGUE GIRONI modo_gioco = 1
                ?'http://localhost:9292/gironi.tsport.yland.it/'
                //:'http://gironi.tsport.yland.it/';                          //ESTERNA
                :'http://gironi-league.tuttosport.com/';                    //PRODUZIONE
                //:'http://devtsgironi.tuttosport.com/';                    //RETE AZIENDALE Y TECH
            //MISTER CALCIO CUP GIRONI modo_gioco = 1
                //?'http://localhost:9292/www.mr.yland.it/'
                //:'http://www.mr.yland.it/';                               //ESTERNA
                //:'http://devmcc.corrieredellosport.it/'                           //RETE AZIENDALE Y TECH
                ;*/

		if (window.cordova) $cordovaGoogleAnalytics.trackView('Registrazione');

		$scope.vm = {
			nome: '',
			cognome: '',
			dataNascita: '',
			email: '',
			nickname: '',
			password: '',
			accettaRegolamentoDiGioco: false,
			accettaCondizioniEditore: false,
			accettaCondizioniGioco: false,
			accettaPrivacy1: false,
			accettaPrivacy2: false,
			accettaPrivacy3: false,
			modoGioco: $stateParams.gioco
		};
		$scope.ionicScrollDelegate = $ionicScrollDelegate;

		$scope.registrazione = function(){
			$rootScope.showLoading();
			var day = $scope.vm.dataNascita.getDate();
			if (day < 10) { day = '0' + day; }
			var month = $scope.vm.dataNascita.getMonth()+1;
			if (month < 10) { month = '0' + month; }
			var year = $scope.vm.dataNascita.getFullYear();
			var dataNascita = day + '/' + month + '/' + year;
			FantaCalcioServices.registraUtente($scope.vm.email, $scope.vm.password, $scope.vm.nome, $scope.vm.cognome, $scope.vm.nickname, dataNascita, 
				$scope.vm.accettaRegolamentoDiGioco, $scope.vm.accettaPrivacy2, $scope.vm.accettaPrivacy3, $scope.vm.accettaCondizioniGioco, $scope.vm.accettaCondizioniEditore, 
				$scope.vm.accettaPrivacy1, $stateParams.gioco).then(function(data){
				
				if(data.success){
					var user = {
						email: data.success.user.email,
						password: data.success.user.password,
						modoGioco: $scope.vm.modoGioco
					}
					console.log(data);
					FantaCalcioServices.authentication(user.email, user.password, user.modoGioco, $rootScope.deviceToken, $rootScope.deviceId, $rootScope.deviceTarget).then(function(loginData:OM.AuthenticationResponse){
						if(loginData.success){
							console.log(loginData);
							$rootScope.modoGioco = user.modoGioco;
							$rootScope.user = loginData.success.user;

							FantaCalcioServices.getSquadre($rootScope.user.idCliente, $rootScope.modoGioco).then(function(squadreData){
								if(squadreData.error){
									$rootScope.hideLoading();
                        			$rootScope.showToast(squadreData.error.user_message);
								}else{
									console.log(squadreData);
									$rootScope.squadreTorneoGironi = squadreData.squadre;
									$rootScope.rosaInCorso = false;
									$rootScope.testoEdizioneDigitale = $rootScope.user.msgCodicePromo;
									$rootScope.squadraSelected = squadreData.squadre[0];
									$state.go('app.opzioniSquadra');
								}
							},function(squadreData){
								$rootScope.hideLoading();
                    			$rootScope.showToast('Errore nella connessione');
							});
						}else if(loginData.error){
							console.log(loginData);
							$rootScope.hideLoading();
                			$rootScope.showToast(loginData.error.user_message, 5000);
						}
					},function(loginData){
						console.log(loginData);
						$rootScope.hideLoading();
    					$rootScope.showToast('Errore nella connessione');
					});
				}else if(data.error){
					console.log(data);
					$rootScope.hideLoading();
        			$rootScope.showToast(data.error.user_message, 5000);
				}
			},function(data){
				$rootScope.hideLoading();
    			$rootScope.showToast('Errore nella connessione');
			});
		};

		$scope.goBack = function(){
			window.history.back();
		};

		$scope.openRegolamento = function() { // regolamento_concorso_xxx.pdf
			$rootScope.showLoading();
			if(ionic.Platform.isIOS()){
				$rootScope.hideLoading();
				window.open($scope.urlDomain+'doc/regolamento_gioco_leghe.pdf',"_blank", "closebuttoncaption=Indietro,location=no,enableviewportscale=yes"); 				
			} else if(ionic.Platform.isAndroid()){
				var filePath: string = encodeURI(window.cordova.file.externalRootDirectory + ('Download/regolamento_gioco_leghe.pdf'));

				var trustHosts = true
			    var options = {};

				$cordovaFileTransfer.download(encodeURI($scope.urlDomain+'doc/regolamento_gioco_leghe.pdf'),
					filePath, options, trustHosts).then(function(fileEntry){
						console.log("Download success");
						console.log(fileEntry);
						$rootScope.hideLoading();
						window.cordova.plugins.fileOpener2.open(filePath, 'application/pdf');
					}, function(error){
						$rootScope.hideLoading();
						navigator.notification.alert('Non è stato possibile recuperare l\'allegato, riprovare.', function(){}, '...');
						console.log("Download error");
						console.log(error);
					});
			}
		};

		$scope.openServizioEditore = function() {
			$rootScope.showLoading();
			if(ionic.Platform.isIOS()){
				$rootScope.hideLoading();
				window.open($scope.urlDomain+'doc/condizioni_generali_utilizzo.pdf',"_blank", "closebuttoncaption=Indietro,location=no,enableviewportscale=yes"); 				
			} else if(ionic.Platform.isAndroid()){					
				var filePath: string = encodeURI(window.cordova.file.externalRootDirectory + "Download/condizioni_generali_utilizzo.pdf");

				var trustHosts = true
			    var options = {};

				$cordovaFileTransfer.download(encodeURI($scope.urlDomain+'doc/condizioni_generali_utilizzo.pdf'),
					filePath, options, trustHosts).then(function(fileEntry){
						console.log("Download success");
						console.log(fileEntry);
						$rootScope.hideLoading();
						window.cordova.plugins.fileOpener2.open(filePath, 'application/pdf');
					},
					function(error){
						$rootScope.hideLoading();
						navigator.notification.alert('Non è stato possibile recuperare l\'allegato, riprovare.', function(){}, '...');
						console.log("Download error");
						console.log(error);
					});
			}
		};
		$scope.openServizioGioco = function() {
			$rootScope.showLoading();
			if(ionic.Platform.isIOS()){
				$rootScope.hideLoading();
				window.open($scope.urlDomain+('doc/condizioni_generali_utilizzo')+($stateParams.gioco==3?'_classico':'_gironi')+'.pdf',"_blank", "closebuttoncaption=Indietro,location=no,enableviewportscale=yes"); 				
			} else if(ionic.Platform.isAndroid()){
				var filePath: string = encodeURI(window.cordova.file.externalRootDirectory + ('Download/condizioni_generali_utilizzo')+($stateParams.gioco==3?'_classico':'_gironi')+'.pdf');

				var trustHosts = true
			    var options = {};

				$cordovaFileTransfer.download(encodeURI($scope.urlDomain+('doc/condizioni_generali_utilizzo')+($stateParams.gioco==3?'_classico':'_gironi')+'.pdf'),
					filePath, options, trustHosts).then(function(fileEntry){
						$rootScope.hideLoading();
						console.log("Download success");
						console.log(fileEntry);
						window.cordova.plugins.fileOpener2.open(filePath, 'application/pdf');
					},
					function(error){
						$rootScope.hideLoading();
						navigator.notification.alert('Non è stato possibile recuperare l\'allegato, riprovare.', function(){}, '...');
						console.log("Download error");
						console.log(error);
					});
			}
		};
		$scope.openInformativaPrivacy = function() {
			$rootScope.showLoading();
			if(ionic.Platform.isIOS()){
				$rootScope.hideLoading();
				window.open($scope.urlDomain+'doc/informativa_cds.pdf',"_blank", "closebuttoncaption=Indietro,location=no,enableviewportscale=yes"); 				
			} else if(ionic.Platform.isAndroid()){
				var fileTransfer = new window['FileTransfer']();
				var filePath: string = encodeURI(window.cordova.file.externalRootDirectory + "Download/condizioni_generali_utilizzo.pdf");


				var trustHosts = true
			    var options = {};

				$cordovaFileTransfer.download(encodeURI($scope.urlDomain+'doc/informativa_cds.pdf'),
					filePath, options, trustHosts).then(function(fileEntry){
						$rootScope.hideLoading();
						console.log("Download success");
						console.log(fileEntry);
						window.cordova.plugins.fileOpener2.open(filePath, 'application/pdf');
					},
					function(error){
						$rootScope.hideLoading();
						navigator.notification.alert('Non è stato possibile recuperare l\'allegato, riprovare.', function(){}, '...');
						console.log("Download error");
						console.log(error);
					});
			}
		};

		$scope.registrazioneInit = function(){
			if($stateParams.gioco == 3) {
				$scope.urlDomain = $rootScope.torneoClassicoUrl;
			} else {
				$scope.urlDomain = $rootScope.torneoGironiUrl;
			}
			$rootScope.hideLoading();
		};

	});

	app.directive('scrollTop', function () {
	    return {
	        restrict: 'A',
	        scope: false,
	        link: function (scope: any, elem) {
	            elem.on('submit', function () {
	            	var firstInvalid = elem[0].querySelector('.ng-invalid');
	                if (firstInvalid) {
	                	//scope.$parent.ionicScrollDelegate.scrollTo(0, firstInvalid.offsetHeight);
						// jc err compilazione su offsetHeight 
						scope.$parent.ionicScrollDelegate.scrollTo(0, firstInvalid.clientHeight);
						//$ionicScrollDelegate.$getByHandle('contentScroll').resize();						
	                }
	            });
	        }
	    };
	});

	app.directive('numericPassword', function() {
	  return {
		require: 'ngModel',
		link: function(scope, elm, attrs, ctrl) {
		  ctrl.$validators.password = function(modelValue, viewValue) {
			//if (ctrl.$isEmpty(modelValue)) {
			//  // consider empty models to be valid
			//  return true;
			//}

			if (viewValue.match(/\d+/g) != null) {
			  // it is valid
			  return true;
			}

			// it is invalid
			return false;
		  };
		}
	  };
	});

	app.directive('etaMinima', function() {
	  return {
		require: 'ngModel',
		link: function(scope, elm, attrs, ctrl) {
		  ctrl.$validators.etaMinima = function(modelValue, viewValue) {
			var now: any = new Date();
			now = now.getYear();
			var dataInserita: any = new Date(viewValue);
			dataInserita = dataInserita.getYear();
			if (now - dataInserita >= 18) {
			  // it is valid
			  return true;
			}

			// it is invalid
			return false;
		  };
		}
	  };
	});

	app.directive('ripetiMail', function() {
	  return {
		require: 'ngModel',
		scope: {mailCheck: '=mailCheck'},
		link: function(scope: any, elm, attrs, ctrl) {
		  ctrl.$validators.ripetiMail = function(modelValue, viewValue) {
			if(scope.mailCheck && viewValue){
				if (scope.mailCheck.toLowerCase() == viewValue.toLowerCase()) {
				  // it is valid
				  return true;
				}
			}

			// it is invalid
			return false;
		  };
		}
	  };
	});

})();
